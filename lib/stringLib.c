/*
 *  stringLib.c
 *  trackDataReader
 *
 *  Created by Dominique Derauw on 3/02/05.
 *  Copyright 2005 Dominique Derauw. All rights reserved.
 *
 */

#include "stringLib.h"

char *allocStringOfLength(size_t length)
{
	char	*str ;
	int i ;

    if((str = malloc(++length)) == NULL) {
		perror("String allocation error ") ;
        exit(-1) ;
    }

	for(i = 0; i < length; i++) str[i] = 0 ;

	return str ;
}


/* Lecture de la première chaine de carractère d'une ligne de texte */
char *readFirstStringIn(char *aString)
{
    char	*firstString ;
	char	buffer[MAXSTRINGLENGTH] ;

	if(aString != NULL) {
		sscanf(aString, "%s ", buffer) ;
		if((firstString = (char *)calloc(1, strlen(buffer) + 1)) == NULL)
			perror("String allocation error\n") ;
		sprintf(firstString, "%s", buffer) ;
    }
    else {
        firstString = allocStringOfLength(0) ;
    }

    return firstString ;
}

char *getStringUpToDelimiter(char *aString, char *delimiter)
{
    char *readString = NULL ;
    int stringLength ;

    stringLength = locateSubStringInString(aString, delimiter) ;
    if(stringLength > 0) {
        readString = allocStringOfLength(stringLength) ;
        memcpy(readString, aString, stringLength) ;
    }

    return(readString) ;
}

char *getStringAfterDelimiter(char *aString, char *delimiter)
{
    char *readString = NULL ;
    int stringLength, i0 ;

    i0 = locateSubStringInString(aString, delimiter) ;
    if(i0 > 0) {
        stringLength = (int)strlen(aString) - i0++ ;
        readString = allocStringOfLength(stringLength) ;
        memcpy(readString, aString + i0, stringLength) ;
    }

    return(readString) ;
}


char *concatenateStrings(char *firstString, char *secondString)
{
    char *aString ;
    size_t stringLength1, stringLength2 ;

    if (firstString == NULL || secondString == NULL) {
        return (NULL) ;
    }
    stringLength1 = strlen(firstString) ;
    stringLength2 = strlen(secondString) ;

    aString = allocStringOfLength(stringLength1 + stringLength2) ;
    memcpy(aString, firstString, stringLength1) ;
    memcpy(aString + stringLength1, secondString, stringLength2) ;

    return(aString) ;
}


/* Strip white characters at start of string */
void stripWhiteCharsAtStartOfString(char *aString)
{
    int i, i0, stringLength ;

    stringLength = (int)strlen(aString) ;
    if(!(i = stringLength)) return ;

    i = 0 ;
    while(i < stringLength) {
        if(strncmp(&aString[i], " ", 1) && (aString[i] != '\n') && (aString[i] != '\r') && (aString[i] != '\t')) break ;
        i++ ;
    }
    if (i) {
        i0 = i ;
        stringLength -= i ;
        for (i = 0; i < stringLength; i++) {
            aString[i] = aString[i + i0] ;
        }
        aString[i] = '\0' ;
    }
}

/* Strip white characters at end of string */
void stripWhiteCharsAtEndOfString(char *aString)
{
    int i ;

    if(!(i = (int)strlen(aString))) return ;

    i-- ;
    while(i >= 0) {
        if(strncmp(&aString[i], " ", 1) && (aString[i] != '\n') && (aString[i] != '\r') && (aString[i] != '\t')) break ;
        i-- ;
    }
    aString[i + 1] = '\0' ;
}

void replaceWhiteCharsInStringByChar(char *aString, char replacementChar)
{
    size_t i, stringLength ;

	if(!(stringLength = strlen(aString))) return ;
    for (i = 0; i < stringLength; i++) {
        if(!strncmp(&aString[i], " ", 1) || (aString[i] == '\n') || (aString[i] == '\r') || (aString[i] == '\t') || (aString[i] == 0x1a))
            strncpy(&aString[i], &replacementChar, 1);
    }
}

/* Replace subString with subString of same length in string */
char replaceSubStringInString(char *aString, char *subStringToFind, char *replacementString)
{
	int i ;
    size_t subStringLength ;

	subStringLength = strlen(subStringToFind) ;
	if(subStringLength != strlen(replacementString))
		return(SUBSTRINGTOOBIG) ;

	i = locateSubStringInString(aString, subStringToFind) ;
	if(i < 0) return i ;
    
	strncpy(aString + i, replacementString, subStringLength) ;

	return(0) ;
}

/* Replace subString with subString of same length in string */
char replaceNSubStringInString(char *aString, char *subStringToFind, char *replacementString)
{
    int err ;

	while ((err = replaceSubStringInString(aString, subStringToFind, replacementString)) >= 0) {
    }
	if (err == SUBSTRINGNOTFOUND) {
        return (0) ;
    }
	return(err) ;
}

char isSubStringPresentInString(char *aString, char *subStringToFind)
{
    return(locateSubStringInString(aString, subStringToFind) >= 0) ;

}

/* This function points after first occurrence of sub-string in a given string */
char *getStringAfterSubStringInString(char *aString, char *aSubString)
{
    return(aString + locateSubStringInString(aString, aSubString) + strlen(aSubString)) ;
}

/* This function finds the first occurence of a substring within a given string and returns its start position */
int locateSubStringInString(char *aString, char *subStringToFind)
{
	size_t i, searchLength, subStringLength ;

    if (!aString) {
        return(SUBSTRINGNOTFOUND) ;
    }
	subStringLength = strlen(subStringToFind) ;
    if(!subStringLength) {
        return SUBSTRINGNOTFOUND ;
    }
    if(strlen(aString) < subStringLength) {
		return(STRINGTOOSMALL) ;
    }

    searchLength = (strlen(aString) - subStringLength + 1) ;
	i = 0 ;
	while(strncmp(aString + i, subStringToFind, subStringLength) && (i < searchLength)) {
		i++ ;
	}
    if(i == searchLength) {
		return(SUBSTRINGNOTFOUND) ;
    }

	return((int)i) ;
}

void removeStringInString(char *aString, char *stringToRemove)
{
    char *endString ;
    int i ;
    size_t subStringLength, endStringLength ;

    subStringLength = strlen(stringToRemove) ;

    if((i = locateSubStringInString(aString, stringToRemove)) < 0)
        return ;

    if ((endString = initStringWithString(&aString[i + subStringLength])) != NULL) {
        endStringLength = strlen(endString) ;
        memcpy(&aString[i], endString, endStringLength) ;
        aString[i + endStringLength] = '\0' ;
        free(endString)  ;
    }
    else
        aString[i] = '\0' ;
}

void removeAnyOccurrenceOfSubStringInString(char *aString, char *stringToRemove)
{
    char *endString ;
    int i ;
    size_t subStringLength, endStringLength ;

    subStringLength = strlen(stringToRemove) ;

    while ((i = locateSubStringInString(aString, stringToRemove)) >= 0) {
        if ((endString = initStringWithString(&aString[i + subStringLength])) != NULL) {
            endStringLength = strlen(endString) ;
            memcpy(&aString[i], endString, endStringLength) ;
            aString[i + endStringLength] = '\0' ;
            free(endString)  ;
        }
        else
            aString[i] = '\0' ;
    }

}

void convertStringToLowercase(char *aString)
{
    int i ;
    size_t stringLength ;

    if(aString == NULL)
       return ;

    stringLength = strlen(aString) ;
    for (i = 0; i < stringLength; i++) {
        if (aString[i] >= 65 && aString[i] <= 90) {
            aString[i] += 32 ;
        }
    }
}

char *initStringWithString(char *aString)
{
	char *aNewString ;
	long stringLength ;

	stringLength = strlen(aString) ;
    aNewString = NULL ;
    if(aString) {
        aNewString = allocStringOfLength(stringLength) ;
        if((stringLength)) {
            memcpy(aNewString, aString, stringLength) ;
        }
    }

	return(aNewString) ;
}

char *initStringWith2Strings(char *string1, char *string2)
{
	char *aNewString ;
	long stringLength ;

    aNewString = NULL ;
    if(string1 && string2) {
        stringLength = strlen(string1) + strlen(string2) ;
        aNewString = allocStringOfLength(stringLength) ;
        sprintf(aNewString, "%s%s", string1, string2) ;
	}

	return(aNewString) ;
}

char *initStringWith3Strings(char *string1, char *string2, char *string3)
{
	char *aNewString ;
	long stringLength ;

    aNewString = NULL ;
    if(string1 && string2 && string3) {
        stringLength = strlen(string1) + strlen(string2) + strlen(string3) ;
        aNewString = allocStringOfLength(stringLength) ;
        sprintf(aNewString, "%s%s%s", string1, string2, string3) ;
	}
	return(aNewString) ;
}


char *initStringWith4Strings(char *string1, char *string2, char *string3, char *string4)
{
	char *aNewString ;
	long stringLength ;

    aNewString = NULL ;
    if(string1 && string2 && string3 && string4) {
        stringLength = strlen(string1) + strlen(string2) + strlen(string3) + strlen(string4) ;
        aNewString = allocStringOfLength(stringLength) ;
        sprintf(aNewString, "%s%s%s%s", string1, string2, string3, string4) ;
	}

	return(aNewString) ;
}


void removeEmptyValsInCSV(CSVStruct *csv)
{
    char **stringVal ;
    int i, i2, numberOfEntries ;
    size_t stringValLength ;

    numberOfEntries = csv->numberOfEntries ;
    for (i = 0; i < csv->numberOfEntries; i++) {
        if (!strlen(csv->stringVal[i])) {
            numberOfEntries-- ;
        }
    }
    stringVal = malloc(numberOfEntries * sizeof(char *)) ;

    if (numberOfEntries != csv->numberOfEntries) {
        for (i = i2 = 0; i < csv->numberOfEntries; i++) {
            stringValLength = strlen(csv->stringVal[i]) ;
            if (stringValLength) {
                stringVal[i2] = allocStringOfLength(stringValLength) ;
                memcpy(stringVal[i2], csv->stringVal[i], stringValLength) ;
                i2++ ;
            }
            free(csv->stringVal[i]) ;
        }
        free(csv->stringVal) ;
        csv->stringVal = stringVal ;
        csv->numberOfEntries = numberOfEntries ;
    }
}

/* Read Comma Separated Values in a String and returns a CSV structure */
CSVStruct *readCSVInString(char *aString)
{
	int i, i0, N ;
	CSVStruct *csv ;

    csv = allocCSVStruct() ;

	N = 0 ;
	for(i = 0; i < strlen(aString); i++) {
		if(!strncmp(&aString[i], ",", 1))
			N++ ;
	}
	csv->numberOfEntries = N + 1 ;
	csv->stringVal = malloc(csv->numberOfEntries * sizeof(char *)) ;

	N = i0 = 0 ;
	for(i = 0; i < strlen(aString); i++) {
		while (!strncmp(&aString[i0], " ", 1)) {
			i0++ ;
			i++ ;
		}
		if(!strncmp(&aString[i], ",", 1)) {
			csv->stringVal[N] = allocStringOfLength(i - i0) ;
			memcpy(csv->stringVal[N], &aString[i0], i - i0) ;
			i0 = i + 1 ;
			N++ ;
		}
	}
	csv->stringVal[N] = (aString[i0])? initStringWithString(&aString[i0]) : initStringWithString("") ;

	return(csv) ;
}

/* Read White Char Seperated Values in a String and returns a CSV structure */
/* White chars are blank space or tab */
CSVStruct *readWhiteCharSeperatedValuesInString(char *aString)
{
	int i, i0, N ;
	CSVStruct *csv ;

    csv = allocCSVStruct() ;

	N = i = 0 ;
    while (!strncmp(&aString[i++], " ", 1)) {
    }
	for(; i < strlen(aString); i++) {
		if(!strncmp(&aString[i], " ", 1) || !strncmp(&aString[i], "\t", 1))
			N++ ;
		while (!strncmp(&aString[i], " ", 1)) {
			i++ ;
		}
	}
	csv->numberOfEntries = N + 1 ;
	csv->stringVal = malloc(csv->numberOfEntries * sizeof(char *)) ;

	N = i0 = i = 0 ;
    while (!strncmp(&aString[i0], " ", 1)) {
        i0++ ;
        i++ ;
    }
	for(; i < strlen(aString); i++) {
		if(!strncmp(&aString[i], " ", 1) || !strncmp(&aString[i], "\t", 1)) {
			csv->stringVal[N] = allocStringOfLength(i - i0) ;
			memcpy(csv->stringVal[N], &aString[i0], i - i0) ;
            while (!strncmp(&aString[++i], " ", 1)) {
            }
			i0 = i ;
			N++ ;
		}
	}
	csv->stringVal[N] = initStringWithString(&aString[i0]) ;

	return(csv) ;
}

CSVStruct *addStringValInCSVStruct(char *aStringValue, CSVStruct *csv)
{
    char **stringValue ;
    int i ;

    if(csv == NULL)
        csv = allocCSVStruct() ;

    csv->numberOfEntries++ ;
    stringValue = malloc(csv->numberOfEntries * sizeof(char *)) ;
    for (i = 0; i < csv->numberOfEntries - 1; i++) {
        stringValue[i] = initStringWithString(csv->stringVal[i]) ;
        free(csv->stringVal[i]) ;
    }
    stringValue[i] = initStringWithString(aStringValue) ;
    if(csv->stringVal)
        free(csv->stringVal) ;
    csv->stringVal = stringValue ;

    return (csv) ;
}

CSVStruct *concatenateCSVStructs(CSVStruct *csv1, CSVStruct *csv2)
{
    int i, i2 ;
    CSVStruct *csv ;
    
    csv = allocCSVStruct() ;
    csv->numberOfEntries = (csv1) ? csv1->numberOfEntries : 0 ;
    csv->numberOfEntries += (csv2) ? csv2->numberOfEntries : 0 ;
    csv->stringVal = malloc(csv->numberOfEntries * sizeof(char *)) ;
    for (i = 0; i < csv1->numberOfEntries; i++) {
        csv->stringVal[i] = initStringWithString(csv1->stringVal[i]) ;
    }
    for (i2 = 0; i < csv->numberOfEntries; i++, i2++) {
        csv->stringVal[i] = initStringWithString(csv2->stringVal[i2]) ;
    }
    
    return (csv) ;
}

char isStringValPresentInCSV(char *aStringValue, CSVStruct *csv)
{
    char isPresent ;
    int i ;

    isPresent = 0 ;

    if (csv) {
        for (i = 0; i < csv->numberOfEntries; i++) {
            if (!strcmp(aStringValue, csv->stringVal[i])) {
                isPresent = i + 1 ;
            }
        }
    }
    return (isPresent) ;
}

CSVStruct *removeStringValInCSVStruct(char *aStringValue, CSVStruct *csv)
{
    int i ;

    for (i = 0; i < csv->numberOfEntries; i++) {
        if (!strcmp(aStringValue, csv->stringVal[i])) {
            csv->stringVal[i][0] = '\0' ;
        }
    }
    removeEmptyValsInCSV(csv) ;

    return (csv) ;
}

CSVStruct *removeStringValAtIndexInCSVStruct(int i, CSVStruct *csv)
{
    csv->stringVal[i][0] = '\0' ;
    removeEmptyValsInCSV(csv) ;

    return (csv) ;
}

/* Allocate a Comma Separated Value structure */
CSVStruct *allocCSVStruct(void)
{
    CSVStruct *csv ;

    if((csv = malloc(sizeof(CSVStruct))) == NULL) {
		perror("Failed to allocate a CSV structure\n") ;
        exit(-1) ;
    }
    csv->numberOfEntries = 0;
    csv->stringVal = NULL ;

    return(csv) ;
}

/* Free a Comma Separated Value structure */
void freeCSVStruct(CSVStruct *csv)
{
	int i ;

    if(csv) {
        for(i = 0; i < csv->numberOfEntries; i++)
            free(csv->stringVal[i]) ;

        if (csv->numberOfEntries) {
            free(csv->stringVal) ;
        }

        free(csv) ;
        *(&csv) = NULL ;
    }
}
