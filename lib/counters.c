/*
 *  counters.c
 *  ersRAWReader
 *
 *  Created by Dominique Derauw on 7/09/06.
 *  Copyright 2006 Dominique Derauw All rights reserved.
 *
 */

#include "counters.h"


/* Little functions to create and display progress counters */

progressCounter *allocProgressCounter(void) 
{
	progressCounter *aProgressCounter ;
	
	if((aProgressCounter = malloc(sizeof(progressCounter))) == NULL)
		ase("Failed to allocate progress counter") ;
	return aProgressCounter ;
}

progressCounter *initProgressCounterWithMinMaxValue(int min, int max)
{
	progressCounter *aProgressCounter ;

	aProgressCounter = allocProgressCounter() ;
	aProgressCounter->output = stdout ;
	aProgressCounter->minIndex = min ;
	aProgressCounter->maxIndex = max ;
	aProgressCounter->gap = max-min ;
	aProgressCounter->formerValue = 0 ;
    aProgressCounter->displayIndex = 0 ;
	
	return aProgressCounter ;
}

void resetProgressCounter(progressCounter *aProgressCounter)
{
    aProgressCounter->formerValue = 0 ;
    aProgressCounter->displayIndex = 0 ;
}

int updateProgressCounterForIndex(progressCounter *p, int anIndex) 
{
	int percent ;
	
	p->currentIndex = anIndex ;
	percent = (p->gap)? 100 * (p->currentIndex - p->minIndex) / p->gap : 100;
	if(percent > p->formerValue) {
		fprintf(p->output, "%3d%% ", percent) ;
		fflush(p->output) ;
		p->formerValue = percent ;
        p->displayIndex++ ;
		if(p->displayIndex == 10) {
            fprintf(p->output, "\n") ;
            p->displayIndex = 0 ;
        }
	}
	
	return percent ;
}

int updatePointProgressCounterForIndex(progressCounter *p, int anIndex) 
{
	int fraction ;
	
	p->currentIndex = anIndex ;
	fraction = (p->gap)? 20 * (p->currentIndex - p->minIndex) / p->gap : 20 ;
	if(fraction > p->formerValue) {
		fprintf(p->output, ".") ;
		fflush(p->output) ;
		p->formerValue = fraction ;
		if(fraction == 20) fprintf(p->output, "\n") ;
	}
	
	return fraction ;
}

void freeProgressCounter(progressCounter *p) 
{
	free(p) ;
}
