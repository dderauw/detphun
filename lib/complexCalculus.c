/*
 *  complexCalculus.c
 *  decPol
 *
 *  Created by Dominique Derauw on Wed Sep 25 2002.
 *  Copyright (c) 2002 Dominique Derauw. All rights reserved.
 *
 */

#include "complexCalculus.h"

/* Multiplication d'un complexe (a.r + i a.i) par un réel */
complexFloat 	mRC(complexFloat a, float b)
{
    complexFloat	res ;
    
    res.r = a.r * b ;
    res.i = a.i * b ;
    
    return(res) ;
}

complexDouble 	mRComplexDouble(complexDouble a, double b)
{
    complexDouble	res ;
    
    res.r = a.r * b ;
    res.i = a.i * b ;
    
    return(res) ;
}

/* Multiplication complexe (a.r + i a.i) x (b.r + i b.i) */
complexFloat 	mC(complexFloat a, complexFloat b)
{
    complexFloat	res ;
    
    res.r = a.r * b.r - a.i * b.i ;
    res.i = a.r * b.i + a.i * b.r ;
    
    return(res) ;
}

complexDouble 	multComplexDouble(complexDouble a, complexDouble b)
{
    complexDouble	res ;
    
    res.r = a.r * b.r - a.i * b.i ;
    res.i = a.r * b.i + a.i * b.r ;
    
    return(res) ;
}

/* Addition complexe (a.r + i a.i) + (b.r + i b.r) */
complexFloat    aC(complexFloat a, complexFloat b)
{
    complexFloat    res ;
    
    res.r = a.r + b.r ;
    res.i = a.i + b.i ;
    
    return(res) ;
}

complexDouble sumComplexDouble(complexDouble a, complexDouble b)
{
    complexDouble    res ;
    
    res.r = a.r + b.r ;
    res.i = a.i + b.i ;
    
    return(res) ;
}

/* Soustraction complexe (a.r + i a.i) - (b.r + i b.r) */
complexFloat	sC(complexFloat a, complexFloat b)
{
    complexFloat	res ;
    
    res.r = a.r - b.r ;
    res.i = a.i - b.i ;
    
    return(res) ;
}

complexDouble subtComplexDouble(complexDouble a, complexDouble b)
{
    complexDouble	res ;
    
    res.r = a.r - b.r ;
    res.i = a.i - b.i ;
    
    return(res) ;
}

/* Complexe conjugué */
complexFloat	cC(complexFloat a)
{
    complexFloat	res ;
    
    res.r =  a.r ;
    res.i = -a.i ;
    
    return(res) ;
}


complexDouble	cCComplexDouble(complexDouble a)
{
    complexDouble	res ;
    
    res.r =  a.r ;
    res.i = -a.i ;
    
    return(res) ;
}


/* Module */
float modC(complexFloat a)
{
    return((float)sqrt(powf(a.r, 2.) + powf(a.i, 2.))) ;
}


double modComplexDouble(complexDouble a)
{
    return(sqrt(pow(a.r, 2.) + pow(a.i, 2.))) ;
}


/* Module au carré */
float sqmC(complexFloat a)
{
    return((float)(powf(a.r, 2.) + powf(a.i, 2.))) ;
}


double sqmComplexDouble(complexDouble a)
{
    return((pow(a.r, 2.) + pow(a.i, 2.))) ;
}


/* Phase of complex number */
float	phiC(complexFloat c)
{
	double	a, b, arg ;
	
	a = (double)c.r ;
	b = (double)c.i ;
	if(a!=0)
	{
		arg = atan(b/a) ;
		if(a<0) 
		{
			if(b>0)	arg += (double)PI ;
			else	arg -= (double)PI ;
		}
	}
	else
	{
		if(b>0) arg =  (double)HALFPI ;
		else 	arg = -(double)HALFPI ;
	}
	return (float)arg ;
}


/* Division complexe */
complexFloat divC(complexFloat a, complexFloat b) 
{
	float sqmB ;
	complexFloat res ;
	
	sqmB = sqmC(b) ;
	sqmB = (sqmB < 1.e-15)? 1.e-15 : 1. / sqmB ;
	
	res = mRC(mC(a, cC(b)), sqmB) ;
	return res ;
}


complexDouble divComplexDouble(complexDouble a, complexDouble b)
{
    double sqmB ;
    complexDouble res ;
    
    sqmB = sqmComplexDouble(b) ;
    sqmB = (sqmB < 1.e-15)? 1.e-15 : 1. / sqmB ;
    
    res = mRComplexDouble(multComplexDouble(a, cCComplexDouble(b)), sqmB) ;
    return res ;
}
