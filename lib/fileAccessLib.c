/*
 *  fileAccesLib.c
 *  trackDataReader
 *
 *  Created by Dominique Derauw on 3/02/05.
 *  Copyright 2005 Dominique Derauw All rights reserved.
 *
 */

#include "fileAccessLib.h"


char *getFileNameFromPath(char *aPath)
{
	char *fileName, *strippedPath ;
    size_t index ;

    if (!aPath) {
        return (aPath) ;
    }

	fileName = initStringWithString(getPointerToFileNameInPath(aPath)) ;
    if(!fileName) {
        strippedPath = initStringWithString(aPath) ;
        index = strlen(strippedPath) ;
        while (strippedPath[--index] == '/') {
            strippedPath[index] = 0 ;
        }
        fileName = initStringWithString(getPointerToFileNameInPath(strippedPath)) ;
        free(strippedPath) ;
    }

	return(fileName) ;
}

char *getPointerToFileNameInPath(char *path)
{
	int	i = 0, position = 0 ;

    if (path) {
        while(path[i]) {
            if(path[i] =='/') position = i ;
            i++ ;
        }

        if(position)
            return(&path[++position]) ;
        else
            return(path) ;

    }
    return (NULL) ;
}

char *getFileExtensionFromPath(char *aPath)
{
	char *extension ;

	extension = initStringWithString(getPointerToFileExtensionInPath(aPath)) ;

	return(extension) ;
}

char *getPointerToFileExtensionInPath(char *path)

{
	int	i = 0, position = 0 ;

	while(path[i]) {
		if(path[i] =='.') position = i ;
		i++ ;
	}

	if(position)
		return(&path[++position]) ;
	else
		return(NULL) ;
}

char *getPointerToLastFileExtensionInPath(char *path)
{
    long	i, position = 0 ;

    i = strlen(path) - 1 ;
    while(i) {
        if(path[i] =='.') {
            position = i ;
            break ;
        }
        i-- ;
    }

    if(position)
        return(&path[++position]) ;
    else
        return(path) ;
}

void stripExtensionAtEndOfPath(char *aPath)
{
    int	i = 0, position = 0 ;

    while(aPath[i]) {
        if(aPath[i] =='.') position = i ;
        i++ ;
    }

    if(position)
        aPath[position] = '\0' ;
}

void stripAllExtensions(char *aPath)
{
    int	i = 0 ;

    while(aPath[i] != '.' && aPath[i] ) {
        i++ ;
    }

    aPath[i] = '\0' ;
}

char *addExtensionAtEndOfPath(char *aPath, char *extension)
{
	char *aNewPath ;

	if(!strncmp(extension, ".", 1))
		aNewPath = initStringWith2Strings(aPath, extension) ;
	else
		aNewPath = initStringWith3Strings(aPath, ".", extension) ;

	free(aPath) ;

	return aNewPath ;
}

char *getDirectoryPathFromPath(char *path)
{
	char	*dir, *cwd, *fullPath ;
	size_t length = 0 ;
	int		i = 0 ;

	if(path == NULL) return(path) ;

	if(!strncmp(path, "./", 2)) {
		cwd = getcwd(NULL, MAXPATHLEN) ;
        fullPath = initStringWith2Strings(cwd, path + 1) ;
		free(cwd) ;
	}
	else {
        fullPath = initStringWithString(path) ;
	}

    while(fullPath[i]) {
        if(fullPath[i] =='/') length = i ;
        i++ ;
    }
    dir = allocStringOfLength(++length) ;
    memcpy(dir, fullPath, length) ;

    free(fullPath) ;

	return dir ;
}

char *getUpperDirectoryPath(char *aDirectoryPath)
{
    char *dir ;
    long unsigned length ;

    dir = initStringWithString(aDirectoryPath) ;
    length = strlen(aDirectoryPath) ;
    while (dir[length - 1] == '/') {
        dir[length - 1] = '\0' ;
        length -- ;
    }
    while (dir[length - 1] != '/' && length > 0) {
        dir[length - 1] = '\0' ;
        length -- ;
    }

    return(dir) ;
}

char isValidDirectoryForPath(char *path)
{
    if (isDir(path) || fileExistsAtPath(path)) {
        return(1) ;
    }

    return(0) ;
}

char isSymbolicLink(char *path)
{
    struct stat buffer ;

    if(path == NULL) return(0) ;
    if(isDir(path)) return(0) ;
    if(!lstat(path, &buffer)) {
        return(S_ISLNK(buffer.st_mode)) ;
    }

    return(0) ;
}

char fileExistsAtPath(char *path)
{
    struct stat buffer ;

    if(path == NULL) return(0) ;
    if(isDir(path)) return(0) ;
    stripWhiteCharsAtEndOfString(path) ;
    if(!stat(path, &buffer)) {
        if(buffer.st_mode & S_IFREG)
            return(1) ;
    }

    return(0) ;
}

char isDir(char *path)
{
	struct stat buffer ;

    if(path == NULL) return(0) ;
	if(!stat(path, &buffer)) {
		if(buffer.st_mode & S_IFDIR)
			return(1) ;
	}

	return 0 ;
}

char isWriteAccessGrantedAtPath(char *path)
{
	char *accessPath ;
	int nGroups, i ;
	uid_t myUID ;
	gid_t myGID, *groupList ;
	struct stat buffer ;

	myUID = getuid() ;

    if (!isDir(path)) {
        accessPath = getDirectoryPathFromPath(path) ;
        if(!isDir(accessPath)) {
            free(accessPath) ;
            return (0) ;
        }
    }
    else {
        accessPath = initStringWithString(path) ;
    }

	if(stat(accessPath, &buffer)) {
        free(accessPath) ;
		return(0) ;
	}
    free(accessPath) ;

	if(buffer.st_uid == myUID) {
		if(buffer.st_mode & S_IWUSR)
			return(1) ;
	}

	nGroups = getgroups(0, NULL) ;
	groupList = malloc(nGroups * sizeof(gid_t)) ;
	nGroups = getgroups(nGroups, groupList) ;
	for(i = 0; i < nGroups; i++) {
		myGID = groupList[i] ;
		if(buffer.st_gid == myGID) {
			if(buffer.st_mode & S_IWGRP)
				return(1) ;
		}
	}

	if(buffer.st_mode & S_IWOTH)
		return(1) ;

	free(groupList) ;
	return(0) ;
}

char isReadAccessGrantedAtPath(char *path)
{
	char *dir ;
	int nGroups, i ;
	uid_t myUID ;
	gid_t myGID, *groupList ;
	struct stat buffer ;

	myUID = getuid() ;

	if(!stat(path, &buffer)) {
		if(buffer.st_mode & S_IFREG) {
			if((dir = getDirectoryPathFromPath(path)) == NULL)
                return(0) ;

			if(stat(dir, &buffer)) {
				return(0) ;
			}
		}
        if((buffer.st_mode & S_IFDIR) == 0)
            return(0) ;
	}
	else {
		return(0) ;
	}

	if(buffer.st_uid == myUID) {
		if(buffer.st_mode & S_IRUSR)
			return(1) ;
	}

	nGroups = getgroups(0, NULL) ;
	groupList = malloc(nGroups * sizeof(gid_t)) ;
	nGroups = getgroups(nGroups, groupList) ;
	for(i = 0; i < nGroups; i++) {
		myGID = groupList[i] ;
		if(buffer.st_gid == myGID) {
            if(buffer.st_mode & S_IRGRP) {
                free(groupList) ;
				return(1) ;
            }
		}
	}

	free(groupList) ;
	if(buffer.st_mode & S_IROTH)
		return(1) ;

	return(0) ;
}

char isRWAccessGrantedAtPath(char *aPath)
{
    return (isReadAccessGrantedAtPath(aPath) & isWriteAccessGrantedAtPath(aPath)) ;
}

off_t getFileLengthAtPath(char *path) {
    struct stat buffer ;

    if(!stat(path, &buffer)) {
        if(buffer.st_mode & S_IFREG) {
            return(buffer.st_size) ;
        }
    }
    return(0) ;
}

CSVStruct *recursiveSearchOfDirInDir(char *dirName, char *dir)
{
    char *aPath ;
    char **directoryContent ;
    int N, i, j ;
    CSVStruct *csvOut = NULL, *aCSV = NULL ;

    directoryContent = getDirectoryContentAtPath(dir, &N) ;
    for (i = 0; i < N; i++) {
        aPath = initStringWith3Strings(dir, "/", directoryContent[i]) ;
        if (isDir(aPath)) {
            if (isSubStringPresentInString(directoryContent[i], dirName) || dirName[0] == '\0') {
                csvOut = addStringValInCSVStruct(aPath, csvOut) ;
            }
            if((aCSV = recursiveSearchOfDirInDir(dirName, aPath)) != NULL) {
                for (j = 0; j < aCSV->numberOfEntries; j++) {
                    csvOut = addStringValInCSVStruct(aCSV->stringVal[j], csvOut) ;
                }
                freeCSVStruct(aCSV) ;
            }
        }
        free(directoryContent[i]) ;
        free(aPath) ;
    }
    free(directoryContent) ;

    return (csvOut) ;
}


/* As its name states it, this function searches for file of given name within input directory */
/* File name can be partial */
CSVStruct *recursiveSearchOfFileInDir(char *fileName, char *dir)
{
    char *aPath ;
    char **directoryContent ;
    int N, i, j ;
    CSVStruct *csvOut = NULL, *aCSV = NULL ;

    directoryContent = getDirectoryContentAtPath(dir, &N) ;
    for (i = 0; i < N; i++) {
        aPath = initStringWith3Strings(dir, "/", directoryContent[i]) ;
        if (isDir(aPath)) {
            if((aCSV = recursiveSearchOfFileInDir(fileName, aPath)) != NULL) {
                for (j = 0; j < aCSV->numberOfEntries; j++) {
                    csvOut = addStringValInCSVStruct(aCSV->stringVal[j], csvOut) ;
                }
                freeCSVStruct(aCSV) ;
            }
        }
        else {
            if (isSubStringPresentInString(directoryContent[i], fileName)) {
                csvOut = addStringValInCSVStruct(aPath, csvOut) ;
            }
        }
        free(aPath) ;
        free(directoryContent[i]) ;
    }
    free(directoryContent) ;

    return (csvOut) ;
}

CSVStruct *recursiveSearchOfFilesWithExtensionInDir(char *extension, char *dir)
{
    char *aPath ;
    char **directoryContent ;
    int N, i, j ;
    CSVStruct *csvOut = NULL, *aCSV = NULL ;

    directoryContent = getDirectoryContentAtPath(dir, &N) ;
    for (i = 0; i < N; i++) {
        aPath = initStringWith3Strings(dir, "/", directoryContent[i]) ;
        if (isDir(aPath)) {
            if((aCSV = recursiveSearchOfFilesWithExtensionInDir(extension, aPath)) != NULL) {
                for (j = 0; j < aCSV->numberOfEntries; j++) {
                    csvOut = addStringValInCSVStruct(aCSV->stringVal[j], csvOut) ;
                }
                freeCSVStruct(aCSV) ;
            }
        }
        else {
            if (getPointerToFileExtensionInPath(directoryContent[i]) != NULL) {
                if (!strcmp(extension, getPointerToFileExtensionInPath(directoryContent[i]))) {
                    csvOut = addStringValInCSVStruct(aPath, csvOut) ;
                }
            }
        }
        free(directoryContent[i]) ;
        free(aPath) ;
    }
    free(directoryContent) ;

    return (csvOut) ;
}

char isFilePresentInSubDir(char *fileName, char *dir)
{
    char isOk = 0 ;
    CSVStruct *csv ;

    if((csv = recursiveSearchOfFileInDir(fileName, dir))) {
        isOk = 1 ;
        freeCSVStruct(csv) ;
    }

    return (isOk) ;
}

/* This function returns the content of a directory. The pointer to (int)N is updated with the number of entries found */
char **getDirectoryContentAtPath(char *aPath, int *numberOfEntries)
{
	char **dirList ;

    /* Get number of entries in directory */
	if(!(*numberOfEntries = getNumberOfDirectoryEntriesAtPath(aPath)))
		return((char **)NULL) ;

	/* Get entry list in directory */
    dirList = getDirectoryEntriesAtPath(aPath) ;

	return(dirList) ;
}

char **getDirectoryEntriesAtPath(char *aPath)
{
    char **entries, *path ;
    int i, numberOfEntries ;
    DIR *dirp ;
    struct dirent *entry ;

    /* Get number of entries in directory */
    if(!(numberOfEntries = getNumberOfDirectoryEntriesAtPath(aPath)))
        return((char **)NULL) ;

    /* Point to directory path */
    path = allocStringOfLength(strlen(aPath) + 2) ;
    sprintf(path, "%s/.", aPath) ;

    /* Open directory and return directory pointer */
    if((dirp = opendir(path)) == NULL) {
        fprintf(stderr,"Cannot open directory: %s\n", aPath) ;
        return((char **)NULL ) ;
    }

    /* Allocate entry set */
    if((entries = (char **)malloc(numberOfEntries * sizeof (char *))) == NULL) {
        fprintf(stderr, "Failed to allocate directory list") ;
        return((char **)NULL ) ;
    }
    rewinddir(dirp) ;
    i = 0 ;
    while ((entry = readdir(dirp)) != NULL) {
        if(strncmp(entry->d_name, ".", 1)) {
            if((entries[i] = initStringWithString(entry->d_name)) == NULL){
                fprintf(stderr, "Failed to allocate entry in directory list") ;
                return((char **)NULL) ;
            }
            i++ ;
        }
    }

    free(path) ;
    (void)closedir(dirp) ;

    return(entries) ;
}

void freeDirectoryEntriesAtPath(char **entries, char *aPath)
{
    int i, numberOfEntries ;

    /* Get number of entries in directory */
    if(!(numberOfEntries = getNumberOfDirectoryEntriesAtPath(aPath)))
        return ;

    for (i = 0; i < numberOfEntries; i++) {
        free(entries[i]) ;
    }
    free(entries) ;
}

char **getAllDirectoryEntriesAtPath(char *aPath)
{
    char **entries, *path ;
    int i, numberOfEntries ;
    DIR *dirp ;
    struct dirent *entry ;

    /* Get number of entries in directory */
    if(!(numberOfEntries = getNumberOfAllDirectoryEntriesAtPath(aPath)))
        return((char **)NULL) ;

    /* Point to directory path */
    path = allocStringOfLength(strlen(aPath) + 2) ;
    sprintf(path, "%s/.", aPath) ;

    /* Open directory and return directory pointer */
    if((dirp = opendir(path)) == NULL) {
        fprintf(stderr,"Cannot open directory: %s\n", aPath) ;
        return((char **)NULL ) ;
    }

    /* Allocate entry set */
    if((entries = (char **)malloc(numberOfEntries * sizeof (char *))) == NULL) {
        fprintf(stderr, "Failed to allocate directory list") ;
        return((char **)NULL ) ;
    }
    rewinddir(dirp) ;
    i = 0 ;
    while ((entry = readdir(dirp)) != NULL) {
        if(strncmp(entry->d_name, ".", 1)) {
            if((entries[i] = initStringWithString(entry->d_name)) == NULL){
                fprintf(stderr, "Failed to allocate entry in directory list") ;
                return((char **)NULL) ;
            }
            i++ ;
        }
        else {
            if(strlen(entry->d_name) > 1 && strncmp(entry->d_name + 1, ".", 1)) {
                if((entries[i] = initStringWithString(entry->d_name)) == NULL){
                    fprintf(stderr, "Failed to allocate entry in directory list") ;
                    return((char **)NULL) ;
                }
                i++ ;
            }
        }
    }

    free(path) ;
    (void)closedir(dirp) ;

    return(entries) ;
}

void freeDirectoryEntries(char **entries, int numberOfEntries)
{
    int i ;

    for (i = 0; i < numberOfEntries; i++) {
        free(entries[i]) ;
    }
    free(entries) ;
}

int getNumberOfDirectoryEntriesAtPath(char *aPath)
{
    char *path ;
    int numberOfEntries ;
    DIR *dirp ;
    struct dirent *dp ;

    path = allocStringOfLength(strlen(aPath) + 2) ;
    sprintf(path, "%s/.", aPath) ;

    numberOfEntries = 0 ;
    if((dirp = opendir(path)) == NULL) {
        return(0) ;
    }

    while ((dp = readdir(dirp)) != NULL) {
        if(strncmp(dp->d_name, ".", 1)) {
            numberOfEntries ++ ;
        }
    }

    free(path) ;
    (void)closedir(dirp) ;

    return(numberOfEntries) ;
}

int getNumberOfAllDirectoryEntriesAtPath(char *aPath)
{
    char *path ;
    int numberOfEntries ;
    DIR *dirp ;
    struct dirent *dp ;

    path = allocStringOfLength(strlen(aPath) + 2) ;
    sprintf(path, "%s/.", aPath) ;

    numberOfEntries = 0 ;
    if((dirp = opendir(path)) == NULL) {
        return(0) ;
    }

    while ((dp = readdir(dirp)) != NULL) {
        if(strncmp(dp->d_name, ".", 1)) {
            numberOfEntries ++ ;
        }
        else {
            if(strlen(dp->d_name) > 1 &&strncmp(dp->d_name + 1, ".", 1)) {
                numberOfEntries ++ ;
            }
        }
    }

    free(path) ;
    (void)closedir(dirp) ;

    return(numberOfEntries) ;
}

void removeDirectoryAtPath(char *aPath)
{
    char **entry, *fullPath ;
    int N, i ;

    if (!isDir(aPath)) {
        return ;
    }

    N = getNumberOfAllDirectoryEntriesAtPath(aPath) ;
    entry = getAllDirectoryEntriesAtPath(aPath) ;
    for (i = 0; i < N; i++) {
        fullPath = initStringWith3Strings(aPath, "/", entry[i]) ;
        if (isDir(fullPath)) {
            removeDirectoryAtPath(fullPath) ;
        }
        if (fileExistsAtPath(fullPath)) {
            unlink(fullPath) ;
        }
        free(fullPath) ;
    }
    if(rmdir(aPath)) {
        printf("Failed to remove %s directory\n", aPath) ;
        sprintf(ertex, "Failed to remove %s directory\n", aPath) ;
        ase(ertex) ;
    }
}


void copyFileAtPathToPath(char *inputPath, char *outputPath)
{
    char *buffer ;
    unsigned long nread, bufferSize ;
    FILE *inputFile, *outputFile ;

    if (!fileExistsAtPath(inputPath)) {
        return ;
    }
    
    if ((inputFile = fopen(inputPath, "r")) == NULL) {
        sprintf(ertex, "Failed to open input file: %s", inputPath) ;
        ase(ertex) ;
    }

    if ((outputFile = fopen(outputPath, "w")) == NULL) {
        sprintf(ertex, "Failed to create output file: %s", outputPath) ;
        ase(ertex) ;
    }

    bufferSize = _BUFFER_SIZE_ ;
    if((buffer = malloc(bufferSize)) == NULL)
        return ;
    while ((nread = fread(buffer, 1, bufferSize, inputFile)) == bufferSize) {
        fwrite(buffer, 1, bufferSize, outputFile);
    }
    if (feof(inputFile)) {
        fwrite(buffer, 1, nread, outputFile) ;
    }

    fclose(inputFile) ;
    fclose(outputFile) ;
}


/* Expend environment variables in path */
/* This function allows taking into account environment variables in paths */
char *expendEnvironmentVariablesInPath(char *aPath)
{
    char *fullPath = NULL, *currentPath = NULL, *aString  ;
    int i ;
    CSVStruct *csv ;

    if((aString = initStringWithString(aPath))) {
        replaceNSubStringInString(aString, "/", ",") ;
    //    aString[strlen(aString) - 1] = '32' ;

        csv = readCSVInString(aString) ;
        removeEmptyValsInCSV(csv) ;
        free(aString) ;

        for (i = 0; i < csv->numberOfEntries; i++) {
            aString = csv->stringVal[i] ;
            if(!strncmp(csv->stringVal[i], "$", 1)) {
                removeAnyOccurrenceOfSubStringInString(aString, "(") ;
                removeAnyOccurrenceOfSubStringInString(aString, ")") ;
                if (!(aString = getenv(csv->stringVal[i] + 1))) {
                    aString = csv->stringVal[i] ;
                }
            }

            if(!i) {
                if (!strncmp(aString, ".", 1)) {
                    fullPath = initStringWithString(getenv("PWD")) ;
                }
                else {
                    if (!strncmp(aString, "/", 1)) {
                        fullPath = initStringWithString(aString) ;
                    }
                    else {
                        fullPath = initStringWith2Strings("/", aString) ;
                    }
                }
            }
            else {
                fullPath = initStringWith3Strings(currentPath, "/", aString) ;
                free(currentPath) ;
            }
            currentPath = initStringWithString(fullPath) ;
        }
        free(currentPath) ;
        freeCSVStruct(csv) ;
    }

    return(fullPath) ;
}
