//
//  rawFileLib.c
//  EnviSATDataReader
//
//  Created by Dominique Derauw on 30/06/16.
//
//

#include "rawFileLib.h"

rawFileDescriptor *allocFileDescriptor(void)
{
    rawFileDescriptor *aFileDescriptor ;

    if((aFileDescriptor = malloc(sizeof(rawFileDescriptor))) == NULL)
        return((rawFileDescriptor *)NULL) ;
    aFileDescriptor->f = NULL ;
    aFileDescriptor->accessPath = NULL ;
    aFileDescriptor->directoryPath = NULL ;
    aFileDescriptor->fileName = NULL ;
    aFileDescriptor->fileLength = 0 ;
    aFileDescriptor->mode = allocStringOfLength(3) ;
    aFileDescriptor->mode[0] = '\0' ;
    aFileDescriptor->byteOrder = isArchBigEndian() ;
    aFileDescriptor->xDim = 0 ;
    aFileDescriptor->yDim = 0 ;
    aFileDescriptor->xLowerIndex = 0 ;
    aFileDescriptor->yLowerIndex = 0 ;
    aFileDescriptor->x0 = 0. ;
    aFileDescriptor->y0 = 0. ;
    aFileDescriptor->xSampling = 0. ;
    aFileDescriptor->ySampling = 0. ;
    aFileDescriptor->excludingValue = NULL ;
    aFileDescriptor->type = (size_t)0 ;
    aFileDescriptor->aoi = NULL ;
    aFileDescriptor->data = NULL ;
    aFileDescriptor->indexedData = NULL ;

    return (aFileDescriptor) ;
}

rawFileDescriptor *allocFileDescriptorForPath(char *aPath)
{
    rawFileDescriptor *aFileDescriptor ;

	aPath = expendEnvironmentVariablesInPath(aPath) ;
    aFileDescriptor = allocFileDescriptor() ;

    aFileDescriptor->accessPath = initStringWithString(aPath) ;
    aFileDescriptor->directoryPath = getDirectoryPathFromPath(aPath) ;
    aFileDescriptor->fileName = getFileNameFromPath(aPath) ;

    free(aPath) ;

    return (aFileDescriptor) ;
}

void setRawFilePath(rawFileDescriptor *aFileDescriptor, char *newFullPath)
{
    if (aFileDescriptor->accessPath) {
        free(aFileDescriptor->accessPath) ;
        free(aFileDescriptor->directoryPath) ;
        free(aFileDescriptor->fileName) ;

        aFileDescriptor->accessPath = initStringWithString(newFullPath) ;
        aFileDescriptor->directoryPath = getDirectoryPathFromPath(newFullPath) ;
        aFileDescriptor->fileName = getFileNameFromPath(newFullPath) ;
    }
}

void setRawFileDimensions(rawFileDescriptor *aFileDescriptor, int xDim, int yDim)
{
    aFileDescriptor->xDim = xDim ;
    aFileDescriptor->yDim = yDim ;
}

void setRawFileLowerIndexes(rawFileDescriptor *aFileDescriptor, int xLowerIndex, int yLowerIndex)
{
    aFileDescriptor->xLowerIndex = xLowerIndex ;
    aFileDescriptor->yLowerIndex = yLowerIndex ;
}

void setRawFileType(rawFileDescriptor *aFileDescriptor, size_t type)
{
    aFileDescriptor->type = type ;
}

void setRawFileOrigin(rawFileDescriptor *aFileDescriptor, int x0, int y0)
{
    aFileDescriptor->x0 = x0 ;
    aFileDescriptor->y0 = y0 ;
}

void setRawFileSampling(rawFileDescriptor *aFileDescriptor, int xSampling, int ySampling)
{
    aFileDescriptor->xSampling = xSampling ;
    aFileDescriptor->ySampling = ySampling ;
}
char openRawFileForReading(rawFileDescriptor *aFileDescriptor)
{
    if (aFileDescriptor == NULL) {
        return (0) ;
    }
    if (!fileExistsAtPath(aFileDescriptor->accessPath)) {
        return (0) ;
    }
    if(!isReadAccessGrantedAtPath(aFileDescriptor->accessPath))
        return(0) ;

    if (aFileDescriptor->directoryPath == NULL) {
        aFileDescriptor->directoryPath = getDirectoryPathFromPath(aFileDescriptor->accessPath) ;
    }
    if (aFileDescriptor->fileName == NULL) {
        aFileDescriptor->fileName = getFileNameFromPath(aFileDescriptor->accessPath) ;
    }
    if (aFileDescriptor->f) {
        fclose(aFileDescriptor->f) ;
        aFileDescriptor->f = NULL ;
    }
    if (aFileDescriptor->f == NULL ) {
        aFileDescriptor->f = fopen(aFileDescriptor->accessPath, "r") ;
    }

    fseeko(aFileDescriptor->f, (off_t)0, SEEK_END) ;
    aFileDescriptor->fileLength = ftell(aFileDescriptor->f) ;
    fseeko(aFileDescriptor->f, (off_t)0, SEEK_SET) ;
    sprintf(aFileDescriptor->mode, "r") ;
    clearerr(aFileDescriptor->f) ;

    return (1) ;
}

char openRawFileForWriting(rawFileDescriptor *aFileDescriptor)
{
    if (aFileDescriptor == NULL) {
        return (0) ;
    }
    if(!isWriteAccessGrantedAtPath(aFileDescriptor->accessPath))
        return(0) ;

    if (aFileDescriptor->directoryPath == NULL) {
        aFileDescriptor->directoryPath = getDirectoryPathFromPath(aFileDescriptor->accessPath) ;
    }
    if (aFileDescriptor->fileName == NULL) {
        aFileDescriptor->fileName = getFileNameFromPath(aFileDescriptor->accessPath) ;
    }
    if (aFileDescriptor->f == NULL ) {
        if((aFileDescriptor->f = fopen(aFileDescriptor->accessPath, "w")) == NULL) {
            return(0) ;
        }
    }

    fseeko(aFileDescriptor->f, (off_t)0, SEEK_SET) ;
    sprintf(aFileDescriptor->mode, "w") ;
    clearerr(aFileDescriptor->f) ;

    return (1) ;
}

char openRawFileForReadingAndWriting(rawFileDescriptor *aFileDescriptor)
{
    if (aFileDescriptor == NULL) {
        return (0) ;
    }
    if (!fileExistsAtPath(aFileDescriptor->accessPath)) {
        return (0) ;
    }
    if(!isReadAccessGrantedAtPath(aFileDescriptor->accessPath) || !isWriteAccessGrantedAtPath(aFileDescriptor->accessPath))
        return(0) ;

    if (aFileDescriptor->directoryPath == NULL) {
        aFileDescriptor->directoryPath = getDirectoryPathFromPath(aFileDescriptor->accessPath) ;
    }
    if (aFileDescriptor->fileName == NULL) {
        aFileDescriptor->fileName = getFileNameFromPath(aFileDescriptor->accessPath) ;
    }
    if (aFileDescriptor->f == NULL ) {
        if(fileExistsAtPath(aFileDescriptor->accessPath)) {
            aFileDescriptor->f = fopen(aFileDescriptor->accessPath, "r+") ;
            sprintf(aFileDescriptor->mode, "r+") ;
            fseeko(aFileDescriptor->f, (off_t)0, SEEK_END) ;
            aFileDescriptor->fileLength = ftell(aFileDescriptor->f) ;
        }
        else {
            if((aFileDescriptor->f = fopen(aFileDescriptor->accessPath, "w+")) == NULL) {
                return (0) ;
            }
            aFileDescriptor->fileLength = 0 ;
            sprintf(aFileDescriptor->mode, "w+") ;
        }
    }
    fseeko(aFileDescriptor->f, (off_t)0, SEEK_SET) ;
    clearerr(aFileDescriptor->f) ;

    return (1) ;
}

rawFileDescriptor *openRawFileForReadingAtPath(char *aPath)
{
    rawFileDescriptor	*aFileDescriptor ;

    if (aPath == NULL) {
        return((rawFileDescriptor *)NULL) ;
    }

	aPath = expendEnvironmentVariablesInPath(aPath) ;
    if(isDir(aPath)) {
        fprintf(stdout, "Given path is a Directory, not a file to be created or opened!\n--> %s\n", aPath) ;
        free(aPath) ;
        return((rawFileDescriptor *)NULL) ;
    }
    if(!fileExistsAtPath(aPath)) {
        free(aPath) ;
        return((rawFileDescriptor *)NULL) ;
    }
    else {
        if(!isReadAccessGrantedAtPath(aPath)) {
            free(aPath) ;
            return((rawFileDescriptor *)NULL) ;
        }
        aFileDescriptor = allocFileDescriptor() ;

        aFileDescriptor->directoryPath = getDirectoryPathFromPath(aPath) ;
        aFileDescriptor->fileName = getFileNameFromPath(aPath) ;
        aFileDescriptor->accessPath = allocStringOfLength(strlen(aFileDescriptor->directoryPath) + strlen(aFileDescriptor->fileName)) ;
        sprintf(aFileDescriptor->accessPath, "%s%s", aFileDescriptor->directoryPath, aFileDescriptor->fileName) ;
        if ((aFileDescriptor->f = fopen(aFileDescriptor->accessPath, "r")) == NULL) {
            free(aPath) ;
            ase("Failed to open file: ") ;
        }
        fseeko(aFileDescriptor->f, (off_t)0, SEEK_END) ;
        aFileDescriptor->fileLength = ftell(aFileDescriptor->f) ;
        fseeko(aFileDescriptor->f, (off_t)0, SEEK_SET) ;
        sprintf(aFileDescriptor->mode, "r") ;
    }
    clearerr(aFileDescriptor->f) ;
    free(aPath) ;

    return aFileDescriptor ;
}

rawFileDescriptor *openRawFileForWritingAtPath(char *aPath)
{
    rawFileDescriptor	*aFileDescriptor ;

    if (aPath == NULL) {
        return((rawFileDescriptor *)NULL) ;
    }

	aPath = expendEnvironmentVariablesInPath(aPath) ;
    if(isDir(aPath)) {
        fprintf(stdout, "Given path is a Directory, not a file to be created or opened!\n") ;
        free(aPath) ;
        return((rawFileDescriptor *)NULL) ;
    }
    if(fileExistsAtPath(aPath)) {
        if(!isWriteAccessGrantedAtPath(aPath)) {
            free(aPath) ;
            return((rawFileDescriptor *)NULL) ;
        }
    }

    aFileDescriptor = allocFileDescriptor() ;

    aFileDescriptor->directoryPath = getDirectoryPathFromPath(aPath) ;
    aFileDescriptor->fileName = getFileNameFromPath(aPath) ;
    aFileDescriptor->accessPath = allocStringOfLength(strlen(aFileDescriptor->directoryPath) + strlen(aFileDescriptor->fileName)) ;
    sprintf(aFileDescriptor->accessPath, "%s%s", aFileDescriptor->directoryPath, aFileDescriptor->fileName) ;
    aFileDescriptor->f = fopen(aFileDescriptor->accessPath, "w") ;
    if(aFileDescriptor->f == NULL) {
        freeRawFileDescriptor(aFileDescriptor) ;
        free(aPath) ;
        return((rawFileDescriptor *)NULL) ;
    }

    aFileDescriptor->fileLength = 0 ;
    fseeko(aFileDescriptor->f, (off_t)0, SEEK_SET) ;
    sprintf(aFileDescriptor->mode, "w") ;

    clearerr(aFileDescriptor->f) ;
    free(aPath) ;

    return aFileDescriptor ;
}


rawFileDescriptor *openRawFileForReadingAndWritingAtPath(char *aPath)
{
    char *aDir ;
    rawFileDescriptor	*aFileDescriptor ;

    if (aPath == NULL) {
        return((rawFileDescriptor *)NULL) ;
    }

	aPath = expendEnvironmentVariablesInPath(aPath) ;
    if(isDir(aPath)) {
        fprintf(stdout, "Given path is a Directory, not a file to be created or opened!\n") ;
        free(aPath) ;
        return((rawFileDescriptor *)NULL) ;
    }
    if(fileExistsAtPath(aPath)) {
        if(!isWriteAccessGrantedAtPath(aPath) || !isReadAccessGrantedAtPath(aPath)) {
            free(aPath) ;
            return((rawFileDescriptor *)NULL) ;
        }
    }
    else {
        aDir = getDirectoryPathFromPath(aPath) ;
        if(!isWriteAccessGrantedAtPath(aDir) || !isReadAccessGrantedAtPath(aDir)) {
            free(aDir) ;
            free(aPath) ;
            return((rawFileDescriptor *)NULL) ;
        }
    }

    aFileDescriptor = allocFileDescriptor() ;

    aFileDescriptor->directoryPath = getDirectoryPathFromPath(aPath) ;
    aFileDescriptor->fileName = getFileNameFromPath(aPath) ;
    aFileDescriptor->accessPath = initStringWith2Strings(aFileDescriptor->directoryPath, aFileDescriptor->fileName) ;
    if(fileExistsAtPath(aFileDescriptor->accessPath)) {
        aFileDescriptor->f = fopen(aPath, "r+") ;
        fseeko(aFileDescriptor->f, (off_t)0, SEEK_END) ;
        aFileDescriptor->fileLength = ftell(aFileDescriptor->f) ;
        fseeko(aFileDescriptor->f, (off_t)0, SEEK_SET) ;
        sprintf(aFileDescriptor->mode, "r+") ;
    }
    else {
        aFileDescriptor->f = fopen(aPath, "w+") ;
        aFileDescriptor->fileLength = 0 ;
        fseeko(aFileDescriptor->f, (off_t)0, SEEK_SET) ;
        sprintf(aFileDescriptor->mode, "w+") ;
    }

    clearerr(aFileDescriptor->f) ;
    free(aPath) ;

    return aFileDescriptor ;
}

rawFileDescriptor *createRawFileForReadingAndWritingAtPath(char *aPath)
{
    char *aDir ;
    rawFileDescriptor	*aFileDescriptor ;

    if (aPath == NULL) {
        return((rawFileDescriptor *)NULL) ;
    }

 	aPath = expendEnvironmentVariablesInPath(aPath) ;
    if(isDir(aPath)) {
        fprintf(stdout, "Given path is a Directory, not a file to be created or opened!\n") ;
        free(aPath) ;
        return((rawFileDescriptor *)NULL) ;
    }
    if(fileExistsAtPath(aPath)) {
        if(!isWriteAccessGrantedAtPath(aPath) || !isReadAccessGrantedAtPath(aPath)) {
            free(aPath) ;
            return((rawFileDescriptor *)NULL) ;
        }
        unlink(aPath) ;
    }
    else {
        aDir = getDirectoryPathFromPath(aPath) ;
        if(!isWriteAccessGrantedAtPath(aDir) || !isReadAccessGrantedAtPath(aDir)) {
            free(aPath) ;
            return((rawFileDescriptor *)NULL) ;
        }
        free(aDir) ;
    }

    aFileDescriptor = allocFileDescriptor() ;

    aFileDescriptor->directoryPath = getDirectoryPathFromPath(aPath) ;
    aFileDescriptor->fileName = getFileNameFromPath(aPath) ;
    aFileDescriptor->accessPath = initStringWith2Strings(aFileDescriptor->directoryPath, aFileDescriptor->fileName) ;
    aFileDescriptor->f = fopen(aPath, "w+") ;
    fseeko(aFileDescriptor->f, (off_t)0, SEEK_SET) ;
    sprintf(aFileDescriptor->mode, "w+") ;

    free(aPath) ;
    clearerr(aFileDescriptor->f) ;

    return aFileDescriptor ;
}

void	freeRawFileDescriptor(rawFileDescriptor *aFileDescriptor)
{
    if(aFileDescriptor != NULL) {
        if (fileExistsAtPath(aFileDescriptor->accessPath)) {
            closeRawFile(aFileDescriptor) ;
        }
        if(aFileDescriptor->accessPath != NULL)
            free(aFileDescriptor->accessPath) ;
        if(aFileDescriptor->directoryPath != NULL)
            free(aFileDescriptor->directoryPath) ;
        if(aFileDescriptor->fileName != NULL)
            free(aFileDescriptor->fileName) ;
        if (aFileDescriptor->excludingValue) {
            free(aFileDescriptor->excludingValue) ;
        }
        if (aFileDescriptor->indexedData && aFileDescriptor->type) {
            freeMatrixOfType(aFileDescriptor->indexedData, aFileDescriptor->yLowerIndex, aFileDescriptor->xLowerIndex, aFileDescriptor->type) ;
        }
        free(aFileDescriptor->mode) ;
        free(aFileDescriptor) ;
    }
}

void closeRawFile(rawFileDescriptor *aFileDescriptor)
{
    if(aFileDescriptor->f && fileExistsAtPath(aFileDescriptor->accessPath)) {
        fflush(aFileDescriptor->f) ;
        fclose(aFileDescriptor->f) ;
        aFileDescriptor->f = NULL ;
    }
}

