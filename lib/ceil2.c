
/* ceil2.c */
/*---------------------------------------------------------------------------------------------------*/
/* Fonction retournant la plus grande puissance de 2 supérieure au nombre donne. */
/*
 *  Created by Dominique Derauw a long time ago.
 *  Copyright (c) Dominique Derauw. All rights reserved.
 *
 */

#include "ceil2.h"


void	ceil2(int *ptr_nb, int *ptr_expnb)
{
	/* definition of structure corresponding to a float */
#ifdef __ppc__
	typedef struct	{
			unsigned s : 1 ;
			unsigned e : 8 ;
			unsigned m : 23;
			} fl ;
#else
	typedef struct	{
			unsigned m : 23;
			unsigned e : 8 ;
			unsigned s : 1 ;
			} fl ;
#endif

	/* definition of variables */

	fl		a;

	union 		{
			fl 	structfloat;
			float 	tempfloat;
			} u ;

	u.tempfloat = (float)*ptr_nb;		/* Passage d'entier a float */
	a = u.structfloat;			/* Lecture du float structure */
	if(a.m != 0)
	{
		a.m = 0;				/* Mise a zero de la partie fractionnaire */
		u.structfloat = a;
		*ptr_expnb = (int) (a.e - 126);		/* Lecture de l'exposant et passage de char a integer*/
		*ptr_nb = (int)(2 * u.tempfloat);	/* Lecture de la puissance de 2 recherchee et passage a entier. */
	}
	else	*ptr_expnb = (int) (a.e - 127);	
}

int ceil2ofIntVal(int anInt)
{
	int anExponent ;
	
	ceil2(&anInt, &anExponent) ;
	
	return anInt ;
}

int ceil2ofFloatVal(float aFloat)
{
	int anInt, anExponent ;
	
	anInt = (int)ceilf(aFloat) ;
	ceil2(&anInt, &anExponent) ;
	
	return anInt ;
}
