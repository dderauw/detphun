/*
 *  planeGeometry.c
 *  initInSAR
 *
 *  Created by Dominique Derauw on 1/11/11.
 *  Copyright 2011 Dominique Derauw. All rights reserved.
 *
 */

#include "planeGeometry.h"

polygon *allocPolygone(int numberOfAngles)
{
	int	i ;
	polygon *aPolygon ;

    if((aPolygon = malloc(sizeof(polygon))) == NULL) {
		perror("Failed to allocate a polygon") ;
        exit(-1 );
    }
	aPolygon->N = numberOfAngles ;

    if((aPolygon->P = malloc(aPolygon->N * sizeof(coord2D))) == NULL) {
		perror("Failed to allocate a quadrilateral") ;
        exit(-1 );
    }

    if((aPolygon->V = malloc(aPolygon->N * sizeof(double *))) == NULL) {
		perror("Failed to allocate a quadrilateral") ;
        exit(-1 );
    }

	for(i = 0; i < aPolygon->N; i++)
		aPolygon->V[i] = (double *)&aPolygon->P[i].x ;

	return(aPolygon) ;
}

quadrilateral *allocQuadrilateral(void)
{
    return(allocPolygone(4)) ;
}

void freePolygone(polygon *aPolygone)
{
	free(aPolygone->P) ;
	free(aPolygone->V) ;
	free(aPolygone) ;
}

