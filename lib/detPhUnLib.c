//
//  detPhUnLib.c
//  detPhUn
//
//  Created by Dominique Derauw on 30/08/12.
//  Copyright (c) 2012 Dominique Derauw. All rights reserved.
//

#include <stdio.h>
#include "detPhUnLib.h"
double m3x3Determinant(double **M) ;
double **m3x3Inversion(double **M) ;
double *multMatVect(double **M1, double *V1, int rank) ;


void deterministicPhaseUnwrapping(keyValue *params)
{
    char *aFilePath, *aNewFilePath, *filteredPhaseResidue ;
    char withFiltering ;
    int i, xTileIndex = 0, yTileIndex ;
    int filteringFlags ;
    int numberOfIterations, mode ;
    int xTileSize, yTileSize, xTileBorderSize, yTileBorderSize ;
    double normalizationFactor, coherenceThreshold ;
    double xFWHM, yFWHM, smoothingFactor ;
    rawFileDescriptor *interferogramFile, *unwrappedPhaseFile, *coherenceFile, *phaseResidue, *lastPhaseResidue = NULL ;
    tileTool *interfTileTool, *currentPhaseTileTool, *residualPhaseTileTool, *unwrappedPhaseTileTool, *coherenceTileTool = NULL ;
    detphun *gP ;
    progressCounter *aCounter ;
    keyValue *filterigParameters = NULL ;
    
    
    /* Open input interferogram */
    if ((interferogramFile = openRawFileForReadingAtPath(getStringValForKeyIn(params, "Interferogram file path"))) == NULL) {
        ase("Interferogram file cannot be opened") ;
    }
    interferogramFile->xDim = getIntValForKeyIn(params, "X dim") ;
    interferogramFile->yDim = getIntValForKeyIn(params, "Y dim") ;

    /* Create unwrapped phase file for output */
    if ((unwrappedPhaseFile = openRawFileForReadingAndWritingAtPath(getStringValForKeyIn(params, "Output"))) == NULL) {
        ase("Failed to create output file") ;
    }
    unwrappedPhaseFile->xDim = interferogramFile->xDim ;
    unwrappedPhaseFile->yDim = interferogramFile->yDim ;
    
    /* Open Coherence file if provided */
    coherenceThreshold = 0. ;
    coherenceFile = NULL ;
    aFilePath = getStringValForKeyIn(params, "Coherence im") ;
    if (fileExistsAtPath(aFilePath)) {
        coherenceFile = openRawFileForReadingAtPath(aFilePath) ;
        coherenceFile->xDim = interferogramFile->xDim ;
        coherenceFile->yDim = interferogramFile->yDim ;
        coherenceThreshold = getDoubleValForKeyIn(params, "Coherence th") ;
    }
    
    /* Create phase residue file */
    aFilePath = initStringWith2Strings(unwrappedPhaseFile->accessPath, ".phaseResidue") ;
    phaseResidue = openRawFileForReadingAndWritingAtPath(aFilePath) ;
    phaseResidue->xDim = interferogramFile->xDim ;
    phaseResidue->yDim = interferogramFile->yDim ;
    free(aFilePath) ;
    
    /* Create temp file for iterative processing */
    numberOfIterations = getIntValForKeyIn(params, "Number") ;
    if (numberOfIterations > 1) {
        aFilePath = initStringWith2Strings(interferogramFile->accessPath, ".lastPhaseResidue") ;
        lastPhaseResidue = openRawFileForReadingAndWritingAtPath(aFilePath) ;
        lastPhaseResidue->xDim = interferogramFile->xDim ;
        lastPhaseResidue->yDim = interferogramFile->yDim ;
        free(aFilePath) ;
    }
    
    /* Read sub-window parameters */
    xTileSize = getIntValForKeyIn(params, "X window") ;
    yTileSize = getIntValForKeyIn(params, "Y window") ;
    xTileBorderSize = getIntValForKeyIn(params, "X border") ;
    yTileBorderSize = getIntValForKeyIn(params, "Y border") ;
    
    if (xTileSize > interferogramFile->xDim || xTileSize < 32) {
        xTileSize = interferogramFile->xDim ;
        xTileBorderSize = 0 ;
    }
    if (yTileSize > interferogramFile->yDim || yTileSize < 32) {
        yTileSize = interferogramFile->yDim ;
        yTileBorderSize = 0 ;
    }

    /* Alloc tile tool for input interferogram */
    interfTileTool = allocTileToolForFilesOfType(interferogramFile, unwrappedPhaseFile, "float") ;
    initTileToolWithSizes(interfTileTool, xTileSize, yTileSize, xTileBorderSize, yTileBorderSize) ;
    
    currentPhaseTileTool = allocTileToolForFilesOfType(interferogramFile, unwrappedPhaseFile, "float") ;
    initTileToolWithSizes(currentPhaseTileTool, xTileSize, yTileSize, xTileBorderSize, yTileBorderSize) ;
    
    /* Alloc tile tool for coherence management */
    if (coherenceFile) {
        coherenceTileTool = allocTileToolForFilesOfType(coherenceFile, NULL, "float") ;
        initTileToolWithSizes(coherenceTileTool, xTileSize, yTileSize, xTileBorderSize, yTileBorderSize) ;
    }

    /* Alloc tile tool for iterative unwrapping */
    residualPhaseTileTool = allocTileToolForFilesOfType(phaseResidue, phaseResidue, "float") ;
    initTileToolWithSizes(residualPhaseTileTool, xTileSize, yTileSize, xTileBorderSize, yTileBorderSize) ;
    
    unwrappedPhaseTileTool = allocTileToolForFilesOfType(unwrappedPhaseFile, unwrappedPhaseFile, "float") ;
    initTileToolWithSizes(unwrappedPhaseTileTool, xTileSize, yTileSize, xTileBorderSize, yTileBorderSize) ;
    
    /* Filtering initialization */
    xFWHM = getDoubleValForKeyIn(params, "X FWHM") ;
    yFWHM = getDoubleValForKeyIn(params, "Y FWHM") ;
    smoothingFactor = getDoubleValForKeyIn(params, "Gold") ;
    if (smoothingFactor && !coherenceFile) {
        ase("Please, provide a coherence file for Goldstein filtering") ;
    }
    withFiltering = ((xFWHM && yFWHM) || smoothingFactor) ;
    filteringFlags = 0 ;
    if (withFiltering) {
        filteringFlags |= _DETPHUN_WITH_FILTERING_ ;
        if ((xFWHM && yFWHM) && smoothingFactor) {
            filteringFlags |= _COMBINED_FILTERINGS_ ;
        }
        filterigParameters = allocAKeyValuePair() ;
        filteredPhaseResidue = initStringWith2Strings(unwrappedPhaseTileTool->input->directoryPath, "tempFile") ;
        setStringValForKeyIn(filterigParameters, interfTileTool->input->accessPath, "Input file") ;
        setStringValForKeyIn(filterigParameters, phaseResidue->accessPath, "Output file") ;
        setStringValForKeyIn(filterigParameters, coherenceTileTool->input->accessPath, "Coherence file") ;
        setIntValForKeyIn(filterigParameters, interferogramFile->xDim, "xDim") ;
        setIntValForKeyIn(filterigParameters, interferogramFile->yDim, "yDim") ;
        setDoubleValForKeyIn(filterigParameters, xFWHM, "Sigma X") ;
        setDoubleValForKeyIn(filterigParameters, yFWHM, "Sigma Y") ;
        setDoubleValForKeyIn(filterigParameters, smoothingFactor, "Smoothing factor") ;
        setIntValForKeyIn(filterigParameters, filteringFlags, "Flags") ;
        setDoubleValForKeyIn(filterigParameters, 1, "Power argument") ;
        setStringValForKeyIn(filterigParameters, filteredPhaseResidue, "Output file") ;
    }
    
    /* Deterministic phase unwrapping initialization */
    mode = (strncmp(getStringValForKeyIn(params, "Mode"), "detPhUn1", 8) == 0)? _DETPHUN1_ : _DETPHUN2_ ;
    gP = allocAndInitDetphunWithWindowSize(xTileSize, yTileSize, mode) ;
    gP->initialPhase = (float **)interfTileTool->tile ;
    gP->currentPhase = (float **)currentPhaseTileTool->tile ;
    gP->dephi = (float **)unwrappedPhaseTileTool->tile ;
    gP->coh = NULL ;
    if (coherenceFile) {
        gP->coh = (float **)coherenceTileTool->tile ;
    }
    gP->iterationNumber = numberOfIterations ;
    gP->coherenceThreshold = coherenceThreshold ;
    gP->removePhasePlane = (strncmp(getStringValForKeyIn(params, "Flattening"), "YES", 3) == 0)? 1 : 0 ;
    gP->removePhasePlane = (xTileSize == interferogramFile->xDim)? gP->removePhasePlane : 0 ;
    
    for(i = 0; i < gP->iterationNumber; i++) {
        if (gP->iterationNumber > 1) {
            printf("\nIteration %d\n", i + 1) ;
        }
        if (i > 0) {
            if (withFiltering) {
                printf("Phase filtering\n") ;
                setStringValForKeyIn(filterigParameters, phaseResidue->accessPath, "Input file") ;
                interferogramFiltering(filterigParameters) ;
                
                closeRawFile(phaseResidue) ;
                unlink(phaseResidue->accessPath) ;
                aFilePath = getStringValForKeyIn(filterigParameters, "Output file") ;
                rename(getStringValForKeyIn(filterigParameters, "Output file"), phaseResidue->accessPath) ;
                openRawFileForReadingAndWriting(phaseResidue) ;
            }
            currentPhaseTileTool->input = phaseResidue ;
            phaseResidue = lastPhaseResidue ;
        }
        aCounter = initProgressCounterWithMinMaxValue(0, interfTileTool->xTileNumber * interfTileTool->yTileNumber) ;
        printf("Deterministic phase unwrapping\n") ;
        
        for(yTileIndex = 0; yTileIndex < interfTileTool->yTileNumber; yTileIndex++) {
            for(xTileIndex = 0; xTileIndex < interfTileTool->xTileNumber; xTileIndex++) {
                resetDetphun(gP) ;
                readTileAtIndexes(interfTileTool, xTileIndex, yTileIndex) ;
                readTileAtIndexes(currentPhaseTileTool, xTileIndex, yTileIndex) ;
                if (coherenceFile) {
                    readTileAtIndexes(coherenceTileTool, xTileIndex, yTileIndex) ;
                }
                if (i) {
                    readTileAtIndexes(unwrappedPhaseTileTool, xTileIndex, yTileIndex) ;
                }
                transferDataToZ(gP) ;
                
                if (gP->mode == 1) {
                    evaluateXGradient(gP) ;
                    evaluateYGradient(gP) ;
                    
                    fftw_execute(gP->FFTPlanZ1) ;
                    fftw_execute(gP->FFTPlanZ2) ;
                    
                    /* Half pixel shift */
                    multZ1PIQx(gP) ;
                    multZ2PIQy(gP) ;
                    
                    multZ1Qx(gP) ;
                    multZ2Qy(gP) ;
                    
                    addZ1Z2(gP) ;
                    divZ1Q2(gP) ;
                    
                    fftw_execute(gP->IFFTPlanZ1) ;
                    
                    scalarMultiplyZ1(gP, 1./((float)TWOPI)/gP->xDim/gP->yDim) ;
                }
                else {
                    normalizationFactor = pow(1. / (double)gP->xDim2 / (double)gP->yDim2 / 4, 2.);
                    memcpy(gP->input[0], gP->sinPhi[0], gP->xDim2 * gP->yDim2 * sizeof(double)) ;
                    fftw_execute(gP->FFTPlan) ;

                    multOutPQ2(gP) ;
                    
                    fftw_execute(gP->IFFTPlan) ;
                    multOutCosPhi(gP) ;
                    
                    fftw_execute(gP->FFTPlan) ;
                    divOutPQ2(gP) ;
                    
                    fftw_execute(gP->IFFTPlan) ;
                    multOutCst(gP, normalizationFactor) ;

                    memcpy(gP->temp1[0], gP->output[0], gP->xDim2 * gP->yDim2 * sizeof(double)) ;

                    memcpy(gP->input[0], gP->cosPhi[0], gP->xDim2 * gP->yDim2 * sizeof(double)) ;
                    fftw_execute(gP->FFTPlan) ;
                    multOutPQ2(gP) ;
                    
                    fftw_execute(gP->IFFTPlan) ;
                    multOutSinPhi(gP) ;
                    
                    fftw_execute(gP->FFTPlan) ;
                    divOutPQ2(gP) ;
                    
                    fftw_execute(gP->IFFTPlan) ;
                    multOutCst(gP, normalizationFactor) ;
                    memcpy(gP->temp2[0], gP->output[0], gP->xDim2 * gP->yDim2 * sizeof(double)) ;
                    
                    diffTemp(gP) ;
                }

                addZ1iToDephi(gP) ;
                if (i == gP->iterationNumber - 1) {
//                    roundIt(gP) ;
                }
                diffInterfDephi(gP) ;
                
                updateTileToolForIndexes(unwrappedPhaseTileTool, xTileIndex, yTileIndex) ;
                writeTile(unwrappedPhaseTileTool) ;
                writeTileToOutputFile(currentPhaseTileTool, phaseResidue) ;
                updateProgressCounterForIndex(aCounter, xTileIndex * yTileIndex) ;
 
            }
        }
        

        updateProgressCounterForIndex(aCounter, xTileIndex * yTileIndex) ;
        freeProgressCounter(aCounter) ;
        if (i > 0) {
            lastPhaseResidue = currentPhaseTileTool->input ;
        }
    }
    
    if (i > 1) {
        unlink(lastPhaseResidue->accessPath) ;
        freeRawFileDescriptor(lastPhaseResidue) ;
    }
    aFilePath = initStringWithString(phaseResidue->accessPath) ;
    stripExtensionAtEndOfPath(aFilePath) ;
    aNewFilePath = initStringWith2Strings(aFilePath, ".phaseResidue") ;
    rename(phaseResidue->accessPath, aNewFilePath) ;
    free(aFilePath) ;
    free(aNewFilePath) ;
    freeTileTool(currentPhaseTileTool) ;
    freeTileTool(unwrappedPhaseTileTool) ;
    freeTileTool(interfTileTool) ;
    freeTileTool(residualPhaseTileTool) ;
    if (coherenceFile) {
        freeTileTool(coherenceTileTool) ;
        freeRawFileDescriptor(coherenceFile) ;
    }
    freeRawFileDescriptor(phaseResidue) ;
    freeRawFileDescriptor(interferogramFile ) ;
    freeRawFileDescriptor(unwrappedPhaseFile) ;
    
    freeDetPhun(gP) ;
}


void diffInterfDephi(detphun *gP)
{
    int    iX, iY ;
    float residualPhase, unwrappedPhase, wrappedPhase ;
    complexFloat c2, c1 ;
    
    for(iY = 0; iY < gP->yDim; iY++) {
        for(iX = 0; iX < gP->xDim; iX++) {
            wrappedPhase = gP->initialPhase[iY][iX] ;
            unwrappedPhase = gP->dephi[iY][iX] ;
            c1.r = cosf(unwrappedPhase) ;
            c1.i = sinf(unwrappedPhase) ;
            c2.r = cosf(wrappedPhase) ;
            c2.i = sinf(wrappedPhase) ;
            c2 = mC(c2, cC(c1)) ;
            residualPhase = (float)phi(&c2) ;
            gP->currentPhase[iY][iX] = residualPhase ;
        }
    }
}


void roundIt(detphun *gP)
{
    int iX, iY, k ;
    
    for (k = 0; k < 5; k++) {
        for(iY = 0; iY < gP->yDim; iY++) {
            for(iX = 0; iX < gP->xDim; iX++) {
                gP->dephi[iY][iX] = gP->initialPhase[iY][iX] + TWOPI * roundf((gP->dephi[iY][iX] - gP->initialPhase[iY][iX]) / TWOPI) ;
            }
        }
    }
}

void addZ1iToDephi(detphun *gP)
{
    int iX, iY ;
    
    for(iY = 0; iY < gP->yDim; iY++) {
        for(iX = 0; iX < gP->xDim; iX++) {
            gP->dephi[iY][iX] += (gP->mode == 1)? (float)gP->Z1[iY][iX].i : (float)(gP->temp1[iY][iX]) ;
            
        }
    }
    if (gP->removePhasePlane) {
        removePhasePlane(gP) ;
    }
}


void diffTemp(detphun *gP)
{
    int iX, iY ;
    
    for(iY = 0; iY < gP->yDim2; iY++) {
        for(iX = 0; iX < gP->xDim2; iX++) {
            gP->temp1[iY][iX] -= gP->temp2[iY][iX] ;
        }
    }
}


void scalarMultiplyZ1(detphun *gP, float aFloat)
{
    int    iX, iY ;
    
    for(iY = 0; iY < gP->yDim; iY++) {
        for(iX = 0; iX < gP->xDim; iX++) {
            gP->Z1[iY][iX] = mRComplexDouble(gP->Z1[iY][iX], aFloat) ;
        }
    }
}


void divZ1Q2(detphun *gP)
{
    int    iX, iY ;
    float inverseOfQ2, q2 ;
    
    for(iY = 0; iY < gP->yDim; iY++) {
        for(iX = 0; iX < gP->xDim; iX++) {
            q2 = gP->qx2[iX] + gP->qy2[iY] ;
            inverseOfQ2 = (q2)? 1. / q2 : 1 ;
            gP->Z1[iY][iX] = mRComplexDouble(gP->Z1[iY][iX], inverseOfQ2) ;
        }
    }
}



void addZ1Z2(detphun *gP)
{
    int    iX, iY ;
    
    for(iY = 0; iY < gP->yDim; iY++) {
        for(iX = 0; iX < gP->xDim; iX++) {
            gP->Z1[iY][iX] = sumComplexDouble(gP->Z1[iY][iX], gP->Z2[iY][iX]) ;
        }
    }
}


void multZ1PIQx(detphun *gP)
{
    int    iX, iY ;
    
    for(iY = 0; iY < gP->yDim; iY++) {
        for(iX = 0; iX < gP->xDim; iX++) {
            gP->Z1[iY][iX] = multComplexDouble(gP->Z1[iY][iX], gP->xPhaseShift[iX]) ;
        }
    }
}


void multZ2PIQy(detphun *gP)
{
    int    iX, iY ;
    
    for(iY = 0; iY < gP->yDim; iY++) {
        for(iX = 0; iX < gP->xDim; iX++) {
            gP->Z2[iY][iX] = multComplexDouble(gP->Z2[iY][iX], gP->yPhaseShift[iY]) ;
        }
    }
}


void multZ1Qx(detphun *gP)
{
    int    iX, iY ;
    
    for(iY = 0; iY < gP->yDim; iY++) {
        for(iX = 0; iX < gP->xDim; iX++) {
            gP->Z1[iY][iX] = mRComplexDouble(gP->Z1[iY][iX], gP->qx[iX]) ;
        }
    }
}


void multZ2Qy(detphun *gP)
{
    int    iX, iY ;
    
    for(iY = 0; iY < gP->yDim; iY++) {
        for(iX = 0; iX < gP->xDim; iX++) {
            gP->Z2[iY][iX] = mRComplexDouble(gP->Z2[iY][iX], gP->qy[iY]) ;
        }
    }
}

void multOutPQ2(detphun *gP)
{
    int    iX, iY ;
    
    for(iY = 0; iY < gP->yDim2; iY++) {
        for(iX = 0; iX < gP->xDim2; iX++) {
            gP->output[iY][iX] *= gP->pq2[iY][iX] ;
        }
    }
}


void divOutPQ2(detphun *gP)
{
    int    iX, iY ;
    
    for(iY = 0; iY < gP->yDim2; iY++) {
        for(iX = 0; iX < gP->xDim2; iX++) {
            gP->output[iY][iX] /= (gP->pq2[iY][iX])? gP->pq2[iY][iX] : 1. ;
        }
    }
}


void multOutCosPhi(detphun *gP)
{
    int    iX, iY ;
    
    for(iY = 0; iY < gP->yDim2; iY++) {
        for(iX = 0; iX < gP->xDim2; iX++) {
            gP->output[iY][iX] *= gP->cosPhi[iY][iX] ;
        }
    }
}


void multOutSinPhi(detphun *gP)
{
    int    iX, iY ;
    
    for(iY = 0; iY < gP->yDim2; iY++) {
        for(iX = 0; iX < gP->xDim2; iX++) {
            gP->output[iY][iX] *= gP->sinPhi[iY][iX] ;
        }
    }
}

void multOutCst(detphun *gP, double factor)
{
    int    iX, iY ;
    
    for(iY = 0; iY < gP->yDim2; iY++) {
        for(iX = 0; iX < gP->xDim2; iX++) {
            gP->output[iY][iX] *= factor ;
        }
    }
}


void evaluateXGradient(detphun *gP)
{
    int iX, iY ;
    float coh ;
    complexDouble *Z ;
    
    Z = vectorComplexDouble(0, gP->xDim - 1) ;
    for(iY = 0; iY < gP->yDim; iY++) {
        for(iX = 0; iX < gP->xDim - 1; iX++) {
            /* X derivative of wrapped phase 1 */
            Z[iX].r = (unwrappedPhaseGap((float)gP->Z1[iY][iX + 1].r, (float)gP->Z1[iY][iX].r)) ;
            Z[iX].i = 0. ;
            if (gP->coh) {
                coh = (gP->coh[iY][iX] + gP->coh[iY][iX + 1]) / 2. ;
                Z[iX].r = (coh  > gP->coherenceThreshold)? Z[iX].r : 0 ;
            }
        }
        Z[iX].r = Z[iX - 1].r ;

        memcpy(gP->Z1[iY], Z, gP->xDim * sizeof(complexDouble)) ;
    }
    freeVectorComplexDouble(Z, 0) ;
}

void evaluateYGradient(detphun *gP)
{
    int iX, iY ;
    float coh ;
    complexDouble *Z ;

    Z = vectorComplexDouble(0, gP->yDim - 1) ;
    for(iX = 0; iX < gP->xDim; iX++) {
        for(iY = 0; iY < gP->yDim - 1; iY++) {
            /* Y derivative of wrapped phase */
            Z[iY].r = (unwrappedPhaseGap((float)gP->Z2[iY + 1][iX].r, (float)gP->Z2[iY][iX].r)) ;
            Z[iY].i = 0. ;
            if (gP->coh) {
                coh = (gP->coh[iY][iX] + gP->coh[iY + 1][iX]) / 2. ;
                Z[iY].r = (coh  > gP->coherenceThreshold)? Z[iY].r : 0 ;
            }
        }
        Z[iY].r = Z[iY - 1].r ;
        
        for(iY = 0; iY < gP->yDim; iY++) {
            gP->Z2[iY][iX].r = Z[iY].r ;
        }
    }
    freeVectorComplexDouble(Z, 0) ;
}


void transferDataToZ(detphun *gP)
{
    int iX, iY ;
    double *zeros ;
    
    if (gP->mode == 1) {
        for(iY = 0; iY < gP->yDim; iY++) {
            for(iX = 0; iX < gP->xDim; iX++) {
                gP->Z1[iY][iX].r = gP->Z2[iY][iX].r = (double)gP->currentPhase[iY][iX] ;
                //            gP->Z1[iY][iX].r = gP->Z2[iY][iX].r = exp(-(pow((iX - gP->xDim/2.)/10., 2.) + pow((iY - gP->yDim/2.)/5., 2.))) ;
                gP->Z1[iY][iX].i = gP->Z2[iY][iX].i = 0. ;
            }
        }
    }
    
    /* FFTW */
    if (gP->mode == 2) {
        zeros = vectorDouble(0, gP->xDim2 - 1) ;
        for(iY = 0; iY < gP->yDim; iY++) {
            memcpy(gP->sinPhi[iY], zeros, gP->xDim2 * sizeof(double)) ;
            memcpy(gP->cosPhi[iY], zeros, gP->xDim2 * sizeof(double)) ;
            for(iX = 0; iX < gP->xDim; iX++) {
                gP->sinPhi[iY][iX] = sin(gP->currentPhase[iY][iX]) ;
                gP->cosPhi[iY][iX] = cos(gP->currentPhase[iY][iX]) ;
            }
            if (gP->coh) {
                for(iX = 0; iX < gP->xDim; iX++) {
                    if (gP->coh[iY][iX] < gP->coherenceThreshold) {
                        gP->cosPhi[iY][iX] = gP->sinPhi[iY][iX] = 0. ;
                    }
                }
            }
        }
        for(; iY < gP->yDim2; iY++) {
            memcpy(gP->sinPhi[iY], zeros, gP->xDim2 * sizeof(double)) ;
            memcpy(gP->cosPhi[iY], zeros, gP->xDim2 * sizeof(double)) ;
        }
        freeVectorDouble(zeros, 0) ;
    }

}


detphun *allocAndInitDetphunWithWindowSize(int xDim, int yDim, int unsigned flag)
{
    int        i, iX, iY ;
    detphun    *gP ;
    float dphix, dphiy ;
    double q2 ;
    
    if((gP = (detphun *)malloc(sizeof(detphun))) == NULL) {
        ase("Fail to allocate detPhUn global pointer") ;
    }
    
    gP->mode = (flag & _DETPHUN2_)? 2 : 1 ;
    gP->xDim = xDim ;
    gP->yDim = yDim ;
    gP->xDim2 = ceil2ofIntVal(xDim) ;
    gP->yDim2 = ceil2ofIntVal(yDim) ;
    gP->xDim2 = xDim ;
    gP->yDim2 = yDim ;
    gP->iterationNumber = 1 ;
    dphix = dphiy = 0. ;
    
    if (gP->mode == 1) {
        gP->Z1 = matrixComplexDouble(0, gP->yDim - 1, 0, gP->xDim - 1) ;
        printf("Init FFTW complex forward plan\n") ;
        gP->FFTPlanZ1 = fftw_plan_dft_2d(gP->yDim, gP->xDim, (fftw_complex *)gP->Z1[0], (fftw_complex *)gP->Z1[0], FFTW_FORWARD, FFTW_MEASURE) ;
        printf("Init FFTW complex backward plan\n") ;
        gP->IFFTPlanZ1 = fftw_plan_dft_2d(gP->yDim, gP->xDim, (fftw_complex *)gP->Z1[0], (fftw_complex *)gP->Z1[0], FFTW_BACKWARD, FFTW_MEASURE) ;
        gP->Z2 = matrixComplexDouble(0, gP->yDim - 1, 0, gP->xDim - 1) ;
        printf("Init FFTW second complex forward plan\n") ;
        gP->FFTPlanZ2 = fftw_plan_dft_2d(gP->yDim, gP->xDim, (fftw_complex *)gP->Z2[0], (fftw_complex *)gP->Z2[0], FFTW_FORWARD, FFTW_MEASURE) ;
        printf("Init FFTW secondcomplex backward plan\n") ;
        gP->IFFTPlanZ2 = fftw_plan_dft_2d(gP->yDim, gP->xDim, (fftw_complex *)gP->Z2[0], (fftw_complex *)gP->Z2[0], FFTW_BACKWARD, FFTW_MEASURE) ;
        
        gP->xPhaseShift = vectorComplexDouble(0, gP->xDim - 1) ;
        gP->qx = vectorDouble(0, gP->xDim - 1) ;
        gP->qx2 = vectorDouble(0, gP->xDim - 1) ;
        
        for(i = 0; i < gP->xDim / 2; i++) gP->qx[i] = (float)i / (float)gP->xDim + dphix ;
        for(; i < gP->xDim; i++) gP->qx[i] = (float)(i - gP->xDim) / (float)gP->xDim - dphix ;
        for(i = 0; i < gP->xDim; i++) {
            //        gP->qx[i] += dphix ;
            gP->xPhaseShift[i].r =  cos(PI * gP->qx[i]) ;
            gP->xPhaseShift[i].i = -sin(PI * gP->qx[i]) ;
            gP->qx2[i] = pow(gP->qx[i], 2.) ;
        }
        
        gP->yPhaseShift = vectorComplexDouble(0, gP->yDim - 1) ;
        gP->qy = vectorDouble(0, gP->yDim - 1) ;
        gP->qy2 = vectorDouble(0, gP->yDim - 1) ;
        
        for(i = 0; i < gP->yDim / 2; i++) gP->qy[i] = (float)i / (float)gP->yDim + dphiy ;
        for(; i < gP->yDim; i++) gP->qy[i] = (float)(i - gP->yDim) / (float)gP->yDim - dphiy ;
        for(i = 0; i < gP->yDim; i++) {
            //        gP->qy[i] += dphiy ;
            gP->yPhaseShift[i].r =  cos(PI * gP->qy[i]) ;
            gP->yPhaseShift[i].i = -sin(PI * gP->qy[i]) ;
            gP->qy2[i] = pow(gP->qy[i], 2.) ;
        }
    }
    else {
        gP->sinPhi = matrixDouble(0, gP->yDim2 - 1, 0, gP->xDim2  - 1) ;
        gP->cosPhi = matrixDouble(0, gP->yDim2 - 1, 0, gP->xDim2  - 1) ;
        gP->temp1 = matrixDouble(0, gP->yDim2 - 1, 0, gP->xDim2  - 1) ;
        gP->temp2 = matrixDouble(0, gP->yDim2 - 1, 0, gP->xDim2  - 1) ;
        gP->pq2 = matrixDouble(0, gP->yDim2 - 1, 0, gP->xDim2  - 1) ;
        gP->input = matrixDouble(0, gP->yDim2 - 1, 0, gP->xDim2  - 1) ;
        gP->output = gP->input ;
        printf("Init FFTW real forward plan\n") ;
        gP->FFTPlan = fftw_plan_r2r_2d(gP->yDim2, gP->xDim2, gP->input[0], gP->input[0], FFTW_REDFT00, FFTW_REDFT00, FFTW_MEASURE) ;
        printf("Init FFTW real backward plan\n") ;
        gP->IFFTPlan = fftw_plan_r2r_2d(gP->yDim2, gP->xDim2, gP->input[0], gP->input[0], FFTW_REDFT00, FFTW_REDFT00, FFTW_MEASURE) ;

        for(iY = 0; iY < gP->yDim2; iY++) {
            q2 = pow((double)iY / (double)gP->yDim2, 2) ;
            for(iX = 0; iX < gP->xDim2; iX++) {
                gP->pq2[iY][iX] = pow((double)iX / (double)gP->xDim2, 2) + q2 ;
            }
        }
    }

    
    return gP ;
}


void freeDetPhun(detphun *gP)
{
    if (gP->mode == 1) {
        freeVectorDouble(gP->qx, 0) ;
        freeVectorDouble(gP->qy, 0) ;
        freeVectorDouble(gP->qx2, 0) ;
        freeVectorDouble(gP->qy2, 0) ;
        freeVectorComplexDouble(gP->xPhaseShift, 0) ;
        freeVectorComplexDouble(gP->yPhaseShift, 0) ;
        freeMatrixComplexDouble(gP->Z1, 0, 0) ;
        freeMatrixComplexDouble(gP->Z2, 0, 0) ;
    }
    else {
        freeMatrixDouble(gP->cosPhi, 0, 0) ;
        freeMatrixDouble(gP->sinPhi, 0, 0) ;
        freeMatrixDouble(gP->input, 0, 0) ;
//        freeMatrixDouble(gP->output, 0, 0) ;
        fftw_destroy_plan(gP->FFTPlan) ;
        
    }
}

void resetDetphun(detphun *gP)
{
    int i ;
    float *zero ;
    
    if (gP->mode == 1) {
        resetZ(gP) ;
    }
    zero = vectorFloat(0, gP->xDim - 1) ;
    for(i = 0; i < gP->xDim; i++)
        zero[i] = 0. ;
    
    for(i = 0; i < gP->yDim; i++) {
        memcpy(gP->dephi[i], zero, gP->xDim * sizeof(float)) ;
        //        memcpy(gP->data[i], zero, gP->xDim) ;
    }
    
    freeVectorFloat(zero, 0) ;
}

void resetZ(detphun *gP)
{
    int i ;
    complexDouble *zero ;
    
    zero = vectorComplexDouble(0, gP->xDim - 1) ;
    for(i = 0; i < gP->xDim; i++)
        zero[i].r = zero[i].i = 0. ;
    for(i = 0; i < gP->yDim; i++) {
        memcpy(gP->Z1[i], zero, gP->xDim * sizeof(complexFloat)) ;
        memcpy(gP->Z2[i], zero, gP->xDim * sizeof(complexFloat)) ;
    }
    freeVectorComplexDouble(zero, 0) ;
}


void removePhasePlane(detphun *gP)
{
    int    xDim, yDim, xDimCoh, yDimCoh, iX0, iY0 ;
    int xDimBloc, yDimBloc, deltaX, deltaY, nBlocX, nBlocY ;
    int i, j, k, l, iX, iY ;
    int    *index ;
    float **DEM, **coh, zVal ;
    float norm ;
    float excludingValue ;
    double **M, *V ;
    double **MI, *VV ;
    
    xDim = xDimCoh = gP->xDim ;
    yDim = yDimCoh = gP->yDim ;
    genFloatNaN() ;
    excludingValue = floatNaN ;
    
    DEM = gP->dephi ;
    coh = gP->coh ;

    index = vectorInt(0, 2) ;
    M = matrixDouble(0, 2, 0, 2) ;
    V = vectorDouble(0, 2) ;
    
    for(i = 0; i < 3; i++) {
        for(j = 0; j < 3; j++) {
            M[i][j] = 0.0 ;
        }
        V[i] = 0.0 ;
    }
    
    xDimBloc = yDimBloc = 41 ;
    do {
        xDimBloc = xDimBloc / 2 + 1 ;
        nBlocX = xDim / xDimBloc ;
    } while(nBlocX <= 1) ;
    
    do {
        yDimBloc = yDimBloc / 2 + 1 ;
        nBlocY = yDim / yDimBloc ;
    } while(nBlocY <= 1) ;
    
    deltaX = (xDim - nBlocX * xDimBloc)/(nBlocX - 1) ;
    deltaY = (yDim - nBlocY * yDimBloc)/(nBlocY - 1) ;
    iX0 = (deltaX)? 0 : (xDim - nBlocX * xDimBloc) / 2 ;
    iY0 = (deltaY)? 0 : (yDim - nBlocY * yDimBloc) / 2 ;
    
    for(i = 0; i < nBlocY; i++) {
        iY = i * (yDimBloc + deltaY) + yDimBloc/2 + iY0 ;
        for(j = 0; j < nBlocX; j++) {
            iX = j * (xDimBloc + deltaX) + xDimBloc/2 + iX0 ;
            zVal = norm = 0. ;
            for(k = -yDimBloc/2; k <= yDimBloc/2; k++) {
                for(l = -xDimBloc/2; l <= xDimBloc/2; l++) {
                    if (!areFloatValsEquals(DEM[iY + k][iX + l], excludingValue)) {
                        if (gP->coh) {
                            if (coh[iY + k][iX + l] > gP->coherenceThreshold) {
                                zVal += DEM[iY + k][iX + l] ;
                                norm++ ;
                            }
                        }
                        else {
                            zVal += DEM[iY + k][iX + l] ;
                            norm++ ;
                        }
                    }
                }
            }
            if (norm) {
                zVal /= norm ;
                
                M[0][0] += pow((double)iX, 2.) ;
                M[0][1] += iX * iY ;
                M[0][2] += iX ;
                M[1][1] += pow((double)iY, 2.) ;
                M[1][2] += iY ;
                M[2][2]++ ;
                
                V[0] += iX * zVal ;
                V[1] += iY * zVal ;
                V[2] += zVal ;
            }
        }
    }
    for(i = 0; i < 3; i++) {
        for(j = i + 1; j < 3; j++) {
            M[j][i] = M[i][j] ;
        }
    }
    
    MI = m3x3Inversion(M) ;
    VV = multMatVect(MI, V, 3) ;
    
    printf("Removed plane: %9.5f X + %9.5f Y + %9.5f\n", VV[0], VV[1], VV[2]);
    for(iY = 0; iY < yDim; iY++) {
        for(iX = 0; iX < xDim; iX++) {
            DEM[iY][iX] -= (float)(VV[0] * iX + VV[1] * iY + VV[2]) ;
        }
    }
    
    freeMatrixDouble(M, 0, 0) ;
    freeMatrixDouble(MI, 0, 0) ;
    freeVectorDouble(V, 0) ;
    freeVectorDouble(VV, 0) ;
}


double **m3x3Inversion(double **M)
{
    int i, j ;
    double determinant, **MI = NULL ;

    determinant = m3x3Determinant(M) ;
    
    if (determinant) {
        MI = matrixDouble(0, 2, 0, 2) ;
        for(i=0;i<3;i++){
            for(j=0;j<3;j++)
                MI[j][i] = ((M[(i+1)%3][(j+1)%3] * M[(i+2)%3][(j+2)%3]) - (M[(i+1)%3][(j+2)%3] * M[(i+2)%3][(j+1)%3])) / determinant ;
            printf("\n");
        }
    }
    
    return(MI) ;
}

double m3x3Determinant(double **M)
{
    int i ;
    double determinant = 0 ;
    
    for(i=0;i<3;i++)
        determinant = determinant + (M[0][i] * (M[1][(i+1)%3]*M[2][(i+2)%3] - M[1][(i+2)%3] * M[2][(i+1)%3])) ;

    return (determinant) ;
}

double **multMat(double **M1, double **M2, int rank)
{
    int i, j, k ;
    double **M ;
    
    M = matrixDouble(0, rank - 1, 0, rank - 1) ;
    for (i = 0; i < rank; i++) {
        for (j = 0; j < rank; j++) {
            M[i][j] = 0. ;
            for (k = 0; k < rank; k++) {
                M[i][j] += M1[i][k] * M2[k][j] ;
            }
        }
    }
    
    return (M) ;
}


double *multMatVect(double **M1, double *V1, int rank)
{
    int i, k ;
    double *V ;
    
    V = vectorDouble(0, rank - 1) ;
    for (i = 0; i < rank; i++) {
        V[i] = 0. ;
        for (k = 0; k < rank; k++) {
            V[i] += M1[i][k] * V1[k] ;
        }
    }
    
    return (V) ;
}
