/*
 *  matrix.c
 *  manMadeStructureGrowth
 *
 *  Created by Dominique Derauw on 16/12/08.
 *  Copyright 2008 Dominique Derauw All rights reserved.
 *
 */

/* Matrix definition and allocation on continuous memory space */

#include "matrix.h"

/* ----------------- Allocation of a continuous space for a matrix of char unsigned  ----------------- */

unsigned char	**matrixUChar(int rowLowerIndex, int rowHigherIndex, int columnLowerIndex, int columnHigherIndex)
{
	int		iX, iY ;
	unsigned char	**aMatrix ;
	
	/* Allocation of a column vector of pointers */
	if((aMatrix = (unsigned char **) malloc((unsigned) (rowHigherIndex - rowLowerIndex + 1) * sizeof(unsigned char *))) == NULL) {
            perror("Unsigned char matrix allocation error ") ;
            exit(-1) ;
        }
	
	/* Allocation of the required continuous space */
	if((aMatrix[0] = (unsigned char *)calloc((unsigned)(columnHigherIndex - columnLowerIndex + 1) * (rowHigherIndex - rowLowerIndex + 1), sizeof(unsigned char))) == NULL) {
		perror("Unsigned char matrix allocation error ") ;
		exit(-1) ;
	}
	
	/* Indexing of the column vector */
	for(iY = rowLowerIndex + 1; iY <= rowHigherIndex; iY++)
		aMatrix[iY - rowLowerIndex] = &aMatrix[0][(iY - rowLowerIndex) * (columnHigherIndex - columnLowerIndex + 1)] - columnLowerIndex ;

	aMatrix[0] -= columnLowerIndex ;
	aMatrix -= rowLowerIndex ;
	
	for(iY = rowLowerIndex; iY <= rowHigherIndex; iY++)
		for(iX = columnLowerIndex; iX < columnHigherIndex; iX++)
			aMatrix[iY][iX] = 0 ;
	
	return(aMatrix) ;
}


/* ----------------- De-allocation of a matrix of char unsigned ----------------- */

void	freeMatrixUChar(unsigned char **aMatrix, int rowLowerIndex, int columnLowerIndex)
{
    if (aMatrix) {
        free(aMatrix[rowLowerIndex] + columnLowerIndex) ;
        free((void *)(aMatrix+rowLowerIndex)) ;
    }
}


/* ----------------- Allocation of a continuous space for a matrix of short  ----------------- */

short	**matrixShort(int rowLowerIndex, int rowHigherIndex, int columnLowerIndex, int columnHigherIndex)
{
	int		i ;
	short	**aMatrix ;
	
	/* Allocation of a column vector of pointers */
	if((aMatrix = (short **) malloc((unsigned) (rowHigherIndex - rowLowerIndex + 1) * sizeof(short *))) == NULL) {
            perror("short matrix allocation error") ;
            exit(-1) ;
        }
	
	/* Allocation of the required continuous space */
	if((aMatrix[0] = (short *)calloc((unsigned)(columnHigherIndex - columnLowerIndex + 1) * (rowHigherIndex - rowLowerIndex + 1), sizeof(short))) == NULL) {
		perror("short matrix allocation error") ;
		exit(-1) ;
	}
	
	/* Indexing of the column vector */
	for(i = rowLowerIndex + 1; i <= rowHigherIndex; i++)
		aMatrix[i - rowLowerIndex] = &aMatrix[0][(i - rowLowerIndex) * (columnHigherIndex - columnLowerIndex + 1)] - columnLowerIndex ;

	aMatrix[0] -= columnLowerIndex ;
	aMatrix -= rowLowerIndex ;
	return(aMatrix) ;
}


/* ----------------- De-allocation of a matrix of short ----------------- */

void	freeMatrixShort(short **aMatrix, int rowLowerIndex, int columnLowerIndex)
{
    if (aMatrix) {
        free(aMatrix[rowLowerIndex] + columnLowerIndex) ;
        free((void *)(aMatrix+rowLowerIndex)) ;
    }
}


/* ----------------- Allocation of a continuous space for a matrix of short unsigned  ----------------- */

short unsigned	**matrixShortUnsigned(int rowLowerIndex, int rowHigherIndex, int columnLowerIndex, int columnHigherIndex)
{
	int		i ;
	short unsigned	**aMatrix ;
	
	/* Allocation of a column vector of pointers */
	if((aMatrix = (short unsigned **) malloc((unsigned) (rowHigherIndex - rowLowerIndex + 1) * sizeof(short unsigned *))) == NULL) {
            perror("short unsigned matrix allocation error") ;
            exit(-1) ;
        }
	
	/* Allocation of the required continuous space */
	if((aMatrix[0] = (short unsigned *)calloc((unsigned)(columnHigherIndex - columnLowerIndex + 1) * (rowHigherIndex - rowLowerIndex + 1), sizeof(short unsigned))) == NULL) {
		perror("short unsigned matrix allocation error") ;
		exit(-1) ;
	}
	
	/* Indexing of the column vector */
	for(i = rowLowerIndex + 1; i <= rowHigherIndex; i++)
		aMatrix[i - rowLowerIndex] = &aMatrix[0][(i - rowLowerIndex) * (columnHigherIndex - columnLowerIndex + 1)] - columnLowerIndex ;

	aMatrix[0] -= columnLowerIndex ;
	aMatrix -= rowLowerIndex ;
	return(aMatrix) ;
}


/* ----------------- De-allocation of a matrix of short unsigned ----------------- */

void	freeMatrixShortUnsigned(short unsigned **aMatrix, int rowLowerIndex, int columnLowerIndex)
{
    if (aMatrix) {
        free(aMatrix[rowLowerIndex] + columnLowerIndex) ;
        free((void *)(aMatrix+rowLowerIndex)) ;
    }
}


/* ----------------- Allocation of a continuous space for a matrix of int  ----------------- */

int	**matrixInt(int rowLowerIndex, int rowHigherIndex, int columnLowerIndex, int columnHigherIndex)
{
	int		i ;
	int	**aMatrix ;
	
	/* Allocation of a column vector of pointers */
	if((aMatrix = (int **) malloc((unsigned) (rowHigherIndex - rowLowerIndex + 1) * sizeof(int *))) == NULL) {
        perror("Int matrix allocation error") ;
        exit(-1) ;
    }
	
	/* Allocation of the required continuous space */
	if((aMatrix[0] = (int *)calloc((unsigned)(columnHigherIndex - columnLowerIndex + 1) * (rowHigherIndex - rowLowerIndex + 1), sizeof(int))) == NULL) {
		perror("Int matrix allocation error") ;
		exit(-1) ;
	}
	
	/* Indexing of the column vector */
	for(i = rowLowerIndex + 1; i <= rowHigherIndex; i++)
		aMatrix[i - rowLowerIndex] = &aMatrix[0][(i - rowLowerIndex) * (columnHigherIndex - columnLowerIndex + 1)] - columnLowerIndex ;
    
	aMatrix[0] -= columnLowerIndex ;
	aMatrix -= rowLowerIndex ;
	return(aMatrix) ;
}


/* ----------------- De-allocation of a matrix of int ----------------- */

void	freeMatrixInt(int **aMatrix, int rowLowerIndex, int columnLowerIndex)
{
    if (aMatrix) {
        free(aMatrix[rowLowerIndex] + columnLowerIndex) ;
        free((void *)(aMatrix+rowLowerIndex)) ;
    }
}


/* ----------------- Allocation of a continuous space for a matrix of int  ----------------- */

int unsigned	**matrixIntUnsigned(int rowLowerIndex, int rowHigherIndex, int columnLowerIndex, int columnHigherIndex)
{
	int		i ;
	int unsigned	**aMatrix ;
	
	/* Allocation of a column vector of pointers */
	if((aMatrix = (int unsigned **) malloc((unsigned) (rowHigherIndex - rowLowerIndex + 1) * sizeof(int unsigned *))) == NULL) {
        perror("Int matrix allocation error") ;
        exit(-1) ;
    }
	
	/* Allocation of the required continuous space */
	if((aMatrix[0] = (int unsigned *)calloc((unsigned)(columnHigherIndex - columnLowerIndex + 1) * (rowHigherIndex - rowLowerIndex + 1), sizeof(int unsigned))) == NULL) {
		perror("Int matrix allocation error") ;
		exit(-1) ;
	}
	
	/* Indexing of the column vector */
	for(i = rowLowerIndex + 1; i <= rowHigherIndex; i++)
		aMatrix[i - rowLowerIndex] = &aMatrix[0][(i - rowLowerIndex) * (columnHigherIndex - columnLowerIndex + 1)] - columnLowerIndex ;
    
	aMatrix[0] -= columnLowerIndex ;
	aMatrix -= rowLowerIndex ;
	return(aMatrix) ;
}


/* ----------------- De-allocation of a matrix of int ----------------- */

void	freeMatrixIntUnsigned(int **aMatrix, int rowLowerIndex, int columnLowerIndex)
{
    if (aMatrix) {
        free(aMatrix[rowLowerIndex] + columnLowerIndex) ;
        free((void *)(aMatrix+rowLowerIndex)) ;
    }
}


/* ----------------- Allocation of a continuous space for a matrix of float  ----------------- */

float	**matrixFloat(int rowLowerIndex, int rowHigherIndex, int columnLowerIndex, int columnHigherIndex)
{
	int		i ;
	float	**aMatrix ;
	
	/* Allocation of a column vector of pointers */
	if((aMatrix = (float **) malloc((unsigned) (rowHigherIndex - rowLowerIndex + 1) * sizeof(float *))) == NULL) {
            perror("float matrix allocation error") ;
            exit(-1) ;
        }
	
	/* Allocation of the required continuous space */
	if((aMatrix[0] = (float *)calloc((unsigned)(columnHigherIndex - columnLowerIndex + 1) * (rowHigherIndex - rowLowerIndex + 1), sizeof(float))) == NULL) {
		perror("float matrix allocation error") ;
		exit(-1) ;
	}
	
	/* Indexing of the column vector */
	for(i = rowLowerIndex + 1; i <= rowHigherIndex; i++)
		aMatrix[i - rowLowerIndex] = &aMatrix[0][(i - rowLowerIndex) * (columnHigherIndex - columnLowerIndex + 1)] - columnLowerIndex ;

	aMatrix[0] -= columnLowerIndex ;
	aMatrix -= rowLowerIndex ;
	return(aMatrix) ;
}


/* ----------------- De-allocation of a matrix of float ----------------- */

void	freeMatrixFloat(float **aMatrix, int rowLowerIndex, int columnLowerIndex)
{
    if (aMatrix) {
        free(aMatrix[rowLowerIndex] + columnLowerIndex) ;
        free((void *)(aMatrix+rowLowerIndex)) ;
    }
}



/* ----------------- Allocation of a continuous space for a matrix of double  ----------------- */

double	**matrixDouble(int rowLowerIndex, int rowHigherIndex, int columnLowerIndex, int columnHigherIndex)
{
	int		i ;
	double	**aMatrix ;
	
	/* Allocation of a column vector of pointers */
	if((aMatrix = (double **) malloc((unsigned) (rowHigherIndex - rowLowerIndex + 1) * sizeof(double *))) == NULL) {
            perror("double matrix allocation error") ;
            exit(-1) ;
        }
	
	/* Allocation of the required continuous space */
	if((aMatrix[0] = (double *)calloc((unsigned)(columnHigherIndex - columnLowerIndex + 1) * (rowHigherIndex - rowLowerIndex + 1), sizeof(double))) == NULL) {
		perror("double matrix allocation error") ;
		exit(-1) ;
	}
	
	/* Indexing of the column vector */
	for(i = rowLowerIndex + 1; i <= rowHigherIndex; i++)
		aMatrix[i - rowLowerIndex] = &aMatrix[0][(i - rowLowerIndex) * (columnHigherIndex - columnLowerIndex + 1)] - columnLowerIndex ;

	aMatrix[0] -= columnLowerIndex ;
	aMatrix -= rowLowerIndex ;
	return(aMatrix) ;
}


/* ----------------- De-allocation of a matrix of double ----------------- */

void	freeMatrixDouble(double **aMatrix, int rowLowerIndex, int columnLowerIndex)
{
    if (aMatrix) {
        free(aMatrix[rowLowerIndex] + columnLowerIndex) ;
        free((void *)(aMatrix+rowLowerIndex)) ;
    }
}




/* ----------------- Allocation of a continuous space for a matrix of complexShort  ----------------- */

complexShort	**matrixComplexShort(int rowLowerIndex, int rowHigherIndex, int columnLowerIndex, int columnHigherIndex)
{
	int		i ;
	complexShort	**aMatrix ;
	
	/* Allocation of a column vector of pointers */
	if((aMatrix = (complexShort **) malloc((unsigned) (rowHigherIndex - rowLowerIndex + 1) * sizeof(complexShort *))) == NULL) {
		perror("complexShort matrix allocation error") ;
		exit(-1) ;
	}
	
	/* Allocation of the required continuous space */
	if((aMatrix[0] = (complexShort *)calloc((unsigned)(columnHigherIndex - columnLowerIndex + 1) * (rowHigherIndex - rowLowerIndex + 1), sizeof(complexShort))) == NULL) {
		perror("complexFloat matrix allocation error") ;
		exit(-1) ;
	}
	
	/* Indexing of the column vector */
	for(i = rowLowerIndex + 1; i <= rowHigherIndex; i++)
		aMatrix[i - rowLowerIndex] = &aMatrix[0][(i - rowLowerIndex) * (columnHigherIndex - columnLowerIndex + 1)] - columnLowerIndex ;
	
	aMatrix[0] -= columnLowerIndex ;
	aMatrix -= rowLowerIndex ;
	return(aMatrix) ;
}


/* ----------------- De-allocation of a matrix of complexShort ----------------- */

void	freeMatrixComplexShort(complexShort **aMatrix, int rowLowerIndex, int columnLowerIndex)
{
    if (aMatrix) {
        free(aMatrix[rowLowerIndex] + columnLowerIndex) ;
        free((void *)(aMatrix+rowLowerIndex)) ;
    }
}


/* ----------------- Allocation of a continuous space for a matrix of complexFloat  ----------------- */

complexFloat	**matrixComplexFloat(int rowLowerIndex, int rowHigherIndex, int columnLowerIndex, int columnHigherIndex)
{
	int		i ;
	complexFloat	**aMatrix ;
    size_t memSize ;
	
	/* Allocation of a column vector of pointers */
    memSize = (rowHigherIndex - rowLowerIndex + 1) * sizeof(*aMatrix) ;
	if((aMatrix = malloc(memSize)) == NULL) {
		perror("complexFloat matrix allocation error") ;
		exit(-1) ;
	}
	
	/* Allocation of the required continuous space */
    memSize = (columnHigherIndex - columnLowerIndex + 1) * (rowHigherIndex - rowLowerIndex + 1) * sizeof(**aMatrix) ;
	if((*aMatrix = malloc(memSize)) == NULL) {
		perror("complexFloat matrix allocation error") ;
		exit(-1) ;
	}
    memset(*aMatrix, 0, memSize) ;
	
	/* Indexing of the column vector */
	for(i = rowLowerIndex + 1; i <= rowHigherIndex; i++)
		aMatrix[i - rowLowerIndex] = &aMatrix[0][(i - rowLowerIndex) * (columnHigherIndex - columnLowerIndex + 1)] - columnLowerIndex ;
	
	aMatrix[0] -= columnLowerIndex ;
	aMatrix -= rowLowerIndex ;
    
	return(aMatrix) ;
}


/* ----------------- De-allocation of a matrix of complexFloat ----------------- */

void	freeMatrixComplexFloat(complexFloat **aMatrix, int rowLowerIndex, int columnLowerIndex)
{
    if (aMatrix) {
        free(aMatrix[rowLowerIndex] + columnLowerIndex) ;
        free((void *)(aMatrix+rowLowerIndex)) ;
    }
}



/* ----------------- Allocation of a continuous space for a matrix of complexDouble  ----------------- */

complexDouble	**matrixComplexDouble(int rowLowerIndex, int rowHigherIndex, int columnLowerIndex, int columnHigherIndex)
{
    complexDouble	**aMatrix ;
    
    aMatrix = (complexDouble **)matrixOfType(rowLowerIndex, rowHigherIndex, columnLowerIndex, columnHigherIndex, sizeof(complexDouble)) ;
    
    return(aMatrix) ;
}


/* ----------------- De-allocation of a matrix of complexDouble ----------------- */

void	freeMatrixComplexDouble(complexDouble **aMatrix, int rowLowerIndex, int columnLowerIndex)
{
    if (aMatrix) {
        freeMatrixOfType((void **)aMatrix, rowLowerIndex, columnLowerIndex, sizeof(complexDouble)) ;
    }
}


/* ----------------- Allocation of a continuous space for a matrix of any type  ----------------- */

void	**matrixOfType(int rowLowerIndex, int rowHigherIndex, int columnLowerIndex, int columnHigherIndex, size_t type)
{
    int		i ;
    void	**aMatrix ;
    
    /* Allocation of a column vector of pointers */
    if((aMatrix = (void **) malloc((unsigned) (rowHigherIndex - rowLowerIndex + 1) * sizeof(void *))) == NULL) {
        perror("Matrix allocation error") ;
        exit(-1) ;
    }
    
    /* Allocation of the required continuous space */
    if((aMatrix[0] = (void *)calloc((unsigned)(columnHigherIndex - columnLowerIndex + 1) * (rowHigherIndex - rowLowerIndex + 1), type)) == NULL) {
        perror("Matrix allocation error") ;
        exit(-1) ;
    }
    
    /* Indexing of the column vector */
    for(i = rowLowerIndex + 1; i <= rowHigherIndex; i++)
        aMatrix[i - rowLowerIndex] = aMatrix[0] + (i - rowLowerIndex) * (columnHigherIndex - columnLowerIndex + 1) * type - columnLowerIndex * type ;
    
    aMatrix[0] -= columnLowerIndex * type ;
    aMatrix -= rowLowerIndex ;
    
    return(aMatrix) ;
}


/* ----------------- De-allocation of a matrix of any type ----------------- */

void	freeMatrixOfType(void **aMatrix, int rowLowerIndex, int columnLowerIndex, size_t type)
{
    if (aMatrix) {
        free(aMatrix[rowLowerIndex] + columnLowerIndex * type) ;
        free((void *)(aMatrix + rowLowerIndex)) ;
    }
}


