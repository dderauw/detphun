//
//  tileToolLib.c
//  detPhUn
//
//  Created by Dominique Derauw on 30/08/12.
//  Copyright (c) 2012 Dominique Derauw. All rights reserved.
//

#include <stdio.h>
#include "tileToolLib.h"


tileTool *allocTileToolForDataOfType(void **inputDataMatrix, void **outputDataMatrix, int xDimData, int yDimData, char *dataType)
{
    tileTool *aTileTool ;

    if((aTileTool = malloc(sizeof(tileTool))) == NULL)
        ase("Failed to allocate tileTool") ;

    aTileTool->inputDataMatrix = inputDataMatrix ;
    if (outputDataMatrix == NULL) {
        aTileTool->outputDataMatrix = inputDataMatrix ;
    }
    else {
        aTileTool->outputDataMatrix = outputDataMatrix ;
    }

    aTileTool->input = NULL ;
    aTileTool->output = NULL ;
    aTileTool->mirroring = 0 ;
    aTileTool->tile = NULL ;

    aTileTool->typeSize = 0 ;
    sprintf(aTileTool->dataType, "%s", dataType) ;
    if (!strncmp((const char *)dataType, "char", 4)) {
        aTileTool->typeSize = sizeof(char) ;
        aTileTool->typeCode = _CHAR_ID_ ;
    }
    if (!strncmp((const char *)dataType, "int", 3)) {
        aTileTool->typeSize = sizeof(int) ;
        aTileTool->typeCode = _INT32_ID_ ;
    }
    if (!strncmp((const char *)dataType, "long", 4)) {
        aTileTool->typeSize = sizeof(long) ;
        aTileTool->typeCode = _INT64_ID_ ;
    }
    if (!strncmp((const char *)dataType, "float", 5)) {
        aTileTool->typeSize = sizeof(float) ;
        aTileTool->typeCode = _REAL32_ID_ ;
    }
    if (!strncmp((const char *)dataType, "double", 6)) {
        aTileTool->typeSize = sizeof(double) ;
        aTileTool->typeCode = _REAL64_ID_ ;
    }
    if (strstr((const char *)dataType, "complexFloat")) {
        aTileTool->typeSize = sizeof(complexFloat) ;
        aTileTool->typeCode = _CPLX32_ID_ ;
    }
    if (strstr((const char *)dataType, "complexDouble")) {
        aTileTool->typeSize = sizeof(complexDouble) ;
        aTileTool->typeCode = _CPLX64_ID_ ;
    }

    aTileTool->aoi = allocQuadrilateral() ;

    aTileTool->xDimIn = xDimData ;
    aTileTool->yDimIn = yDimData ;
    aTileTool->aoi->P[0].x = aTileTool->aoi->P[3].x = 0 ;
    aTileTool->aoi->P[1].x = aTileTool->aoi->P[2].x = aTileTool->xDimIn - 1 ;
    aTileTool->aoi->P[0].y = aTileTool->aoi->P[1].y = 0 ;
    aTileTool->aoi->P[2].y = aTileTool->aoi->P[3].y = aTileTool->yDimIn - 1 ;

    aTileTool->xDimOut = aTileTool->xDimIn ;
    aTileTool->yDimOut = aTileTool->yDimIn ;

    return (aTileTool) ;
}

tileTool *allocTileToolForFilesOfType(rawFileDescriptor *inputFile, rawFileDescriptor *outputFile, char *dataType)
{
    int i ;
    tileTool *aTileTool ;

    if((aTileTool = malloc(sizeof(tileTool))) == NULL)
        ase("Failed to allocate tileTool") ;

    aTileTool->inputDataMatrix = NULL ;
    aTileTool->input = inputFile ;
    aTileTool->output = outputFile ;
    aTileTool->mirroring = 0 ;
    aTileTool->tile = NULL ;

    aTileTool->typeSize = 0 ;
    sprintf(aTileTool->dataType, "%s", dataType) ;
    if (!strncmp((const char *)dataType, "char", 4)) {
        aTileTool->typeSize = sizeof(char) ;
        aTileTool->typeCode = _CHAR_ID_ ;
    }
    if (!strncmp((const char *)dataType, "int", 3)) {
        aTileTool->typeSize = sizeof(int) ;
        aTileTool->typeCode = _INT32_ID_ ;
    }
    if (!strncmp((const char *)dataType, "long", 4)) {
        aTileTool->typeSize = sizeof(long) ;
        aTileTool->typeCode = _INT64_ID_ ;
    }
    if (!strncmp((const char *)dataType, "float", 5)) {
        aTileTool->typeSize = sizeof(float) ;
        aTileTool->typeCode = _REAL32_ID_ ;
    }
    if (!strncmp((const char *)dataType, "double", 6)) {
        aTileTool->typeSize = sizeof(double) ;
        aTileTool->typeCode = _REAL64_ID_ ;
    }
    if (!strncmp((const char *)dataType, "complexFloat", 3)) {
        aTileTool->typeSize = sizeof(complexFloat) ;
        aTileTool->typeCode = _CPLX32_ID_ ;
    }
    if (strstr((const char *)dataType, "complexDouble")) {
        aTileTool->typeSize = sizeof(complexDouble) ;
        aTileTool->typeCode = _CPLX64_ID_ ;
    }

    if (!aTileTool->typeSize) {
        ase("Tile tool initialization error: wrong type requested") ;
    }

    aTileTool->aoi = allocQuadrilateral() ;

    aTileTool->xDimIn = aTileTool->yDimIn = 0 ;
    if (inputFile != NULL) {
        aTileTool->xDimIn = inputFile->xDim ;
        aTileTool->yDimIn = inputFile->yDim ;
        if (inputFile->aoi != NULL) {
            for (i = 0; i < inputFile->aoi->N; i++) {
                aTileTool->aoi->P[i].x = inputFile->aoi->P[i].x ;
                aTileTool->aoi->P[i].y = inputFile->aoi->P[i].y ;
            }
        }
        else {
            aTileTool->aoi->P[0].x = aTileTool->aoi->P[3].x = 0 ;
            aTileTool->aoi->P[1].x = aTileTool->aoi->P[2].x = aTileTool->xDimIn - 1 ;
            aTileTool->aoi->P[0].y = aTileTool->aoi->P[1].y = 0 ;
            aTileTool->aoi->P[2].y = aTileTool->aoi->P[3].y = aTileTool->yDimIn - 1 ;
        }
    }

    aTileTool->xDimOut = aTileTool->yDimOut = 0 ;
    if (outputFile != NULL) {
        aTileTool->xDimOut = outputFile->xDim ;
        aTileTool->yDimOut = outputFile->yDim ;
    }
    else {
        aTileTool->xDimOut = aTileTool->xDimIn ;
        aTileTool->yDimOut = aTileTool->yDimIn ;
    }

    return (aTileTool) ;
}

void freeTileTool(tileTool *aTileTool)
{
    if (aTileTool) {
        free(aTileTool->tile[0]) ;
        free(aTileTool->tile) ;
        aTileTool->tile = NULL ;
        freePolygone(aTileTool->aoi) ;
        free(aTileTool) ;

        *(&aTileTool) = NULL ;
    }
}


void **initTileToolWithSizes(tileTool *aTileTool, int unsigned xTileSize, int unsigned yTileSize, int unsigned xTileBorderSize, int unsigned yTileBorderSize)
{
    int iY ;
    div_t qr ;

    /* Init base values of tiles */
    aTileTool->xTileSize = xTileSize ;
    aTileTool->yTileSize = yTileSize ;
    aTileTool->xTileBorderSize = xTileBorderSize ;
    aTileTool->yTileBorderSize = yTileBorderSize ;
    aTileTool->xTileIndex = aTileTool->yTileIndex = 0 ;

    /* Basic size verification */
    if (aTileTool->xTileSize < 2 * aTileTool->xTileBorderSize) {
        aTileTool->xTileBorderSize = aTileTool->xTileSize / 16 ;
    }
    aTileTool->xDimEA = aTileTool->xTileSize - 2 * aTileTool->xTileBorderSize ;

    if (aTileTool->yTileSize < 2 * aTileTool->yTileBorderSize) {
        aTileTool->yTileBorderSize = aTileTool->yTileSize / 16 ;
    }
    aTileTool->yDimEA = aTileTool->yTileSize - 2 * aTileTool->yTileBorderSize ;

    /* Update input or output size if required */
    if (aTileTool->input == NULL) {
        aTileTool->xDimIn = aTileTool->xDimOut ;
        aTileTool->yDimIn = aTileTool->yDimOut ;
        aTileTool->aoi->P[0].x = aTileTool->aoi->P[3].x = 0 ;
        aTileTool->aoi->P[1].x = aTileTool->aoi->P[2].x = aTileTool->xDimIn - 1 ;
        aTileTool->aoi->P[0].y = aTileTool->aoi->P[1].y = 0 ;
        aTileTool->aoi->P[2].y = aTileTool->aoi->P[3].y = aTileTool->yDimIn - 1 ;
    }

    if (aTileTool->output == NULL) {
        aTileTool->xDimOut = aTileTool->xDimIn ;
        aTileTool->yDimOut = aTileTool->yDimIn ;
    }
    else {
        aTileTool->xDimOut = aTileTool->aoi->P[1].x - aTileTool->aoi->P[0].x + 1 ;
        aTileTool->yDimOut = aTileTool->aoi->P[3].y - aTileTool->aoi->P[0].y + 1 ;
    }

    /* Compute number of tiles for input */
    if (aTileTool->xDimEA < aTileTool->xDimIn) {
        qr = div(aTileTool->aoi->P[1].x - aTileTool->aoi->P[0].x + 1, aTileTool->xDimEA) ;
        aTileTool->xTileNumber = qr.quot + ((qr.rem)? 1 : 0) ;
    }
    else {
        aTileTool->xTileNumber = 1 ;
    }

    if (aTileTool->yDimEA < aTileTool->yDimIn) {
        qr = div(aTileTool->aoi->P[3].y - aTileTool->aoi->P[0].y + 1, aTileTool->yDimEA) ;
        aTileTool->yTileNumber = qr.quot + ((qr.rem)? 1 : 0) ;
    }
    else {
        aTileTool->yTileNumber = 1 ;
    }

    /* Alloc data tile */
    /* We first release existing tile in case of reinitialization of a tile tool with other values */
    if (aTileTool->tile) {
        free(aTileTool->tile[0]) ;
        free(aTileTool->tile) ;
    }
    /* We then alloc new tile */
    if((aTileTool->tile = malloc(aTileTool->yTileSize * sizeof(void *))) == NULL) {
        ase("Failed to allocate tile data space") ;
    }
    if((aTileTool->tile[0] = malloc(aTileTool->xTileSize * aTileTool->yTileSize * aTileTool->typeSize)) == NULL) {
        ase("Failed to allocate tile data space") ;
    }
	/* Indexing of the column vector */
	for(iY = 1; iY < aTileTool->yTileSize; iY++)
		aTileTool->tile[iY] = aTileTool->tile[0] + iY * aTileTool->xTileSize * aTileTool->typeSize ;

    return ((void **)aTileTool->tile) ;
}


void resetTileToZero(tileTool *aTileTool)
{
    int iY ;
    char unsigned *aNullLine ;

    aNullLine = calloc(aTileTool->xTileSize, aTileTool->typeSize) ;
//    for (iX = 0; iX < aTileTool->xTileSize * aTileTool->typeSize; iX++) {
//        aNullLine[iX] = 0 ;
//    }
    for (iY = 0; iY < aTileTool->yTileSize; iY++) {
        memcpy(aTileTool->tile[iY], aNullLine, aTileTool->xTileSize * aTileTool->typeSize) ;
    }

    free(aNullLine) ;
}

void updateTileToolForIndexes(tileTool *aTileTool, int unsigned xTileIndex, int unsigned yTileIndex)
{
    int unsigned xShift, yShift ;

    aTileTool->xTileIndex = xTileIndex ;
    aTileTool->yTileIndex = yTileIndex ;

    /* Initialize values */
    aTileTool->leftBorder = aTileTool->rightBorder = aTileTool->bottomBorder = aTileTool->upBorder = 0 ;
    aTileTool->x0Tile = aTileTool->y0Tile = 0 ;
    aTileTool->xDimReadData = aTileTool->xTileSize ;
    aTileTool->yDimReadData = aTileTool->yTileSize ;

    /* Compute starting points and update values if on borders */
    /* Horizontal check */
    aTileTool->x0EA = aTileTool->xTileBorderSize ;
    aTileTool->x0In = aTileTool->aoi->P[0].x + aTileTool->xTileIndex * aTileTool->xDimEA - aTileTool->xTileBorderSize ;
    aTileTool->x0Out = aTileTool->xTileIndex * aTileTool->xDimEA ;
    /* Right border detection */
    xShift = aTileTool->x0In + aTileTool->xTileSize;
    if (xShift > aTileTool->aoi->P[2].x + 1) {
        aTileTool->rightBorder = 1 ;
        xShift -= aTileTool->aoi->P[2].x + 1 ;
        aTileTool->xDimReadData -= xShift ;
        /* If last tile Effective Area to be read is partially outside of area of interest, shift it backwards */
        xShift = aTileTool->x0In + aTileTool->xTileBorderSize + aTileTool->xDimEA;
        if(xShift > aTileTool->aoi->P[2].x + 1) {
            xShift -= aTileTool->aoi->P[2].x + 1 ;
            aTileTool->x0In -= xShift ;
            aTileTool->x0Out -= xShift ;
            aTileTool->xDimReadData += xShift ;
        }
    }
    /* Left border detection */
    if(aTileTool->x0In < 0) {
        aTileTool->leftBorder = 1 ;
        aTileTool->x0Tile = -aTileTool->x0In ;
        aTileTool->xDimReadData += aTileTool->x0In ;
        aTileTool->x0In = aTileTool->aoi->P[0].x ;
    }

    /* Vertical check */
    aTileTool->y0EA = aTileTool->yTileBorderSize ;
    aTileTool->y0In = aTileTool->aoi->P[0].y + aTileTool->yTileIndex * aTileTool->yDimEA - aTileTool->yTileBorderSize ;
    aTileTool->y0Out = aTileTool->yTileIndex * aTileTool->yDimEA ;
    /* Up border detection */
    yShift = aTileTool->y0In + aTileTool->yTileSize;
    if (yShift > aTileTool->aoi->P[2].y + 1) {
        aTileTool->upBorder = 1 ;
        yShift -= aTileTool->aoi->P[2].y + 1 ;
        aTileTool->yDimReadData -= yShift ;
        /* If last tile Effective Area to be read is partially outside of area of interest, shift it backwards */
        yShift = aTileTool->y0In + aTileTool->yTileBorderSize + aTileTool->yDimEA ;
        if(yShift > aTileTool->aoi->P[2].y + 1) {
            yShift -= aTileTool->aoi->P[2].y + 1 ;
            aTileTool->y0In -= yShift ;
            aTileTool->y0Out -= yShift ;
            aTileTool->yDimReadData += yShift ;
        }
    }
    /* Bottom border detection */
    if(aTileTool->y0In < 0) {
        aTileTool->bottomBorder = 1 ;
        aTileTool->y0Tile = -aTileTool->y0In ;
        aTileTool->yDimReadData += aTileTool->y0In ;
        aTileTool->y0In = aTileTool->aoi->P[0].y ;
    }
}


void **copyTileAtIndexes(tileTool *aTileTool, int unsigned xTileIndex, int unsigned yTileIndex)
{
    int iY ;
    size_t len ;
    long fileSeekVal ;
    void *src, *dest ;

    updateTileToolForIndexes(aTileTool, xTileIndex, yTileIndex) ;
//    resetTileToZero(aTileTool) ;

    len = aTileTool->typeSize * (aTileTool->xDimReadData) ;
    for (iY = 0; iY < aTileTool->yDimReadData; iY++) {
        fileSeekVal = ((aTileTool->y0In + iY) * aTileTool->xDimIn + aTileTool->x0In) * aTileTool->typeSize ;
        src = aTileTool->inputDataMatrix[0] + fileSeekVal ;
        dest = aTileTool->tile[iY + aTileTool->y0Tile] + aTileTool->x0Tile * aTileTool->typeSize ;

        memcpy(dest, src, len) ;
    }


    if (aTileTool->leftBorder) {
        mirrorDataLeft(aTileTool) ;
    }
    if (aTileTool->rightBorder) {
        mirrorDataRight(aTileTool) ;
    }
    if (aTileTool->bottomBorder) {
        mirrorDataBottom(aTileTool) ;
    }
    if (aTileTool->upBorder) {
        mirrorDataUp(aTileTool) ;
    }

    return ((void **)aTileTool->tile) ;
}


void **readTileAtIndexes(tileTool *aTileTool, int unsigned xTileIndex, int unsigned yTileIndex)
{
    int iY ;
    size_t val;
    long fileSeekVal ;

    updateTileToolForIndexes(aTileTool, xTileIndex, yTileIndex) ;
//    resetTileToZero(aTileTool) ;

    for (iY = 0; iY < aTileTool->yDimReadData; iY++) {
        fileSeekVal = ((aTileTool->y0In + iY) * aTileTool->xDimIn + aTileTool->x0In) * aTileTool->typeSize ;
        if(fseek(aTileTool->input->f, fileSeekVal, SEEK_SET) < 0)
            ase("Fail to seek") ;
        val = fread(aTileTool->tile[iY + aTileTool->y0Tile] + aTileTool->x0Tile * aTileTool->typeSize, aTileTool->typeSize, aTileTool->xDimReadData, aTileTool->input->f) ;
        if (val != aTileTool->xDimReadData) {
            printf("Short count of read values: %ld\n", val) ;
        }
    }

    if (aTileTool->leftBorder) {
        mirrorDataLeft(aTileTool) ;
    }
    if (aTileTool->rightBorder) {
        mirrorDataRight(aTileTool) ;
    }
    if (aTileTool->bottomBorder) {
        mirrorDataBottom(aTileTool) ;
    }
    if (aTileTool->upBorder) {
        mirrorDataUp(aTileTool) ;
    }

    return ((void **)aTileTool->tile) ;
}


void setMirroringForTileTool(tileTool *aTileTool)
{
    aTileTool->mirroring = 2 ;
}


void setCircularizationForTileTool(tileTool *aTileTool)
{
    aTileTool->mirroring = 1 ;
}


void mirrorDataLeft(tileTool *aTileTool)
{
    int iY, iX ;
    void *source, *dest ;

    source = NULL ;
    if(!aTileTool->mirroring) {
        source = calloc(1, aTileTool->typeSize) ;
    }

    for (iY = 0; iY < aTileTool->yTileSize; iY++) {
        for (iX = 0; iX < aTileTool->x0Tile; iX++) {
            if(aTileTool->mirroring == 1) {
                source = aTileTool->tile[iY] + (aTileTool->xTileSize - iX - 1) * aTileTool->typeSize ;
            }
            if(aTileTool->mirroring == 2) {
                source = aTileTool->tile[iY] + (aTileTool->x0Tile + iX) * aTileTool->typeSize ;
            }
            dest = aTileTool->tile[iY] + (aTileTool->x0Tile - iX - 1) * aTileTool->typeSize ;
            memcpy(dest, source, aTileTool->typeSize) ;
        }
    }
    if(!aTileTool->mirroring) {
        free(source) ;
    }
}


void mirrorDataRight(tileTool *aTileTool)
{
    int iY, iX ;
    void *source, *dest ;

    source = NULL ;
    if(!aTileTool->mirroring) {
        source = calloc(1, aTileTool->typeSize) ;
    }

    for (iY = 0; iY < aTileTool->yTileSize; iY++) {
        for (iX = 0; iX < aTileTool->xTileSize - aTileTool->xDimReadData; iX++) {
            if(aTileTool->mirroring == 1) {
                source = aTileTool->tile[iY] + iX * aTileTool->typeSize ;
            }
            if(aTileTool->mirroring == 2) {
                if ((aTileTool->xDimReadData - 2) < iX) {
                    break ;
                }
                source = aTileTool->tile[iY] + (aTileTool->xDimReadData - iX - 2) * aTileTool->typeSize ;
            }
            dest = aTileTool->tile[iY] + (aTileTool->xDimReadData + iX) * aTileTool->typeSize ;
            memcpy(dest, source, aTileTool->typeSize) ;
        }
    }
    if(!aTileTool->mirroring) {
        free(source) ;
    }
}


void mirrorDataBottom(tileTool *aTileTool)
{
    int iY ;
    void *source, *dest ;

    source = NULL ;
    if(!aTileTool->mirroring) {
        source = calloc(aTileTool->xTileSize, aTileTool->typeSize) ;
    }

    for (iY = 0; iY < aTileTool->y0Tile; iY++) {
        if(aTileTool->mirroring == 1) {
            source = aTileTool->tile[0] + (aTileTool->yTileSize - iY - 1) * aTileTool->xTileSize * aTileTool->typeSize ;
        }
        if(aTileTool->mirroring == 2) {
            source = aTileTool->tile[0] + (aTileTool->y0Tile + iY) * aTileTool->xTileSize * aTileTool->typeSize ;
        }
        dest = aTileTool->tile[0] + (aTileTool->y0Tile - iY - 1) * aTileTool->xTileSize * aTileTool->typeSize ;
        memcpy(dest, source, aTileTool->xTileSize * aTileTool->typeSize) ;
    }

    if(!aTileTool->mirroring) {
        free(source) ;
    }
}

void mirrorDataUp(tileTool *aTileTool)
{
    int iY ;
    void *source, *dest ;

    source = NULL ;
    if(!aTileTool->mirroring) {
        source = calloc(aTileTool->xTileSize, aTileTool->typeSize) ;
    }

    for (iY = 0; iY < aTileTool->yTileSize - aTileTool->yDimReadData; iY++) {
        if(aTileTool->mirroring == 1) {
            source = aTileTool->tile[0] + iY * aTileTool->xTileSize * aTileTool->typeSize ;
        }
        if(aTileTool->mirroring == 2) {
            if ((aTileTool->yDimReadData - 2) < iY) {
                break ;
            }
            source = aTileTool->tile[0] + (aTileTool->yDimReadData - iY - 2) * aTileTool->xTileSize * aTileTool->typeSize ;
        }
        dest = aTileTool->tile[0] + (aTileTool->yDimReadData + iY) * aTileTool->xTileSize * aTileTool->typeSize ;
        memcpy(dest, source, aTileTool->xTileSize * aTileTool->typeSize) ;
    }

    if(!aTileTool->mirroring) {
        free(source) ;
    }
}


void saveTile(tileTool *aTileTool)
{
    int iY ;
    size_t  len ;
    long fileSeekVal ;
    void *src, *dest ;

    len = aTileTool->typeSize * aTileTool->xDimEA ;
    for (iY = 0; iY < aTileTool->yDimEA; iY++) {
        fileSeekVal = ((aTileTool->y0Out + iY) * aTileTool->xDimOut + aTileTool->x0Out) * aTileTool->typeSize ;

        src = aTileTool->tile[iY + aTileTool->y0EA] + aTileTool->x0EA * aTileTool->typeSize ;
        dest = aTileTool->outputDataMatrix[0] + fileSeekVal ;

        memcpy(dest, src, len) ;
    }
}

void writeTile(tileTool *aTileTool)
{
    int iY ;
    size_t  val ;
    long fileSeekVal ;

    for (iY = 0; iY < aTileTool->yDimEA; iY++) {
        fileSeekVal = ((aTileTool->y0Out + iY) * aTileTool->xDimOut + aTileTool->x0Out) * aTileTool->typeSize ;
        if(fseek(aTileTool->output->f, fileSeekVal, SEEK_SET) < 0)
            ase("Failed to seek") ;
        val = fwrite(aTileTool->tile[iY + aTileTool->y0EA] + aTileTool->x0EA * aTileTool->typeSize, aTileTool->typeSize, aTileTool->xDimEA, aTileTool->output->f) ;
        if (val != aTileTool->xDimEA) {
            printf("Short count of written values: %ld\n", val) ;
        }
    }
    fflush(aTileTool->output->f) ;
}

void writeTileToOutputFile(tileTool *aTileTool, rawFileDescriptor *output)
{
    rawFileDescriptor *aFile ;

    aFile = aTileTool->output ;
    aTileTool->output = output ;
    writeTile(aTileTool) ;
    aTileTool->output = aFile ;
}


void registerDataTileInTileTool(void **data, tileTool *aTileTool)
{
    /* We first release existing tile */
    if (aTileTool->tile) {
        free(aTileTool->tile[0]) ;
        free(aTileTool->tile) ;
    }
    aTileTool->tile = (void **)data ;
}


void swapInAndOutInTileTool(tileTool *aTileTool)
{
    void **data ;

    data = aTileTool->inputDataMatrix ;
    aTileTool->inputDataMatrix = aTileTool->outputDataMatrix ;
    aTileTool->outputDataMatrix = data ;
}


float localAverageOfFloatTile(tileTool *aTileTool)
{
    int iX, iY ;
    float avg = 0 ;
    float **floatTile ;

    floatTile = (float **)aTileTool->tile ;
    for (iY = aTileTool->y0EA; iY < aTileTool->y0EA + aTileTool->yDimEA; iY++) {
        for (iX = aTileTool->x0EA; iX < aTileTool->x0EA + aTileTool->xDimEA; iX++) {
            avg += floatTile[iY][iX] ;
        }
    }
    avg /= (aTileTool->xDimEA * aTileTool->yDimEA) ;

    return(avg) ;
}

/* Tile tools arithmetics: tiles supposed of same size and same type. No controls done */
void multiplyTileTools(tileTool *M1, tileTool *M2, tileTool *M3)
{
    char **charTile1, **charTile2, **charTile3 ;
    int iX, iY, **intTile1, **intTile2, **intTile3 ;
    long **longTile1, **longTile2, **longTile3 ;
    float **floatTile1, **floatTile2, **floatTile3 ;
    double **doubleTile1, **doubleTile2, **doubleTile3 ;
    complexFloat **complexFloatTile1, **complexFloatTile2, **complexFloatTile3 ;
    tileTool *R ;

    R = M3 ;
    if (M3 == NULL) {
        R = M1 ;
    }

    if (!strncmp((const char *)M1->dataType, "char", 4)) {
        charTile1 = (char **)M1->tile ;
        charTile2 = (char **)M2->tile ;
        charTile3 = (char **)M3->tile ;
        for (iY = 0; iY < M1->yTileSize - 1; iY++) {
            for (iX = 0; iX < M1->xTileSize - 1; iX++) {
                charTile3[iY][iX] = charTile1[iY][iX] * charTile2[iY][iX] ;
            }
        }
    }

    if (!strncmp((const char *)M1->dataType, "int", 3)) {
        intTile1 = (int **)M1->tile ;
        intTile2 = (int **)M2->tile ;
        intTile3 = (int **)M3->tile ;
        for (iY = 0; iY < M1->yTileSize - 1; iY++) {
            for (iX = 0; iX < M1->xTileSize - 1; iX++) {
                intTile3[iY][iX] = intTile2[iY][iX] * intTile2[iY][iX] ;
            }
        }
    }

    if (!strncmp((const char *)M1->dataType, "long", 4)) {
        longTile1 = (long **)M1->tile ;
        longTile2 = (long **)M2->tile ;
        longTile3 = (long **)M3->tile ;
        for (iY = 0; iY < M1->yTileSize - 1; iY++) {
            for (iX = 0; iX < M1->xTileSize - 1; iX++) {
                longTile3[iY][iX] = longTile1[iY][iX] * longTile2[iY][iX] ;
            }
        }
    }

    if (!strncmp((const char *)M1->dataType, "float", 5)) {
        floatTile1 = (float **)M1->tile ;
        floatTile2 = (float **)M2->tile ;
        floatTile3 = (float **)M3->tile ;
        for (iY = 0; iY < M1->yTileSize - 1; iY++) {
            for (iX = 0; iX < M1->xTileSize - 1; iX++) {
                floatTile3[iY][iX] = floatTile1[iY][iX] * floatTile2[iY][iX] ;
            }
        }
    }

    if (!strncmp((const char *)M1->dataType, "double", 6)) {
        doubleTile1 = (double **)M1->tile ;
        doubleTile2 = (double **)M2->tile ;
        doubleTile3 = (double **)M3->tile ;
        for (iY = 0; iY < M1->yTileSize - 1; iY++) {
            for (iX = 0; iX < M1->xTileSize - 1; iX++) {
                doubleTile3[iY][iX] = doubleTile1[iY][iX] * doubleTile2[iY][iX] ;
            }
        }
    }

    if (strstr((const char *)M1->dataType, "complexFloat")) {
        complexFloatTile1 = (complexFloat **)M1->tile ;
        complexFloatTile2 = (complexFloat **)M2->tile ;
        complexFloatTile3 = (complexFloat **)M3->tile ;
        for (iY = 0; iY < M1->yTileSize - 1; iY++) {
            for (iX = 0; iX < M1->xTileSize - 1; iX++) {
                complexFloatTile3[iY][iX] = mC(complexFloatTile1[iY][iX], complexFloatTile2[iY][iX]) ;
            }
        }
    }
}
