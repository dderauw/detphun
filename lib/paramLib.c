/*
 *  readParamLib.c
 *  2ESARFormat
 *
 *  Created by Dominique Derauw on 16/02/06.
 *  Copyright 2006 Dominique Derauw. All rights reserved.
 *
 */

#include "paramLib.h"

char unsigned plError ;
char aTextLine[TEXT_LINE_MAX_LENGTH + 1] ;


/* One key - value pair allocation  */
keyValue *allocAKeyValuePair(void)
{
	keyValue *p ;

	if((p = malloc(sizeof(keyValue))) == NULL) {
		perror("Unable to allocate parameter list") ;
		exit(-1) ;
	}

	p->key = initStringWithString("endOfFile") ;
	p->stringVal = NULL ;
	p->type = NULL ;
	p->rank = 0 ;
	p->dimensions = NULL ;
	p->value = NULL ;
	p->nextOne = (keyValue *)NULL ;

	return p ;
}

void resetKeyValuePair(keyValue *aKeyValuePair)
{
	if(aKeyValuePair->stringVal != NULL) {
		free(aKeyValuePair->stringVal) ;
		aKeyValuePair->stringVal = NULL ;
	}
	if(aKeyValuePair->type != NULL) {
		free(aKeyValuePair->type) ;
		aKeyValuePair->type = NULL;
	}
	if(aKeyValuePair->dimensions != NULL) {
		free(aKeyValuePair->dimensions) ;
		aKeyValuePair->dimensions = NULL;
	}
	if(aKeyValuePair->value != NULL) {
		free(aKeyValuePair->value) ;
		aKeyValuePair->value = NULL ;
	}
}

/* Free a list of key - value pairs */
void freeKeyValueList(keyValue *parameterList)
{
    keyValue *nextOne ;

    if (parameterList) {
        while(strcmp(parameterList->key, "endOfFile")) {
            nextOne = parameterList->nextOne ;
            freeKeyValuePair(parameterList) ;
            parameterList = nextOne ;
        }
        free(parameterList->key) ;
        free(parameterList) ;
    }
}


/* Free a key-value pair */
void freeKeyValuePair(keyValue *aKeyValuePair)
{
	if(aKeyValuePair->key != NULL)
		free(aKeyValuePair->key) ;
	if(aKeyValuePair->stringVal != NULL)
		free(aKeyValuePair->stringVal) ;
	if(aKeyValuePair->type != NULL)
		free(aKeyValuePair->type) ;
	if(aKeyValuePair->dimensions != NULL)
		free(aKeyValuePair->dimensions) ;
	if(aKeyValuePair->value != NULL)
		free(aKeyValuePair->value) ;
	free(aKeyValuePair) ;
}


/* Parameter files are supposed to be lists of Values followed by a Key formatted as a C comment */
/* If a line begins like a C comment, it is considered as such */
/* All read values are stored as string Values */

keyValue *readParameterFileAtPath(char *aPath)
{
	char *stringVal = NULL ;
	size_t stringValLength, keyIndex ;
	keyValue *parameterList, *aKeyValPair ;
	rawFileDescriptor *parameterFile ;

    aPath = expendEnvironmentVariablesInPath(aPath) ;
	if(!fileExistsAtPath(aPath)) {
		sprintf(aTextLine, "Parameter File: File not found at path:%s\n", aPath) ;
		perror(aTextLine) ;
		free(aPath) ;
		exit(-1) ;
	}

	if((parameterFile = openRawFileForReadingAtPath(aPath)) == NULL) {
		sprintf(aTextLine, "Parameter File: Read acces denied at path:%s\n", aPath) ;
		perror(aTextLine) ;
		free(aPath) ;
		exit(-1) ;
	}

    /* We allocate a starting key-Value pair */
	aKeyValPair = parameterList = allocAKeyValuePair() ;

    /* Then we start reading file line by line */
	while(fgets(aTextLine, TEXT_LINE_MAX_LENGTH, parameterFile->f) != NULL) {
        /* We localise the starting position of the key, which is also the initial length of the value, and we compute the length of the key */
		keyIndex = stringValLength = findStringValLengthIn(aTextLine) ;

        /* Since we read a line, we are not at the end of the key-value pair chaine, so we free the current "endOfFile" key */
		free(aKeyValPair->key) ;

		if(stringValLength) {
            /* We found a value, so we save it in stringVal to allow stripping white chars at its end */
            stringVal = allocStringOfLength(stringValLength) ;
            memcpy(stringVal, aTextLine, stringValLength) ;
            stripWhiteCharsAtEndOfString(stringVal) ;

            /* We test if stringVal is empty. If yes, value is missing. So, we save the white cahr string... */
            if (!strlen(stringVal)) {
                memcpy(stringVal, aTextLine, stringValLength) ;
            }
            /* If not, we store the key-value pair */
			aKeyValPair->stringVal = initStringWithString(stringVal) ;
			aKeyValPair->key = initStringWithString(&aTextLine[keyIndex]) ;

			free(stringVal) ;
		}
		else {
            /* If there is no value, we deal with a simple comment or an empty line */
            /* The comment is transferd as the value of the key-value pair, including the "end of line" char */
            aKeyValPair->stringVal = initStringWithString(&aTextLine[keyIndex]) ;
            aKeyValPair->key = initStringWithString("aComment") ;
		}

		aKeyValPair->nextOne = allocAKeyValuePair() ;
		aKeyValPair = aKeyValPair->nextOne ;
	}
    if (ferror(parameterFile->f)) {
        perror("-/->\t") ;
    }

	freeRawFileDescriptor(parameterFile) ;
	free(aPath) ;

	return(parameterList) ;
}

/* As expected */
/* Nowadays, key - value pairs of rank greater than zero are  written in separate txt files as a single column vector */
void writeParameterFileAtPath(keyValue *keyValueList, char *aPath)
{
	char *aVectorPath, *extension, *strippedFilePath, *vectorDirectory ;
	size_t stringValLength, keyLength ;
	rawFileDescriptor *parameterFile ;

	aPath = expendEnvironmentVariablesInPath(aPath) ;
	if((parameterFile = openRawFileForWritingAtPath(aPath)) == NULL) {
		fprintf(stdout, "Failed to create parameter file at path %s\n", aPath) ;
		free(aPath) ;
        ase("Failed to save parameter file") ;
		exit(-1) ;
	}
    strippedFilePath = initStringWithString(aPath) ;
    stripExtensionAtEndOfPath(strippedFilePath) ;
    vectorDirectory = initStringWith2Strings(strippedFilePath, ".Vectors") ;

	while(strcmp(keyValueList->key, "endOfFile")) {
        if(!strcmp(keyValueList->key, "aComment")) {
            fprintf(parameterFile->f, "%s", keyValueList->stringVal) ;
        }
        else {
            stringValLength = strlen(keyValueList->stringVal) ;
            if(stringValLength < 40)
                fprintf(parameterFile->f, "%-40s\t\t%s", keyValueList->stringVal, keyValueList->key) ;
            else
                fprintf(parameterFile->f, "%s\t\t%s", keyValueList->stringVal, keyValueList->key) ;
        }
        if (!strncmp(keyValueList->stringVal, "Vector", 6)) {
            keyLength = strlen(keyValueList->key) - 6 ;
            extension = allocStringOfLength(keyLength) ;
            strncpy(extension, &keyValueList->key[3], keyLength) ;
            stripWhiteCharsAtEndOfString(extension) ;
            replaceWhiteCharsInStringByChar(extension, '_') ;
            replaceSubStringInString(extension, "/", "_") ;

            aVectorPath = initStringWith4Strings(vectorDirectory, "/", extension, ".txt") ;
            if(!isDir(vectorDirectory)) {
                if(mkdir((const char *)vectorDirectory, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)) {
                    aVectorPath = initStringWith4Strings(strippedFilePath, "->", extension, ".txt") ;
                }
            }
            writeVectorAtPath(keyValueList, aVectorPath) ;

            free(aVectorPath) ;
            free(extension) ;
        }

		keyValueList = keyValueList->nextOne ;
	}

	free(aPath) ;
    free(strippedFilePath) ;
    free(vectorDirectory) ;
	freeRawFileDescriptor(parameterFile) ;
}

/* Scan parameter list by key value and return the corresponding string value */
/* Key scan begins at the third character of the key string */
/* So, the / *[space] is not part of the keyword */
/* strncmp is used in order to avoid considering closing aTextLine characters as part of the keyword */
/* or to allow using the first string of the aTextLine as keyword */
char *getStringValForKeyIn(keyValue *parameterList, char *aKey)
{
	plError = errno = NO_ERROR ;
	while(strcmp(parameterList->key, "endOfFile")) {
		if(!strncmp(&parameterList->key[3], aKey, strlen(aKey)))
			return(parameterList->stringVal) ;
		parameterList = parameterList->nextOne ;
	}
//	fprintf(stdout, "Keyword /* %s */ not found in parameter list\n", aKey) ;
	plError = errno = KEY_VALUE_NOT_FOUND ;
	return(NULL) ;
}

/* Set a new value to the corresponding key key-value paire or add a new one if not found */
keyValue *setStringValForKeyIn(keyValue *parameterList, char *theValue, char *theKey)
{
	size_t	keyLength, stringValLength ;

	keyLength = strlen(theKey) ;
	stringValLength = strlen(theValue) ;
	while(strcmp(parameterList->key, "endOfFile")) {
        /* If the key-value pair already exists, replace the value with the new one */
		if(keyLength && (stringValLength) && (!strncmp(&parameterList->key[3], theKey, keyLength))) {
			free(parameterList->stringVal) ;
			parameterList->stringVal = allocStringOfLength(stringValLength) ;
			strncpy(parameterList->stringVal, theValue, stringValLength) ;
			if(parameterList->type != NULL)
				free(parameterList->type) ;
			parameterList->type = initStringWithString("STRING") ;
			return(parameterList) ;
		}
        /* If we deal with a comment (keyLength = 0), we do nothing even if an identical comment already exists */
        /* The comment will in any case be added as a new comment key-value pair */
		parameterList = parameterList->nextOne ;
	}

	/* If Key not found, add a new key - value pair to the parameter list */
	parameterList->nextOne = allocAKeyValuePair() ;
	free(parameterList->key) ;
	keyLength = strlen(theKey) + 8 ;
	parameterList->key = allocStringOfLength(keyLength) ;
	if(keyLength == 8)
		sprintf(parameterList->key, "aComment") ;
	else
		sprintf(parameterList->key, "/* %s */\n", theKey) ;

	if(!strcmp(parameterList->key, "aComment")) {
		if(stringValLength) {
			parameterList->stringVal = allocStringOfLength(stringValLength + 7) ;
			sprintf(parameterList->stringVal, "/* %s */\n", theValue) ;
		}
		else {
			parameterList->stringVal = allocStringOfLength(1) ;
			sprintf(parameterList->stringVal, "\n") ;
		}
	}
	else {
		parameterList->stringVal = allocStringOfLength(stringValLength) ;
		strncpy(parameterList->stringVal, theValue, stringValLength) ;
	}

	if(parameterList->type != NULL)
		free(parameterList->type) ;
	parameterList->type = initStringWithString("STRING") ;

	return(parameterList) ;
}

/* Find the length of the string value field */
size_t	findStringValLengthIn(char *aString)
{
	size_t i, N ;

	N = strlen(aString) ;
	for(i = 0; i < N; i++) {
		if(!strncmp(&aString[i], "/* ", 3))
			return(i) ;
	}
	return(0) ;
}

/* Set type of date for a given key-value pair */
char setTypeForKeyIn(keyValue *parameterList, char *theType, char *theKey)
{
	size_t	keyLength, typeLength ;

	keyLength = strlen(theKey) ;
	typeLength = strlen(theType) ;
	while(strcmp(parameterList->key, "endOfFile")) {
		if(!strncmp(&parameterList->key[3], theKey, keyLength)) {
			if(parameterList->type != NULL) free(parameterList->type) ;
			parameterList->type = allocStringOfLength(typeLength) ;
			strncpy(parameterList->type, theType, typeLength) ;
			return(TYPE_SET_FOR_KEY) ;
		}
		parameterList = parameterList->nextOne ;
	}

	return(KEY_VALUE_NOT_FOUND) ;
}

/* Update values in parameter list with respect to defined types */
void updateValuesIn(keyValue *parameterList)
{
	while(strcmp(parameterList->key, "endOfFile")) {
		if(parameterList->type != NULL) {
			if(!strcmp(parameterList->type, "string"))
				parameterList->value = readFirstStringIn(parameterList->stringVal) ;

			if(!strcmp(parameterList->type, "char")) {
				if(parameterList->value == NULL) {
					parameterList->value = (char *)malloc(sizeof(char)) ;
					sscanf(parameterList->stringVal, "%hhd ", (char signed *)parameterList->value) ;
				}
			}

			if(!strcmp(parameterList->type, "short")) {
				if(parameterList->value == NULL) {
					parameterList->value = (short *)malloc(sizeof(short)) ;
					sscanf(parameterList->stringVal, "%hd ", (short *)parameterList->value) ;
				}
			}

			if(!strcmp(parameterList->type, "int")) {
				if(parameterList->value == NULL) {
					parameterList->value = (int *)malloc(sizeof(int)) ;
					sscanf(parameterList->stringVal, "%d ", (int *)parameterList->value) ;
				}
			}

			if(!strcmp(parameterList->type, "long")) {
				if(parameterList->value == NULL) {
					parameterList->value = (long *)malloc(sizeof(long)) ;
					sscanf(parameterList->stringVal, "%ld ", (long *)parameterList->value) ;
				}
			}

			if(!strcmp(parameterList->type, "float")) {
				if(parameterList->value == NULL) {
					parameterList->value = (float *)malloc(sizeof(float)) ;
					sscanf(parameterList->stringVal, "%f ", (float *)parameterList->value) ;
				}
			}

			if(!strcmp(parameterList->type, "double")) {
				if(parameterList->value == NULL) {
					parameterList->value = (double *)malloc(sizeof(double)) ;
					sscanf(parameterList->stringVal, "%lf ", (double *)parameterList->value) ;
				}
			}
		}
		parameterList = parameterList->nextOne ;
	}
}

void *getValueForKeyIn(keyValue *parameterList, char *aKey)
{
	while(strcmp(parameterList->key, "endOfFile")) {
		if(!strncmp(&parameterList->key[3], aKey, strlen(aKey)))
			return(parameterList->value) ;
		parameterList = parameterList->nextOne ;
	}
	fprintf(stdout, "Keyword %s not found in parameter list\n", aKey) ;
	return(NULL) ;
}

int getIntValForKeyIn(keyValue *parameterList, char *aKey)
{
	char *aString ;
	int anInt = 0 ;

	plError = NO_ERROR ;
	aString = getStringValForKeyIn(parameterList, aKey) ;
	if(aString != NULL)
		sscanf(aString, "%d ", &anInt) ;
	else plError = KEY_VALUE_NOT_FOUND ;

	return(anInt) ;
}


float getFloatValForKeyIn(keyValue *parameterList, char *aKey)
{
	char *aString ;
	float aFloat = 0. ;

	plError = NO_ERROR ;
	aString = getStringValForKeyIn(parameterList, aKey) ;
	if(aString != NULL)
		sscanf(aString, "%g ", &aFloat) ;
	else plError = KEY_VALUE_NOT_FOUND ;

	return(aFloat) ;
}


double getDoubleValForKeyIn(keyValue *parameterList, char *aKey)
{
	char *aString ;
	double aDouble = 0. ;

	plError = NO_ERROR ;
	aString = getStringValForKeyIn(parameterList, aKey) ;
	if(aString != NULL)
		sscanf(aString, "%lg ", &aDouble) ;
	else plError = KEY_VALUE_NOT_FOUND ;

	return(aDouble) ;
}


keyValue *setVoidPointerValForKey(keyValue *parameterList, void *aPointer, char *theKey)
{
    keyValue *currentKeyValuePair ;

    currentKeyValuePair = setStringValForKeyIn(parameterList, "void *", theKey) ;
	if(currentKeyValuePair->type != NULL)
		free(currentKeyValuePair->type) ;
    currentKeyValuePair->type = initStringWithString("VOID *") ;
    currentKeyValuePair->value = aPointer ;


    return(currentKeyValuePair) ;

}


keyValue *setUCharValForKeyIn(keyValue *parameterList, char unsigned anUChar, char *theKey)
{
	char	stringVal[80] ;
	keyValue *currentKeyValuePair ;

	sprintf(stringVal, "%d", anUChar) ;
	currentKeyValuePair = setStringValForKeyIn(parameterList, stringVal, theKey) ;
	if(currentKeyValuePair->type != NULL)
		free(currentKeyValuePair->type) ;
	currentKeyValuePair->type = initStringWithString("UCHAR") ;

	return(currentKeyValuePair) ;
}


keyValue *setShortValForKeyIn(keyValue *parameterList, short aShort, char *theKey)
{
	char	stringVal[80] ;
	keyValue *currentKeyValuePair ;

	sprintf(stringVal, "%d", aShort) ;
	currentKeyValuePair = setStringValForKeyIn(parameterList, stringVal, theKey) ;
	if(currentKeyValuePair->type != NULL)
		free(currentKeyValuePair->type) ;
	currentKeyValuePair->type = initStringWithString("SHORT") ;

	return(currentKeyValuePair) ;
}


keyValue *setIntValForKeyIn(keyValue *parameterList, int anInt, char *theKey)
{
	char	stringVal[80] ;
	keyValue *currentKeyValuePair ;

	sprintf(stringVal, "%d", anInt) ;
	currentKeyValuePair = setStringValForKeyIn(parameterList, stringVal, theKey) ;
	if(currentKeyValuePair->type != NULL)
		free(currentKeyValuePair->type) ;
	currentKeyValuePair->type = initStringWithString("INT") ;

	return(currentKeyValuePair) ;
}


keyValue *setLongValForKeyIn(keyValue *parameterList, long aLong, char *theKey)
{
	char	stringVal[80] ;
	keyValue *currentKeyValuePair ;

	sprintf(stringVal, "%ld", aLong) ;
	currentKeyValuePair = setStringValForKeyIn(parameterList, stringVal, theKey) ;
	if(currentKeyValuePair->type != NULL)
		free(currentKeyValuePair->type) ;
	currentKeyValuePair->type = initStringWithString("LONG") ;

	return(currentKeyValuePair) ;
}


keyValue *setFloatValForKeyIn(keyValue *parameterList, float aFloat, char *theKey)
{
	char	stringVal[80] ;
	keyValue *currentKeyValuePair ;

	sprintf(stringVal, "%.8g", aFloat) ;
	currentKeyValuePair = setStringValForKeyIn(parameterList, stringVal, theKey) ;
	if(currentKeyValuePair->type != NULL)
		free(currentKeyValuePair->type) ;
	currentKeyValuePair->type = initStringWithString("FLOAT") ;

	return(currentKeyValuePair) ;
}


keyValue *setDoubleValForKeyIn(keyValue *parameterList, double aDouble, char *theKey)
{
	char	stringVal[80] ;
	keyValue *currentKeyValuePair ;

	sprintf(stringVal, "%.16g", aDouble) ;
	currentKeyValuePair = setStringValForKeyIn(parameterList, stringVal, theKey) ;
	if(currentKeyValuePair->type != NULL)
		free(currentKeyValuePair->type) ;
	currentKeyValuePair->type = initStringWithString("DOUBLE") ;

	return(currentKeyValuePair) ;
}


keyValue *setVectorUCharForKeyIn(keyValue *parameterList, char unsigned *anUCharVector, int vectorLength, char *theKey)
{
	char	stringVal[80] ;
	keyValue *currentKeyValuePair ;

	if((currentKeyValuePair = isKeyValPresent(parameterList, theKey)) != NULL)
		resetKeyValuePair(currentKeyValuePair) ;

	if(vectorLength == 1) {
		currentKeyValuePair = setUCharValForKeyIn(parameterList, anUCharVector[0], theKey) ;
	}
	else {
		sprintf(stringVal, "Vector UCHAR of length %d", vectorLength) ;
		currentKeyValuePair = setStringValForKeyIn(parameterList, stringVal, theKey) ;
	}

	if(currentKeyValuePair->type != NULL) free(currentKeyValuePair->type) ;
	currentKeyValuePair->type = initStringWithString("VECTOR UCHAR") ;
	currentKeyValuePair->rank = 1 ;
	currentKeyValuePair->value = anUCharVector ;
	currentKeyValuePair->dimensions = vectorInt(0, 0) ;
	currentKeyValuePair->dimensions[0] = vectorLength ;

	return(currentKeyValuePair) ;
}


keyValue *setVectorShortForKeyIn(keyValue *parameterList, short *aShortVector, int vectorLength, char *theKey)
{
	char	stringVal[80] ;
	keyValue *currentKeyValuePair ;

	if((currentKeyValuePair = isKeyValPresent(parameterList, theKey)) != NULL)
		resetKeyValuePair(currentKeyValuePair) ;

	if(vectorLength == 1) {
		currentKeyValuePair = setShortValForKeyIn(parameterList, aShortVector[0], theKey) ;
	}
	else {
		sprintf(stringVal, "Vector SHORT of length %d", vectorLength) ;
		currentKeyValuePair = setStringValForKeyIn(parameterList, stringVal, theKey) ;
	}

	if(currentKeyValuePair->type != NULL) free(currentKeyValuePair->type) ;
	currentKeyValuePair->type = initStringWithString("VECTOR SHORT") ;
	currentKeyValuePair->rank = 1 ;
	currentKeyValuePair->value = aShortVector ;
	currentKeyValuePair->dimensions = vectorInt(0, 0) ;
	currentKeyValuePair->dimensions[0] = vectorLength ;

	return(currentKeyValuePair) ;
}


keyValue *setVectorIntForKeyIn(keyValue *parameterList, int *anIntVector, int vectorLength, char *theKey)
{
	char	stringVal[80] ;
	keyValue *currentKeyValuePair ;

	if((currentKeyValuePair = isKeyValPresent(parameterList, theKey)) != NULL)
		resetKeyValuePair(currentKeyValuePair) ;

	if(vectorLength == 1) {
		currentKeyValuePair = setIntValForKeyIn(parameterList, anIntVector[0], theKey) ;
	}
	else {
		sprintf(stringVal, "Vector INT of length %d", vectorLength) ;
		currentKeyValuePair = setStringValForKeyIn(parameterList, stringVal, theKey) ;
	}

	if(currentKeyValuePair->type != NULL) free(currentKeyValuePair->type) ;
	currentKeyValuePair->type = initStringWithString("VECTOR INT") ;
	currentKeyValuePair->rank = 1 ;
	currentKeyValuePair->value = anIntVector ;
	currentKeyValuePair->dimensions = vectorInt(0, 0) ;
	currentKeyValuePair->dimensions[0] = vectorLength ;

	return(currentKeyValuePair) ;
}


keyValue *setVectorLongForKeyIn(keyValue *parameterList, long *aLongVector, int vectorLength, char *theKey)
{
	char	stringVal[80] ;
	keyValue *currentKeyValuePair ;

	if((currentKeyValuePair = isKeyValPresent(parameterList, theKey)) != NULL)
		resetKeyValuePair(currentKeyValuePair) ;

	if(vectorLength == 1) {
		currentKeyValuePair = setLongValForKeyIn(parameterList, aLongVector[0], theKey) ;
	}
	else {
		sprintf(stringVal, "Vector LONG of length %d", vectorLength) ;
		currentKeyValuePair = setStringValForKeyIn(parameterList, stringVal, theKey) ;
	}

	if(currentKeyValuePair->type != NULL) free(currentKeyValuePair->type) ;
	currentKeyValuePair->type = initStringWithString("VECTOR LONG") ;
	currentKeyValuePair->rank = 1 ;
	currentKeyValuePair->value = aLongVector ;
	currentKeyValuePair->dimensions = vectorInt(0, 0) ;
	currentKeyValuePair->dimensions[0] = vectorLength ;

	return(currentKeyValuePair) ;
}


keyValue *setVectorFloatForKeyIn(keyValue *parameterList, float *aFloatVector, int vectorLength,  char *theKey)
{
	char	stringVal[80] ;
	keyValue *currentKeyValuePair ;

	if((currentKeyValuePair = isKeyValPresent(parameterList, theKey)) != NULL)
		resetKeyValuePair(currentKeyValuePair) ;

	if(vectorLength == 1) {
		currentKeyValuePair = setFloatValForKeyIn(parameterList, aFloatVector[0], theKey) ;
	}
	else {
		sprintf(stringVal, "Vector FLOAT of length %d", vectorLength) ;
		currentKeyValuePair = setStringValForKeyIn(parameterList, stringVal, theKey) ;
	}

	if(currentKeyValuePair->type != NULL) free(currentKeyValuePair->type) ;
	currentKeyValuePair->type = initStringWithString("VECTOR FLOAT") ;
	currentKeyValuePair->rank = 1 ;
	currentKeyValuePair->value = aFloatVector ;
	currentKeyValuePair->dimensions = vectorInt(0, 0) ;
	currentKeyValuePair->dimensions[0] = vectorLength ;

	return(currentKeyValuePair) ;
}


keyValue *setVectorDoubleForKeyIn(keyValue *parameterList, double *aDoubleVector, int vectorLength,  char *theKey)
{
	char	stringVal[80] ;
	keyValue *currentKeyValuePair ;

	if((currentKeyValuePair = isKeyValPresent(parameterList, theKey)) != NULL)
		resetKeyValuePair(currentKeyValuePair) ;

	if(vectorLength == 1) {
		currentKeyValuePair = setDoubleValForKeyIn(parameterList, aDoubleVector[0], theKey) ;
	}
	else {
		sprintf(stringVal, "Vector DOUBLE of length %d", vectorLength) ;
		currentKeyValuePair = setStringValForKeyIn(parameterList, stringVal, theKey) ;
	}

	if(currentKeyValuePair->type != NULL) free(currentKeyValuePair->type) ;
	currentKeyValuePair->type = initStringWithString("VECTOR DOUBLE") ;
	currentKeyValuePair->rank = 1 ;
	currentKeyValuePair->value = aDoubleVector ;
	currentKeyValuePair->dimensions = vectorInt(0, 0) ;
	currentKeyValuePair->dimensions[0] = vectorLength ;

	return(currentKeyValuePair) ;
}


/* Remove key-value paire in parameter list */
char removeKeyValuePairIn(keyValue *parameterList, char *theKey)
{
    size_t keyLength ;
    keyValue *preceedingKeyValuePair ;

    keyLength = strlen(theKey) ;
    preceedingKeyValuePair = parameterList ;
    parameterList = parameterList->nextOne ;
    while(strcmp(parameterList->key, "endOfFile")) {
        if(keyLength && (!strncmp(&parameterList->key[3], theKey, keyLength))) {
            preceedingKeyValuePair->nextOne = parameterList->nextOne ;
            freeKeyValuePair(parameterList) ;
            return(KEY_VALUE_REMOVED) ;
        }
        preceedingKeyValuePair = parameterList ;
        parameterList = parameterList->nextOne ;
    }
    /* If Key not found... */
    return(KEY_VALUE_NOT_FOUND) ;
}


/* Remove comment in parameter list */
char removeCommentIn(keyValue *parameterList, char *aComment)
{
    char *aString ;
    size_t stringLength ;
    keyValue *preceedingKeyValuePair ;

    if (!strlen(aComment)) {
        return(KEY_VALUE_NOT_FOUND) ;
    }
    aString = initStringWith2Strings("/* ", aComment) ;
    stringLength = strlen(aString) ;
    preceedingKeyValuePair = parameterList ;
    parameterList = parameterList->nextOne ;
    while(strcmp(parameterList->key, "endOfFile")) {
        if(!strncmp(parameterList->stringVal, aString, stringLength)) {
            free(aString) ;
            preceedingKeyValuePair->nextOne = parameterList->nextOne ;
            freeKeyValuePair(parameterList) ;
            return(KEY_VALUE_REMOVED) ;
        }
        preceedingKeyValuePair = parameterList ;
        parameterList = parameterList->nextOne ;
    }
    /* If Key not found... */
    free(aString) ;
    return(KEY_VALUE_NOT_FOUND) ;
}


keyValue *isKeyValPresent(keyValue *parameterList, char *aKey)
{
    plError = NO_ERROR ;

    while(strcmp(parameterList->key, "endOfFile")) {
        if(!strncmp(&parameterList->key[3], aKey, strlen(aKey)))
            return(parameterList) ;
        parameterList = parameterList->nextOne ;
    }

    plError = KEY_VALUE_NOT_FOUND ;
    return(NULL) ;
}

keyValue *isCommentPresentIn(keyValue *parameterList, char *aComment)
{
    plError = NO_ERROR ;

    if (!strlen(aComment)) {
        plError = COMMENT_NOT_FOUND ;
        return(NULL) ;
    }

    while(strcmp(parameterList->key, "endOfFile")) {
        if(!strncmp(parameterList->stringVal + 3, aComment, strlen(aComment))) {
            return(parameterList) ;
        }
        parameterList = parameterList->nextOne ;
    }

    plError = COMMENT_NOT_FOUND ;
    return(NULL) ;
}

void writeVectorAtPath(keyValue *aKeyValue, char *aVectorFilePath)
{
    int typeSize, i ;
    rawFileDescriptor *vectorTextFile ;

    if((vectorTextFile = openRawFileForWritingAtPath(aVectorFilePath)) == NULL) {
        fprintf(stdout, "Failed to create Vector text file at path %s\n", aVectorFilePath) ;
        exit(-1) ;
    }

    if (!strcmp(aKeyValue->type, "VECTOR CHAR")) {
        typeSize = sizeof(char) ;
        for (i = 0; i < aKeyValue->dimensions[0]; i++) {
            fprintf(vectorTextFile->f, "%hhd\n", *((char *)(aKeyValue->value + i * typeSize))) ;
        }
    }

    if (!strcmp(aKeyValue->type, "VECTOR SHORT")) {
        typeSize = sizeof(short) ;
        for (i = 0; i < aKeyValue->dimensions[0]; i++) {
            fprintf(vectorTextFile->f, "%hd\n", *((short *)(aKeyValue->value + i * typeSize))) ;
        }
    }

    if (!strcmp(aKeyValue->type, "VECTOR INT")) {
        typeSize = sizeof(int) ;
        for (i = 0; i < aKeyValue->dimensions[0]; i++) {
            fprintf(vectorTextFile->f, "%d\n", *((int *)(aKeyValue->value + i * typeSize))) ;
        }
    }

    if (!strcmp(aKeyValue->type, "VECTOR LONG")) {
        typeSize = sizeof(long) ;
        for (i = 0; i < aKeyValue->dimensions[0]; i++) {
            fprintf(vectorTextFile->f, "%ld\n", *((long *)(aKeyValue->value + i * typeSize))) ;
        }
    }

    if (!strcmp(aKeyValue->type, "VECTOR FLOAT")) {
        typeSize = sizeof(float) ;
        for (i = 0; i < aKeyValue->dimensions[0]; i++) {
            fprintf(vectorTextFile->f, "%.8g\n", *((float *)(aKeyValue->value + i * typeSize))) ;
        }
    }

    if (!strcmp(aKeyValue->type, "VECTOR DOUBLE")) {
        typeSize = sizeof(double) ;
        for (i = 0; i < aKeyValue->dimensions[0]; i++) {
            fprintf(vectorTextFile->f, "%.16g\n", *((double *)(aKeyValue->value + i * typeSize))) ;
        }
    }

    freeRawFileDescriptor(vectorTextFile) ;
}

keyValue *setCSVValForKeyIn(keyValue *parameterList, CSVStruct *csv, char *theKey)
{
    char	*stringVals[2] ;
    int i, index0, index1 ;
    keyValue *currentKeyValuePair ;

    index0 = index1 = 0 ;
    stringVals[index0] = initStringWithString(csv->stringVal[0]) ;
    for (i = 1; i < csv->numberOfEntries; i++) {
        index0 = (index0)? 0 : 1 ;
        stringVals[index0] = initStringWith3Strings(stringVals[index1], ", ", csv->stringVal[i]) ;
        free(stringVals[index1]) ;
        index1 = (index1)? 0 : 1 ;
    }

    currentKeyValuePair = setStringValForKeyIn(parameterList, stringVals[index0], theKey) ;
    if(currentKeyValuePair->type != NULL)
        free(currentKeyValuePair->type) ;
    currentKeyValuePair->type = initStringWithString("CSV") ;

    free(stringVals[index0]) ;

    return(currentKeyValuePair) ;
}


CSVStruct *getCSVValForKeyIn(keyValue *parameterList, char *aKey)
{
    char *aString ;
    CSVStruct *csv = NULL ;

    plError = NO_ERROR ;
    aString = getStringValForKeyIn(parameterList, aKey) ;
    if(aString != NULL)
        csv = readCSVInString(aString) ;
    else plError = KEY_VALUE_NOT_FOUND ;

    return(csv) ;
}

