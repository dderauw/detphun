/* bibfiltre.c */
/* bibliothèque des fonctions utilisées pour le filtrage */
/*
 *  Created by Dominique Derauw a long time ago.
 *  Copyright (c) 1995 Dominique Derauw. All rights reserved.
 *
 */

#include "interferogramFiltering.h"


/* ---------------------------------------------- */
void convertFloatPhaseTileToComplex(tileTool *phaseTileTool, tileTool *complexTileTool)
{
    int iX, iY ;
    float **floatPhaseTile ;
    double **doublePhaseTile ;
    complexFloat **complexFloatTile ;
    complexDouble **complexDoubleTile ;
    
    switch (complexTileTool->typeCode) {
        case _CPLX32_ID_:
            complexFloatTile = (complexFloat **)complexTileTool->tile ;
            
            switch (phaseTileTool->typeCode) {
                case _REAL32_ID_:
                    floatPhaseTile = (float **)phaseTileTool->tile ;
                    for (iY = 0; iY < phaseTileTool->yTileSize; iY++) {
                        for (iX = 0; iX < phaseTileTool->xTileSize; iX++) {
                            complexFloatTile[iY][iX].r = cosf(floatPhaseTile[iY][iX]) ;
                            complexFloatTile[iY][iX].i = sinf(floatPhaseTile[iY][iX]) ;
                            if (isnan(complexFloatTile[iY][iX].r) || isnan(complexFloatTile[iY][iX].i)) {
                                printf("aie !\n") ;
                            }
                        }
                    }
                    break ;
                    
                case _REAL64_ID_:
                    doublePhaseTile = (double **)phaseTileTool->tile ;
                    for (iY = 0; iY < phaseTileTool->yTileSize; iY++) {
                        for (iX = 0; iX < phaseTileTool->xTileSize; iX++) {
                            complexFloatTile[iY][iX].r = (float)cos(doublePhaseTile[iY][iX]) ;
                            complexFloatTile[iY][iX].i = (float)sin(doublePhaseTile[iY][iX]) ;
                        }
                    }
                    break ;

                default:
                    break;
            }
            break;
            
        case _CPLX64_ID_:
            complexDoubleTile = (complexDouble **)complexTileTool->tile ;
            
            switch (phaseTileTool->typeCode) {
                case _REAL32_ID_:
                    floatPhaseTile = (float **)phaseTileTool->tile ;
                    for (iY = 0; iY < phaseTileTool->yTileSize; iY++) {
                        for (iX = 0; iX < phaseTileTool->xTileSize; iX++) {
                            complexDoubleTile[iY][iX].r = cos((double)floatPhaseTile[iY][iX]) ;
                            complexDoubleTile[iY][iX].i = sin((double)floatPhaseTile[iY][iX]) ;
                        }
                    }
                    break ;
                    
                case _REAL64_ID_:
                    doublePhaseTile = (double **)phaseTileTool->tile ;
                    for (iY = 0; iY < phaseTileTool->yTileSize; iY++) {
                        for (iX = 0; iX < phaseTileTool->xTileSize; iX++) {
                            complexDoubleTile[iY][iX].r = (float)cos(doublePhaseTile[iY][iX]) ;
                            complexDoubleTile[iY][iX].i = (float)sin(doublePhaseTile[iY][iX]) ;
                        }
                    }
                    break ;
            }
        default:
            break;
    }
}

/* ---------------------------------------------- */
void convertComplexPhaseTileToFloat(tileTool *phaseTileTool, tileTool *complexTileTool)
{
    int iX, iY ;
    float **floatPhaseTile ;
    double **doublePhaseTile ;
    complexFloat **complexFloatTile ;
    complexDouble **complexDoubleTile ;

    switch (complexTileTool->typeCode) {
        case _CPLX32_ID_:
            complexFloatTile = (complexFloat **)complexTileTool->tile ;
            
            switch (phaseTileTool->typeCode) {
                case _REAL32_ID_:
                    floatPhaseTile = (float **)phaseTileTool->tile ;
                    for (iY = 0; iY < phaseTileTool->yTileSize; iY++) {
                        for (iX = 0; iX < phaseTileTool->xTileSize; iX++) {
                            floatPhaseTile[iY][iX] = phi(complexFloatTile[iY] + iX) ;
                        }
                    }
                    break;
                    
                case _REAL64_ID_:
                    doublePhaseTile = (double **)phaseTileTool->tile ;
                    for (iY = 0; iY < phaseTileTool->yTileSize; iY++) {
                        for (iX = 0; iX < phaseTileTool->xTileSize; iX++) {
                            doublePhaseTile[iY][iX] = (double)phi(complexFloatTile[iY] + iX) ;
                        }
                    }
                    break;

                default:
                    break;
            }
            break;
            
        case _CPLX64_ID_:
            complexDoubleTile = (complexDouble **)complexTileTool->tile ;
            
            switch (phaseTileTool->typeCode) {
                case _REAL32_ID_:
                    floatPhaseTile = (float **)phaseTileTool->tile ;
                    for (iY = 0; iY < phaseTileTool->yTileSize; iY++) {
                        for (iX = 0; iX < phaseTileTool->xTileSize; iX++) {
                            floatPhaseTile[iY][iX] = (float)phaseOfComplexDouble(complexDoubleTile[iY] + iX) ;
                        }
                    }
                    break;
                    
                case _REAL64_ID_:
                    doublePhaseTile = (double **)phaseTileTool->tile ;
                    for (iY = 0; iY < phaseTileTool->yTileSize; iY++) {
                        for (iX = 0; iX < phaseTileTool->xTileSize; iX++) {
                            doublePhaseTile[iY][iX] = phaseOfComplexDouble(complexDoubleTile[iY] + iX) ;
                        }
                    }
                    break;

                default:
                    break;
            }
            
        default:
            break;
    }
}

/* ---------------------------------------------- */
/* sigmaX and sigmaY are in fact the Full With at Half Maximum of the filtering Gaussian */
void initFilterTileTool(tileTool *filterTileTool, float sigmaX, float sigmaY)
{
    int	iX, iY ;
    float **floatFilterTile ;
    double **doubleFilterTile ;
    double	sx, sy, fx, fy, e ;
    

    sx = -(PI * PI * sigmaX * sigmaX / 4. / log(2.)) / (pow((double)filterTileTool->xTileSize, 2.)) ;
    sy = -(PI * PI * sigmaY * sigmaY / 4. / log(2.)) / (pow((double)filterTileTool->yTileSize, 2.)) ;
    fx = 1. ;
    fy = 1. ;
    
    switch (filterTileTool->typeSize) {
        case sizeof(float):
            floatFilterTile = (float**)filterTileTool->tile ;
            for(iY = 0; iY < filterTileTool->yTileSize / 2 ; iY++) {
                e = fy * exp(sy * pow((double)iY, 2.)) ;
                for(iX = 0; iX < filterTileTool->xTileSize / 2; iX++) {
                    floatFilterTile[iY][iX] = (float)(e * fx * exp(sx * pow((double)iX, 2.))) ;
                }
                for(; iX < filterTileTool->xTileSize; iX++) {
                    floatFilterTile[iY][iX] = (float)(e * fx * exp(sx * pow((double)iX - filterTileTool->xTileSize, 2.))) ;
                }
            }

            for(; iY < filterTileTool->yTileSize ; iY++) {
                e = fy * exp(sy * pow((double)iY - filterTileTool->yTileSize, 2.)) ;
                for(iX = 0; iX < filterTileTool->xTileSize / 2; iX++) {
                    floatFilterTile[iY][iX] = (float)(e * fx * exp(sx * pow((double)iX, 2.))) ;
                }
                for(; iX < filterTileTool->xTileSize; iX++) {
                    floatFilterTile[iY][iX] = (float)(e * fx * exp(sx * pow((double)iX - filterTileTool->xTileSize, 2.))) ;
                }
            }

            break ;
            
        case sizeof(double):
            doubleFilterTile = (double**)filterTileTool->tile ;
            for(iY = 0; iY < filterTileTool->yTileSize / 2 ; iY++) {
                e = fy * exp(sy * pow((double)iY, 2.)) ;
                for(iX = 0; iX < filterTileTool->xTileSize / 2; iX++) {
                    doubleFilterTile[iY][iX] = e * fx * exp(sx * pow((double)iX, 2.)) ;
                }
                for(; iX < filterTileTool->xTileSize; iX++) {
                    doubleFilterTile[iY][iX] = e * fx * exp(sx * pow((double)iX - filterTileTool->xTileSize, 2.)) ;
                }
            }
            
            for(; iY < filterTileTool->yTileSize ; iY++) {
                e = fy * exp(sy * pow((double)iY - filterTileTool->yTileSize, 2.)) ;
                for(iX = 0; iX < filterTileTool->xTileSize / 2; iX++) {
                    doubleFilterTile[iY][iX] = e * fx * exp(sx * pow((double)iX, 2.)) ;
                }
                for(; iX < filterTileTool->xTileSize; iX++) {
                    doubleFilterTile[iY][iX] = e * fx * exp(sx * pow((double)iX - filterTileTool->xTileSize, 2.)) ;
                }
            }
            
            break ;
            
       default:
            ase("Filter tile tool initialization error: wrong type size. Must be float or double size") ;
            break ;
    }
}


/* ---------------------------------------------- */
/* filterTileTool must have a type compatible with tile to filter */

void tileFiltering(tileTool *aTileTool, tileTool *filterTileTool)
{
    int	iX, iY  ;
    float **floatFilterTile, **floatTile ;
    double **doubleFilterTile, **doubleTile ;
    complexFloat **complexFloatTile ;
    complexDouble **complexDoubleTile ;
    
    switch (aTileTool->typeCode) {
        case _REAL32_ID_:
            floatFilterTile = (float **)filterTileTool->tile ;
            floatTile = (float **)aTileTool->tile ;
            
            for(iY = 0; iY < aTileTool->yTileSize; iY++) {
                for(iX = 0; iX < aTileTool->xTileSize; iX++) {
                    floatTile[iY][iX] *= floatFilterTile[iY][iX] ;
                }
            }
            break;
            
        case _REAL64_ID_:
            doubleFilterTile = (double **)filterTileTool->tile ;
            doubleTile = (double **)aTileTool->tile ;
            
            for(iY = 0; iY < aTileTool->yTileSize; iY++) {
                for(iX = 0; iX < aTileTool->xTileSize; iX++) {
                    doubleTile[iY][iX] *= doubleFilterTile[iY][iX] ;
                }
            }
            break;
            
        case _CPLX32_ID_:
            floatFilterTile = (float **)filterTileTool->tile ;
            complexFloatTile = (complexFloat **)aTileTool->tile ;
            
            for(iY = 0; iY < aTileTool->yTileSize; iY++) {
                for(iX = 0; iX < aTileTool->xTileSize; iX++) {
                    complexFloatTile[iY][iX].r *= floatFilterTile[iY][iX] ;
                    complexFloatTile[iY][iX].i *= floatFilterTile[iY][iX] ;
                }
            }
            break;
            
        case _CPLX64_ID_:
            doubleFilterTile = (double **)filterTileTool->tile ;
            complexDoubleTile = (complexDouble **)aTileTool->tile ;
            
            for(iY = 0; iY < aTileTool->yTileSize; iY++) {
                for(iX = 0; iX < aTileTool->xTileSize; iX++) {
                    complexDoubleTile[iY][iX].r *= doubleFilterTile[iY][iX] ;
                    complexDoubleTile[iY][iX].i *= doubleFilterTile[iY][iX] ;
                }
            }
            break;
            
        default:
            break;
    }
}


/* ---------------------------------------------- */
/* Goldstein filtering of tile. Sent tile must already be in the Fourier space */
void adaptativeFilteringOfTile(tileTool *complexPhaseTileTool, tileTool *smoothedSpectrumTileTool, float alpha)
{
    int	iX, iY  ;
    float power ;
    complexFloat **complexPhaseTile, **smoothedSpectrum ;
    
    complexPhaseTile = (complexFloat **)complexPhaseTileTool->tile ;
    smoothedSpectrum = (complexFloat **)smoothedSpectrumTileTool->tile ;
    
    power = 1. ;
    for(iY = 0; iY < complexPhaseTileTool->yTileSize; iY++) {
        for(iX = 0; iX < complexPhaseTileTool->xTileSize; iX++) {
            power = powf(sqrtf(powf(smoothedSpectrum[iY][iX].r, 2.0) + powf(smoothedSpectrum[iY][iX].i, 2.0)), alpha) ;
            complexPhaseTile[iY][iX].r *= power ;
            complexPhaseTile[iY][iX].i *= power ;
       }
    }
}


