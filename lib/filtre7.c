/*
 *  filtre6.c
 *
 *  Created by Dominique Derauw a long time ago.
 *  Copyright (c) Dominique Derauw. All rights reserved.
 *
 */

/* Ce programme filtre un interférogramme en retournant à l'expression complexe de celui-ci
et en filtrant par une gaussienne partie réelle et imaginaire indépendement l'une de l'autre. Après filtrage, on re-génère un interférogramme.
Le calcul se fait par bloc et un fichier intermédiaire de taille double au résultat attendu est généré.
*/
#define _ADAPTATIVE_FILTERING_
#include "interferogramFiltering.h"

/* main programm start */
void interferogramFiltering(keyValue *parameters)
{
	/* local variables declaration */
	int yDim, xDim ;
    int unsigned xInputIndex, yInputIndex, flag ;
    int unsigned xTileSize, yTileSize, xTileBorderSize, yTileBorderSize, xTileIndex, yTileIndex ;
	float sigmaY, sigmaX, alpha = 0.0, smoothingFactor ;
    float **floatPhaseInput, **filterTile ;
    double powerArg ;
    complexFloat **complexPhaseInput, **complexPhaseOutput ;
    complexFloat **complexPhaseTile, **smoothedPhaseTile ;
	rawFileDescriptor *inputFile = NULL, *outputFile = NULL, *coherenceFile ;
    tileTool *dataInputTileTool, *complexDataInputTileTool, *complexDataOutputTileTool ;
    tileTool *coherenceInputTileTool, *coherenceTileTool ;
    tileTool *complexPhaseTileTool, *filterTileTool = NULL ;
    tileTool *baseFilterTileTool = NULL ;
    tileTool *smoothedSpectrumOfComplexPhase ;
    StructFFT *xFFT, *yFFT ;

/*----------------------------------  INITIALISATION  -----------------------------------*/

    /* Open input and output files */
    inputFile = openRawFileForReadingAtPath(getStringValForKeyIn(parameters, "Input")) ;
    outputFile = openRawFileForWritingAtPath(getStringValForKeyIn(parameters, "Output")) ;
    coherenceFile = openRawFileForReadingAtPath(getStringValForKeyIn(parameters, "Coherence")) ;
    xDim = getIntValForKeyIn(parameters, "xDim") ;
    yDim = getIntValForKeyIn(parameters, "yDim") ;
    setRawFileDimensions(inputFile, xDim, yDim) ;
    setRawFileDimensions(outputFile, xDim, yDim) ;
    setRawFileDimensions(coherenceFile, xDim, yDim) ;
    sigmaX = getFloatValForKeyIn(parameters, "Sigma X") ;
    sigmaY = getFloatValForKeyIn(parameters, "Sigma Y") ;
    smoothingFactor = getFloatValForKeyIn(parameters, "Smoothing") ;
    powerArg = getDoubleValForKeyIn(parameters, "Power argument") ;
    flag = (int unsigned)getIntValForKeyIn(parameters, "Flags") ;
    setRawFileDimensions(inputFile, xDim, yDim) ;
    setRawFileDimensions(outputFile, xDim, yDim) ;
    setRawFileDimensions(coherenceFile, xDim, yDim) ;


    /* Initialize tile tools parameters */
    if (smoothingFactor) {
        xTileSize = getIntValForKeyIn(parameters, "squareWindowSize") ;
        if (xTileSize != XTILESIZE) {
            xTileSize = yTileSize = ceil2ofIntVal(xTileSize) ;
            xTileBorderSize = yTileBorderSize = xTileSize / 2 - 1 ;
            printf("\t-->\tFiltering window size set to %d\n", xTileSize) ;
        }
        else {
            xTileSize = XTILESIZE ;
            xTileBorderSize = XTILEBORDERSIZE ;
            yTileSize = YTILESIZE ;
            yTileBorderSize = YTILEBORDERSIZE ;
        }
    }
    else {
        if(sigmaX * sigmaY == 0)
            return ;
        flag |= _COMBINED_FILTERINGS_ ;
        xTileSize = 64 ;
        xTileBorderSize = 16 ;
        yTileSize = 64 ;
        yTileBorderSize = 16 ;
    }

    /* Initialize input tile tool for sequential reading */
    dataInputTileTool = allocTileToolForFilesOfType(inputFile, outputFile, "float") ;
    floatPhaseInput = (float **)initTileToolWithSizes(dataInputTileTool, xDim, yDim, 0, 0) ;

    /* Initialize coherence tile tool for sequential reading */
    coherenceInputTileTool = allocTileToolForFilesOfType(coherenceFile, NULL, "float") ;
    initTileToolWithSizes(coherenceInputTileTool, xDim, yDim, 0, 0) ;

    /* Initialize complex phase input tile tool */
    complexDataInputTileTool = allocTileToolForFilesOfType(inputFile, outputFile, "complexFloat") ;
    complexPhaseInput = (complexFloat **)initTileToolWithSizes(complexDataInputTileTool, xDim, yDim, 0, 0) ;

    /* Initialize complex phase output tile tool */
    complexDataOutputTileTool = allocTileToolForFilesOfType(inputFile, outputFile, "complexFloat") ;
    complexPhaseOutput = (complexFloat **)initTileToolWithSizes(complexDataOutputTileTool, xDim, yDim, 0, 0) ;

    /* Initialize complex phase subwindow tile tool */
    complexPhaseTileTool = allocTileToolForDataOfType((void **)complexPhaseInput, (void **)complexPhaseOutput, xDim, yDim, "complexFloat") ;
    complexPhaseTile = (complexFloat **)initTileToolWithSizes(complexPhaseTileTool, xTileSize, yTileSize, xTileBorderSize, yTileBorderSize) ;

    /* Initialize smoothed complex phase subwindow tile tool */
    smoothedSpectrumOfComplexPhase = allocTileToolForDataOfType((void **)complexPhaseInput, NULL, xDim, yDim, "complexFloat") ;
    smoothedPhaseTile = (complexFloat **)initTileToolWithSizes(smoothedSpectrumOfComplexPhase, xTileSize, yTileSize, xTileBorderSize, yTileBorderSize) ;

    /* Initialize coherence subwindow tile tool */
    coherenceTileTool = allocTileToolForDataOfType(coherenceInputTileTool->tile, NULL, xDim, yDim, "float") ;
    initTileToolWithSizes(coherenceTileTool, xTileSize, yTileSize, xTileBorderSize, yTileBorderSize) ;

    /* Initialize base filter tile tool */
    if ((sigmaX && sigmaY) && (flag & _COMBINED_FILTERINGS_)) {
        baseFilterTileTool = allocTileToolForFilesOfType(inputFile, outputFile, "float") ;
        initTileToolWithSizes(baseFilterTileTool, xTileSize, yTileSize, xTileBorderSize, yTileBorderSize) ;
        initFilterTileTool(baseFilterTileTool, sigmaX, sigmaY) ;
    }
    /* Initialize smoothing filter for averaging power spectrum */
    if (smoothingFactor) {
        filterTileTool = allocTileToolForFilesOfType(inputFile, outputFile, "float") ;
        filterTile = (float **)initTileToolWithSizes(filterTileTool, xTileSize, yTileSize, xTileBorderSize, yTileBorderSize) ;
        initFilterTileTool(filterTileTool, smoothingFactor, smoothingFactor) ;
    }

    /* Initialize FFT's */
    xFFT = initFFT(xTileSize, yTileSize, ALONGX) ;
    yFFT = initFFT(xTileSize, yTileSize, ALONGY) ;

    /* Blahblah... */
    if ((flag & _BLAH_BLAH_)) {
        if (smoothingFactor) {
            printf("\nAdaptative interferogram filtering :\n") ;
            printf("****************************************\n\n") ;
            printf("Input interferogram : %s\n", inputFile->accessPath) ;
            printf("Output interferogram : %s\n", outputFile->accessPath) ;
            printf("Power spectrum smoothing factor =  %f\n", smoothingFactor ) ;
            if (flag & _COMBINED_FILTERINGS_) {
                printf("Adaptative filter will be applied to classically filtered interferogram\n") ;
                printf("Non-adaptative interferogram filtering characteristics\n") ;
                printf("Range filter Full Width at Half Maximum = %f\n", sigmaX) ;
                printf("Azimuth filter Full Width at Half Maximum = %f\n", sigmaY) ;
            }
        }
        else {
            printf("\nNon-adaptative interferogram filtering\n") ;
            printf("**************************************\n") ;
            printf("Range filter Full Width at Half Maximum = %f\n", sigmaX) ;
            printf("Azimuth filter Full Width at Half Maximum = %f\n", sigmaY) ;
        }
    }

    /* Start filtering tile by tile */
    powerArg = (powerArg)? powerArg : 1. ;
    for(yInputIndex = 0; yInputIndex < dataInputTileTool->yTileNumber; yInputIndex++) {
        for(xInputIndex = 0; xInputIndex < dataInputTileTool->xTileNumber; xInputIndex++) {
            /* Sequential reading of input phase */
            readTileAtIndexes(dataInputTileTool, xInputIndex, yInputIndex) ;
            /* Conversion of input phase into complex */
            convertFloatPhaseTileToComplex(dataInputTileTool, complexDataInputTileTool) ;

            /* Sequential reading of coherence */
            readTileAtIndexes(coherenceInputTileTool, xInputIndex, yInputIndex) ;

            for (yTileIndex = 0; yTileIndex < complexPhaseTileTool->yTileNumber; yTileIndex ++) {
                for (xTileIndex = 0; xTileIndex < complexPhaseTileTool->xTileNumber; xTileIndex++) {
                    /* Read required data */
                    copyTileAtIndexes(complexPhaseTileTool, xTileIndex, yTileIndex) ;
                    copyTileAtIndexes(smoothedSpectrumOfComplexPhase, xTileIndex, yTileIndex) ;
                    copyTileAtIndexes(coherenceTileTool, xTileIndex, yTileIndex) ;

                    /* Generating smoothed spectrum of complex phase tile */
                    if (smoothingFactor) {
                        /* Compute Goldstein alpha factor */
                        alpha = 1. - pow(localAverageOfFloatTile(coherenceTileTool), powerArg) ;
                        if (alpha > Chouillat) {
                            tileFiltering(smoothedSpectrumOfComplexPhase, filterTileTool) ;
                            fftX(FORWARD_FFT, smoothedSpectrumOfComplexPhase->tile[0], xFFT) ;
                            fftY(FORWARD_FFT, smoothedSpectrumOfComplexPhase->tile[0], yFFT) ;
                        }
                        else {
                            alpha = 0 ;
                        }
                    }

                    fftX(FORWARD_FFT, complexPhaseTile[0], xFFT) ;
                    fftY(FORWARD_FFT, complexPhaseTile[0], yFFT) ;

                    if (alpha)
                        adaptativeFilteringOfTile(complexPhaseTileTool, smoothedSpectrumOfComplexPhase, alpha) ;

                    if ((flag & _COMBINED_FILTERINGS_))
                        tileFiltering(complexPhaseTileTool, baseFilterTileTool) ;

                    fftX(INVERSE_FFT, complexPhaseTile[0], xFFT) ;
                    fftY(INVERSE_FFT, complexPhaseTile[0], yFFT) ;

                    saveTile(complexPhaseTileTool) ;
                }
            }

            convertComplexPhaseTileToFloat(dataInputTileTool, complexDataOutputTileTool) ;
            writeTile(dataInputTileTool) ;
        }
    }

    freeTileTool(dataInputTileTool) ;
    freeTileTool(coherenceInputTileTool) ;
    freeTileTool(complexDataInputTileTool) ;
    freeTileTool(complexDataOutputTileTool) ;
    freeTileTool(complexPhaseTileTool) ;
    freeTileTool(smoothedSpectrumOfComplexPhase) ;
    freeTileTool(coherenceTileTool) ;
    freeTileTool(baseFilterTileTool) ;
    if (sigmaX && sigmaY) {
        freeTileTool(filterTileTool) ;
    }
    freeFFT(xFFT) ;
    freeFFT(yFFT) ;
}
