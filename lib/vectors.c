/*
 *  vectors.c
 *  
 *
 *  Created by dd on Mon Dec 03 2001.
 *  Copyright (c) 2001 Dominique Derauw All rights reserved.
 *
 */

#include "vectors.h"

/* ----------------- Allocation de mémoire pour un vecteur d'entiers (char) ----------------- */

unsigned char	*vectorUChar(int lowerIndex, int higherIndex)
{
	unsigned char	*ptr ;
	if((ptr = (unsigned char *)calloc((unsigned) (higherIndex - lowerIndex + 1), sizeof(unsigned char))) == NULL) {
            perror("Erreur d'allocation d'un vecteur d'unsigned char") ;
            exit(-1) ;
        }
	ptr -= lowerIndex ;
	return(ptr) ;
}

/* ----------------- désallocation de mémoire pour un vecteur d'unsigned char (char) ----------------- */

void	freeVectorUChar(unsigned char *v, int lowerIndex)
{
	free((void *)(v + lowerIndex)) ;
}

/* ----------------- Allocation de mémoire pour un vecteur d'entiers (short) ----------------- */

short *vectorShort(int lowerIndex, int higherIndex)
{
	short	*ptr ;
	if((ptr = (short *)calloc((unsigned) (higherIndex - lowerIndex + 1), sizeof(short))) == NULL) {
		perror("Erreur d'allocation d'un vecteur d'entiers short") ;
		exit(-1) ;
	}
	ptr -= lowerIndex ;
	return(ptr) ;
}

/* ----------------- désallocation de mémoire pour un vecteur d'entiers (short) ----------------- */

void	freeVectorShort(short *v, int lowerIndex)
{
	free((void *)(v + lowerIndex)) ;
}

/* ----------------- Allocation de mémoire pour un vecteur d'entiers (int) ----------------- */

int *vectorInt(int lowerIndex, int higherIndex)
{
	int	*ptr ;
	if((ptr = (int *)calloc((unsigned) (higherIndex - lowerIndex + 1), sizeof(int))) == NULL) {
		perror("Erreur d'allocation d'un vecteur d'entiers") ;
		exit(-1) ;
	}
	ptr -= lowerIndex ;
	return(ptr) ;
}

/* ----------------- désallocation de mémoire pour un vecteur d'entiers (int) ----------------- */

void	freeVectorInt(int *v, int lowerIndex)
{
	free((void *)(v + lowerIndex)) ;
}

/* ----------------- Allocation de mémoire pour un vecteur d'entiers (intunsigned) ----------------- */

int unsigned *vectorIntUnsigned(int lowerIndex, int higherIndex)
{
	int unsigned	*ptr ;
	if((ptr = (int unsigned *)calloc((unsigned) (higherIndex - lowerIndex + 1), sizeof(int unsigned))) == NULL) {
		perror("Erreur d'allocation d'un vecteur d'entiers") ;
		exit(-1) ;
	}
	ptr -= lowerIndex ;
	return(ptr) ;
}

/* ----------------- désallocation de mémoire pour un vecteur d'entiers (int unsigned) ----------------- */

void	freeVectorIntUnsigned(int unsigned *v, int lowerIndex)
{
	free((void *)(v + lowerIndex)) ;
}

/* ----------------- Allocation de mémoire pour un vecteur d'entiers (long) ----------------- */

long	*vectorLong(int lowerIndex, int higherIndex)
{
	long	*ptr ;
	if((ptr = (long *)calloc((unsigned) (higherIndex - lowerIndex + 1), sizeof(long))) == NULL) {
        perror("Erreur d'allocation d'un vecteur d'entiers") ;
        exit(-1) ;
    }
	ptr -= lowerIndex ;
	return(ptr) ;
}

/* ----------------- désallocation de mémoire pour un vecteur d'entiers (long) ----------------- */
void	freeVectorLong(long *v, int lowerIndex)
{
	free((void *)(v + lowerIndex)) ;
}

/* ----------------- Allocation de mémoire pour un vecteur d'entiers (long unsigned) ----------------- */

long unsigned *vectorLongUnsigned(int lowerIndex, int higherIndex)
{
	long unsigned	*ptr ;
	if((ptr = (long unsigned *)calloc((unsigned) (higherIndex - lowerIndex + 1), sizeof(long unsigned))) == NULL) {
        perror("Erreur d'allocation d'un vecteur d'entiers") ;
        exit(-1) ;
    }
	ptr -= lowerIndex ;
	return(ptr) ;
}

/* ----------------- désallocation de mémoire pour un vecteur d'entiers (long unsigned) ----------------- */
void freeVectorLongUnsigned(long unsigned *v, int lowerIndex)
{
	free((void *)(v + lowerIndex)) ;
}

/* ----------------- Allocation de mémoire pour un vecteur de réels float ----------------- */

float	*vectorFloat(int lowerIndex, int higherIndex)
{
	float	*ptr ;
	if((ptr = (float *)calloc((unsigned) (higherIndex - lowerIndex + 1), sizeof(float))) == NULL) {
            perror("Erreur d'allocation d'un vecteur de float") ;
            exit(-1) ;
        }
	ptr -= lowerIndex ;
	return(ptr) ;
}

/* ----------------- désallocation de mémoire pour un vecteur de réels float ----------------- */

void	freeVectorFloat(float *v, int lowerIndex)
{
	free((void *)(v + lowerIndex)) ;
}

/* ----------------- Allocation de mémoire pour un vecteur de réels double ----------------- */

double	*vectorDouble(int lowerIndex, int higherIndex)
{
	double	*ptr ;
	if((ptr = (double *)calloc((unsigned) (higherIndex - lowerIndex + 1), sizeof(double))) == NULL) {
            perror("Erreur d'allocation d'un vecteur de double") ;
            exit(-1) ;
        }
	ptr -= lowerIndex ;
	return(ptr) ;
}

/* ----------------- désallocation de mémoire pour un vecteur de réels double ----------------- */

void	freeVectorDouble(double *v, int lowerIndex)
{
	free((void *)(v + lowerIndex)) ;
}

/* ----------------- Allocation de mémoire pour un vecteur de complex float ----------------- */

complexFloat	*vectorComplexFloat(int lowerIndex, int higherIndex)
{
	complexFloat	*ptr ;
	if((ptr = (complexFloat *)calloc((unsigned) (higherIndex - lowerIndex + 1), sizeof(complexFloat))) == NULL) {
            perror("Erreur d'allocation d'un vecteur de complex float") ;
            exit(-1) ;
        }
	ptr -= lowerIndex ;
	return(ptr) ;
}

/* ----------------- désallocation de mémoire pour un vecteur de complex float ----------------- */

void	freeVectorComplexFloat(complexFloat *v, int lowerIndex)
{
	free((void *)(v + lowerIndex)) ;
}

/* ----------------- Allocation de mémoire pour un vecteur de complex char ----------------- */

complexChar	*vectorComplexChar(int lowerIndex, int higherIndex)
{
	complexChar	*ptr ;
	if((ptr = (complexChar *)calloc((unsigned) (higherIndex - lowerIndex + 1), sizeof(complexChar))) == NULL) {
            perror("Erreur d'allocation d'un vecteur de complex float") ;
            exit(-1) ;
        }
	ptr -= lowerIndex ;
	return(ptr) ;
}

/* ----------------- désallocation de mémoire pour un vecteur de complex char ----------------- */

void	freeVectorComplexChar(complexChar *v, int lowerIndex)
{
	free((void *)(v + lowerIndex)) ;
}

/* ----------------- Allocation de mémoire pour un vecteur de complex char ----------------- */

complexShort	*vectorComplexShort(int lowerIndex, int higherIndex)
{
	complexShort	*ptr ;
	if((ptr = (complexShort *)calloc((unsigned) (higherIndex - lowerIndex + 1), sizeof(complexShort))) == NULL) {
            perror("Erreur d'allocation d'un vecteur de complex float") ;
            exit(-1) ;
        }
	ptr -= lowerIndex ;
	return(ptr) ;
}

/* ----------------- désallocation de mémoire pour un vecteur de complex char ----------------- */

void	freeVectorComplexShort(complexShort *v, int lowerIndex)
{
	free((void *)(v + lowerIndex)) ;
}

/* ----------------- Allocation de mémoire pour un vecteur de complex double ----------------- */

complexDouble	*vectorComplexDouble(int lowerIndex, int higherIndex)
{
    complexDouble	*ptr ;
    if((ptr = (complexDouble *)calloc((unsigned) (higherIndex - lowerIndex + 1), sizeof(complexDouble))) == NULL) {
        perror("Erreur d'allocation d'un vecteur de complex double") ;
        exit(-1) ;
    }
    ptr -= lowerIndex ;
    return(ptr) ;
}

/* ----------------- désallocation de mémoire pour un vecteur de complex double ----------------- */

void	freeVectorComplexDouble(complexDouble *v, int lowerIndex)
{
    free((void *)(v + lowerIndex)) ;
}




/* ----------------- Allocation de mémoire pour un vecteur de complex double ----------------- */

void	*vectorOfType(int lowerIndex, int higherIndex, size_t type)
{
    void *ptr ;
    
    if((ptr = calloc((unsigned) (higherIndex - lowerIndex + 1), type)) == NULL) {
        perror("Erreur d'allocation d'un vecteur de complex double") ;
        exit(-1) ;
    }
    ptr -= lowerIndex * type ;
    return(ptr) ;
}

/* ----------------- désallocation de mémoire pour un vecteur de complex double ----------------- */

void	freeVectorOfType(void *v, int lowerIndex, size_t type)
{
    free((void *)(v + lowerIndex * type)) ;
}




/* Allocation of floatVector structure */
floatVector *allocFloatVectorWithIndexes(int lowerIndex, int upperIndex)
{
	floatVector *aFloatVector ;
	
    if((aFloatVector = malloc(sizeof(floatVector))) == NULL) {
	   perror("Failed to allocate a floatVector") ;
        exit(-1) ;
    }
	
	aFloatVector->v = vectorFloat(lowerIndex, upperIndex) ;
	aFloatVector->lowerIndex = lowerIndex ;
	aFloatVector->upperIndex = upperIndex ;
	
	return(aFloatVector) ;
}

void freeFloatVector(floatVector *aFloatVector)
{
	freeVectorFloat(aFloatVector->v, aFloatVector->lowerIndex) ;
	free(aFloatVector) ;
}
