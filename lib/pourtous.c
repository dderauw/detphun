/*
 *  Created by Dominique Derauw a long time ago and constantly updated.
 *  Copyright (c) 1993 Dominique Derauw. All rights reserved.
 *
 */


#include "pourtous.h"

char	ertex[MAXERRORMESSAGELENGTH] ;
char	accessPath[MAXPATHLENGTH] ;
char	aComment[MAXCOMMENTLENGTH] ;
char	aCommand[_MAX_COMMAND_LENGTH_] ;
float	floatNaN ;
float	floatInf ;

/* ________________ Generation du floatNaN ________________ */
float genFloatNaN() {
    union fl		NaN ;
    
    errno = 0 ;
    if(isArchBigEndian()) {
        NaN.b[0] = 127 ;
        NaN.b[1] = 192 ;
        NaN.b[2] = 0 ;
        NaN.b[3] = 0 ;
    }
    else {
        NaN.b[3] = 127 ;
        NaN.b[2] = 192 ;
        NaN.b[1] = 0 ;
        NaN.b[0] = 0 ;
    }
    
    floatNaN = NaN.fl ;
    return (floatNaN) ;
}

/* ________________ Float infinite value generation ________________ */
float genFloatInf() {
    union fl		Infini ;
    
    errno = 0 ;
    if(isArchBigEndian()) {
        Infini.b[0] = 127 ;
        Infini.b[1] = 128 ;
        Infini.b[2] = 0 ;
        Infini.b[3] = 0 ;
    }
    else {
        Infini.b[3] = 127 ;
        Infini.b[2] = 128 ;
        Infini.b[1] = 0 ;
        Infini.b[0] = 0 ;
    }
    
    floatInf = Infini.fl ;
    return (floatInf) ;
}

/* ________________ Arret sur erreur ________________ */

void ase(char *texte)
{
	perror(texte) ;
	exit(-1) ;
}


/* ________________ Phase gap between two points ________________ */
double unwrappedPhaseGap(float endPoint, float startPoint)
{
    double	d ;
    
    d = (double)endPoint - (double)startPoint ;
    if(d < -PI) d += TWOPI ;
    else if(d > PI) d -= TWOPI ;
    return(d) ;
}



/* ________________ Phase unwrapping between two points ________________ */
double unwrappPhaseFromPointToPoint(float endPoint, float startPoint)
{
    double	d ;
    
    d = startPoint + unwrappedPhaseGap(endPoint, startPoint) ;
    
    return(d) ;
}

/* ________________ Deroulement de la phase ________________ */

double dephi(float *phi1, float *phi2)
{
	double	d ;
	
	d = (double)*phi1 - (double)*phi2 ;
	if(d < -PI) d+=(double)TWOPI ;
	else if(d >  PI) d-=(double)TWOPI ;
	return d ;
}

/* _______________ Calcul de l'argument d'un nombre complexe _______________ */

double	phi(complexFloat *ptr)
{
	double	a, b, arg ;
	
	a = (double)ptr->r ;
	b = (double)ptr->i ;
	if(a!=0)
	{
		arg = atan(b/a) ;
		if(a<0)
		{
			if(b>0)	arg += (double)PI ;
			else	arg -= (double)PI ;
		}
	}
	else
	{
		if(b>0) arg =  (double)HALFPI ;
		else
			if(b<0) arg = -(double)HALFPI ;
			else arg = 0. ;
	}
    
	return arg ;
}


/* _______________ Calcul de l'argument d'un nombre complexe _______________ */

double	phaseOfComplexDouble(complexDouble *ptr)
{
	double	a, b, arg ;
	
	a = ptr->r ;
	b = ptr->i ;
	if(a!=0)
	{
		arg = atan(b/a) ;
		if(a<0)
		{
			if(b>0)	arg += (double)PI ;
			else	arg -= (double)PI ;
		}
	}
	else
	{
		if(b>0) arg =  (double)HALFPI ;
		else
			if(b<0) arg = -(double)HALFPI ;
			else arg = 0. ;
	}
    
	return arg ;
}


/* ________________ Temps ecoule ________________ */

void duration( long startTime, long endTime)
{
	long	timeDelay, hours, minutes, seconds ;
	timeDelay = endTime - startTime ;
	timeDelay = timeDelay % 86400L ;
	hours = timeDelay / 3600L ;
	timeDelay = timeDelay % 3600L ;
	minutes = timeDelay / 60L ;
	seconds = timeDelay % 60L ;
        printf("\nEnd time = %s\n", ctime(&endTime)) ;
	printf("Duration = %2ld:%2ld:%2ld\n", hours, minutes, seconds) ;
}

/* ________________ Lecture des arguments en lignes de commande ________________ */
char	argument(char **nextArgument, int argc, const char **argv, const char *searchedArgument)
{
    char	isSearchedArgumentPresent = 0 ;
    int		i ;
    
    i = 0 ;
	*nextArgument = NULL ;
    while(i < argc && strncmp(argv[i], searchedArgument, strlen(searchedArgument))) i++ ;
    if(i < argc) {
        isSearchedArgumentPresent = i ;
        if(i < argc - 1) *nextArgument = (char *)argv[++i] ;
    }
    return isSearchedArgumentPresent ;
}

/* ________________ Return non zero value if keyword is present in command line ________________ */
char	isArgumentPresent(int argc, const char **argv, const char *searchedArgument)
{
    char	isSearchedArgumentPresent = 0 ;
    int		i ;
    
    i = 0 ;
    while(i < argc && strncmp(argv[i], searchedArgument, strlen(searchedArgument))) {
        i++ ;
    }
    if(i < argc) {
        isSearchedArgumentPresent = i ;
    }
    return isSearchedArgumentPresent ;
}

/* ________________ Return non zero value if flag is present in command line ________________ */
/* A flag being a single letter within a set preceeded by a single hyphen */
char	isFlagPresent(int argc, const char **argv, const char *searchedFlag)
{
    char	isSearchedArgumentPresent = 0 ;
    int		i, k ;
    long    j, argLen ;
    
    i = 1 ;
    k = (!strncmp(searchedFlag, "-", 1)) ;
    while(i < argc) {
        argLen = strlen(argv[i]) ;
        if (argLen > 1) {
            if(!strncmp(argv[i], "-", 1) && strncmp(argv[i], "--", 2)) {
                j = 0 ;
                while (j < argLen) {
                    if (argv[i][j] == searchedFlag[k]) {
                        if (j < argLen - 1 && argv[i][j + 1] == '=') {
                            isSearchedArgumentPresent = 0 ;
                        }
                        else
                            isSearchedArgumentPresent = 1 ;
                    }
                    j++ ;
                }
            }
        }
        i++ ;
    }

    return isSearchedArgumentPresent ;
}

/* Return next argument after requested argument in command line */
char *getNextArgument(int argc, const char **argv, const char *searchedArgument)
{
	int i ;
	
	i = isArgumentPresent(argc, argv, searchedArgument) ;
	if(i && i < argc - 1) 
		return((char *)argv[++i]) ;
	else 
		return((char *)NULL) ;
}



/* Division modulaire d'un reel x par un reel y */
/* Le resultat est stocke dans une structure donnant le quotient (entier) et le reste (reel) */
/* le quotient donne se trouve dans l'interval [0 ; x] */
struct divrf divrf(float x, float y) 
{
    int		signe ;
    struct divrf divMod ;
    
    signe  = (x < 0)? -1 : 1 ;
    signe *= (y < 0)? -1 : 1 ;
    divMod.q = signe * abs((int)floor(x / y)) ;
    divMod.r = x - (float)divMod.q * y ;
    return divMod ;
}


/* Division modulaire d'un r�el double x par un r�el double y */
/* Le r�sultat est stock� dans une structure donnant le quotient (entier) et le reste (r�el) */
/* le quotient donn� se trouve dans l'interval [0 ; x[ */
struct divr divr(double x, double y) 
{
    int		signe ;
    struct divr divMod ;
    
    signe  = (x < 0)? -1 : 1 ;
    signe *= (y < 0)? -1 : 1 ;
    divMod.q = signe * abs((int)floor(x / y)) ;
    divMod.r = x - (float)divMod.q * y ;
    return divMod ;
}


void floatByteSwap(float *aFloat)
{
	char	aByte ;
	union fl *swappedFloat ;
	
	swappedFloat = (union fl *)aFloat ;
	
	aByte = swappedFloat->b[0] ;
	swappedFloat->b[0] = swappedFloat->b[3] ;
	swappedFloat->b[3] = aByte ;
	
	aByte = swappedFloat->b[1] ;
	swappedFloat->b[1] = swappedFloat->b[2] ;
	swappedFloat->b[2] = aByte ;	
}

double aRandomNumberBetweenValues(double minVal, double maxVal)
{
    return((double)random() / (pow(2., 31.) - 1) * (maxVal - minVal) + minVal) ;
}

float aRandomFloatBetweenValues(float minVal, float maxVal)
{
    return((float)random() / (powf(2., 31.) - 1) * (maxVal - minVal) + minVal) ;
}


char isArchBigEndian(void)
{
	char aChar ;
	int anInt ;
	
	anInt = 1 ;
	aChar = *(char*)&anInt ;	
	
	return(!aChar) ;
}

char setNumberOfAllowedOpenendFilesToMax()
{
    char isItOk = 1 ;
    struct rlimit limit ;
    
    if(!getrlimit(RLIMIT_NOFILE, &limit)) {
        //		printf("rlim_cur = %llu\t rlim_max = %llu\n", limit.rlim_cur, limit.rlim_max) ;
        limit.rlim_cur = MAX_NUMBER_OF_OPENED_FILES ;
        
        if(setrlimit(RLIMIT_NOFILE, &limit)) {
            printf("Failed to change the limit number of allowed opened files") ;
            isItOk = 0 ;
        }
        if(getrlimit(RLIMIT_NOFILE, &limit))
            printf("Failed to read the limit number of allowed opened files") ;
    }

    return(isItOk) ;
}

/* Simple function to compare float values, including NaN */
char areFloatValsEquals(float val1, float val2)
{
    if (isnan(val1)) {
        return(isnan(val2)) ;
    }
    if (isnan(val2)) {
        return(isnan(val1)) ;
    }
    
    return(val1 == val2) ;
}



/* Return the month index between 0 and 11 */
int monthIndex(char *aMonth)
{
    int index = -1 ;
    
    if (!strncmp(aMonth, "JAN", 3) || !strncmp(aMonth, "Jan", 3)) {
        index = 0 ;
    }
    if (!strncmp(aMonth, "FEB", 3) || !strncmp(aMonth, "Feb", 3)) {
        index = 1 ;
    }
    if (!strncmp(aMonth, "MAR", 3) || !strncmp(aMonth, "Mar", 3)) {
        index = 2 ;
    }
    if (!strncmp(aMonth, "APR", 3) || !strncmp(aMonth, "Apr", 3)) {
        index = 3 ;
    }
    if (!strncmp(aMonth, "MAY", 3) || !strncmp(aMonth, "May", 3)) {
        index = 4 ;
    }
    if (!strncmp(aMonth, "JUN", 3) || !strncmp(aMonth, "Jun", 3)) {
        index = 5 ;
    }
    if (!strncmp(aMonth, "JUL", 3) || !strncmp(aMonth, "Jul", 3)) {
        index = 6 ;
    }
    if (!strncmp(aMonth, "AUG", 3) || !strncmp(aMonth, "Aug", 3)) {
        index = 7 ;
    }
    if (!strncmp(aMonth, "SEP", 3) || !strncmp(aMonth, "Sep", 3)) {
        index = 8 ;
    }
    if (!strncmp(aMonth, "OCT", 3) || !strncmp(aMonth, "Oct", 3)) {
        index = 9 ;
    }
    if (!strncmp(aMonth, "NOV", 3) || !strncmp(aMonth, "Nov", 3)) {
        index = 10 ;
    }
    if (!strncmp(aMonth, "DEC", 3) || !strncmp(aMonth, "Dec", 3)) {
        index = 11 ;
    }

    return(index) ;
}


/* PPCM */
int ppcm(int X, int Y)
{
    int A=X;
    int B=Y;
    while (A!=B)
    {
        while (A>B) B=B+Y;
        while (A<B) A=A+X;
    }
    return A;
}


/* PGCD */
int pgcd(int x, int y)
{
    return(x * y / ppcm(x, y)) ;
}
