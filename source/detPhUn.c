//
//  main.c
//  detPhUn
//
//  Created by Dominique Derauw on 30/08/12.
//  Copyright (c) 2012 Centre Spatial de Liège. All rights reserved.
//

#include <stdio.h>

#include "detPhUnLib.h"

/* fonctions */
void usage(void) ;
void createParameterFileAtPath(char *aPath) ;


/* main programm start */
int main(int argc, const char **argv)
{
	time_t startTime, endTime ;
    keyValue *p = NULL ;

    if (argc > 1) {
        if (isArgumentPresent(argc, argv, "-create")) {
            createParameterFileAtPath((char *)argv[1]) ;
        }
        
        p = readParameterFileAtPath((char *)argv[1]) ;
    }
    else {
        usage() ;
    }
	time(&startTime) ;
    
    deterministicPhaseUnwrapping(p) ;

	time(&endTime) ;
	duration(startTime, endTime) ;
	return(0) ;
}


void usage(void)
{
	printf("Usage:\ndetPhUn /pathTo/parameters.txt [-create] \n") ;
    printf("-create: A default parameter text file will be created at indicated path\n\n") ;
	exit(0) ;
}


void createParameterFileAtPath(char *aPath)
{
    keyValue *p ;
    
    p = allocAKeyValuePair() ;
    
    setStringValForKeyIn(p, "Deterministic phase unwrapping parameter file", "") ;
    setStringValForKeyIn(p, "*********************************************", "") ;
    setStringValForKeyIn(p, "/Ou/Ai/Je/Foutu/Ce/P_tain/Dinterfero", "Interferogram file path") ;
    setStringValForKeyIn(p, "/path/to/CoherenceFile", "Coherence image file path") ;
    setStringValForKeyIn(p, "1234", "X dimension") ;
    setStringValForKeyIn(p, "1234", "Y dimension") ;
    setStringValForKeyIn(p, "/pathTo/OutputFile", "Output file path") ;
    setStringValForKeyIn(p, "", "") ;
    setStringValForKeyIn(p, "Processing parameters", "") ;
    setStringValForKeyIn(p, "*********************", "") ;
    setStringValForKeyIn(p, "detPhUn2", "Mode (detPhUn1 or detPhUn2)") ;
    setStringValForKeyIn(p, "1", "Number of iterations") ;
    setStringValForKeyIn(p, "0.1", "Coherence threshold") ;
    setStringValForKeyIn(p, "0", "X FWHM of classical filter  (disabled if X or Y FWHM = 0)") ;
    setStringValForKeyIn(p, "0", "Y FWHM of classical filter  (disabled if X or Y FWHM = 0)") ;
    setStringValForKeyIn(p, "0", "Goldstein filter smoothing factor (disabled if 0)") ;
    setStringValForKeyIn(p, "YES", "Flattening (YES/NO)") ;
    setStringValForKeyIn(p, "", "") ;
    setStringValForKeyIn(p, "Windowing", "") ;
    setStringValForKeyIn(p, "*********", "") ;
    setStringValForKeyIn(p, "fullSize", "X window size") ;
    setStringValForKeyIn(p, "fullSize", "Y window size") ;
    setStringValForKeyIn(p, "0", "X border size") ;
    setStringValForKeyIn(p, "0", "Y border size") ;

    writeParameterFileAtPath(p, aPath) ;
    
    exit(0) ;
}
