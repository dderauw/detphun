/* **************************************** */
/* detPhUn : Deterministic phase unwrapping */
/* **************************************** */

/* The detPhUn directory contains this file and some sub directories which are:		*/
/* bin: Directory where the compiled version of detPhUn will be linked and saved. 	*/
/* Dev: Directory containing the Xcode project						*/
/* include: Directory containing .h files						*/
/* lib: Directory containing home made .c libraries					*/
/* source: Directory containing the detPhUn.c main file and a basic make file		*/
/* Test: Directory containing a test data set with an already filled parameter file	*/

To compile detPhUn, open a terminal and change dir to ./detPhUn/source then launch make.
It should compile fine provided you installed the fftw3 library on your machine. 
To do so, on Linux Ubuntu, use "sudo apt install libfftw3-dev". On MacOSX (Darwin), use macports: "sudo port install fftw-3".

Once compiled, from the detPhUn directory launch the following command:

./bin/detPhUn ./Test/params.txt

Have fun !


/* *********************************** */
/*  © SAREOS  -  Dominique Derauw 2020 */
/* dderauw@ecgs.lu - dderauw@sareos.eu */
/* *********************************** */
