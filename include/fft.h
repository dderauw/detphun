/* fft.h */
/* Librairies fft et ifft range et azimut */
/*
 *  Created by Dominique Derauw a long time ago.
 *  Copyright (c) Dominique Derauw. All rights reserved.
 *
 */

#if !defined(FFT)
#define FFT

#include "pourtous.h"
#include "complexTypes.h"
#include "ceil2.h"
#include "vectors.h"

#define ALONGX 0
#define ALONGY 1
#define FORWARD_FFT -1.
#define INVERSE_FFT  1.

typedef	struct {
	int		axe ;
	int		dimX ;
	int		dimY ;
	int		dimXP2 ;
	int		dimYP2 ;
	int		log2dimX ;
	int		log2dimY ;
	int 	*irvs ;
	double	*sin ;
	double	*cos ;
} StructFFT ;

/* Fonctions */
StructFFT *initFFT(int dimX, int dimY, int axe) ;
void fftY(double signe, complexFloat *bloc, StructFFT *PtrFFT) ;
void fftX(double signe, complexFloat *bloc, StructFFT *PtrFFT) ;
void freeFFT(StructFFT *PtrFFT) ;

#endif
