/*
 *  DefTypesComplex.h
 *  
 *
 *  Created by Dominique Derauw on Mon Dec 03 2001.
 *  Copyright (c) 2003 Dominique Derauw. All rights reserved.
 *
 */

#if !defined(DEF_TYPES_COMPLEX)
#define DEF_TYPES_COMPLEX

typedef struct {
        char r, i ;
    } complexChar ;		/* !!! In more than 2 bytes alignement computer environement this structure will probably have a size of 4 bytes */

typedef struct {
        short r, i ;
    } complexShort ;

typedef struct {
        int r, i ;
    } complexInt ;

typedef struct {
        long r, i ;
    } complexLong ;

typedef struct {
        float r, i ;
    } complexFloat ;
    
typedef struct {
        double r, i ;
    } complexDouble ;

#define COMPLEX_CHAR_SIZE sizeof(complexChar)
#define COMPLEX_SHORT_SIZE sizeof(complexShort)
#define COMPLEX_INT_SIZE sizeof(complexInt)
#define COMPLEX_LONG_SIZE sizeof(complexLong)
#define COMPLEX_FLOAT_SIZE sizeof(complexFloat)
#define COMPLEX_DOUBLE_SIZE sizeof(complexDouble)

#endif
