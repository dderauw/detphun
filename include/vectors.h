/*
 *  vectors.h
 *  
 *
 *  Created by dd on Mon Dec 03 2001.
 *  Copyright (c) 2001 Dominique Derauw All rights reserved.
 *
 */

#if !defined(VECTORS)
#define VECTORS

#include <stdio.h>
#include <stdlib.h>
#include "complexTypes.h"

typedef struct {
	int lowerIndex, upperIndex ;
	int maxValIndex, minValIndex, averageValIndex ;
	float *v ;
	float maxVal, minVal, average ;
} floatVector ;

unsigned char	*vectorUChar(int lowerIndex, int higherIndex) ;
short			*vectorShort(int lowerIndex, int higherIndex) ;
int				*vectorInt(int lowerIndex, int higherIndex) ;
int	unsigned    *vectorIntUnsigned(int lowerIndex, int higherIndex) ;
long			*vectorLong(int lowerIndex, int higherIndex) ;
long unsigned	*vectorLongUnsigned(int lowerIndex, int higherIndex) ;
float			*vectorFloat(int lowerIndex, int higherIndex) ;
double			*vectorDouble(int lowerIndex, int higherIndex) ;
complexChar	*vectorComplexChar(int lowerIndex, int higherIndex) ;
complexShort	*vectorComplexShort(int lowerIndex, int higherIndex) ;
complexFloat	*vectorComplexFloat(int lowerIndex, int higherIndex) ;
complexDouble	*vectorComplexDouble(int lowerIndex, int higherIndex) ;
void	*vectorOfType(int lowerIndex, int higherIndex, size_t type) ;

void freeVectorUChar(unsigned char *, int lowerIndex) ;
void freeVectorShort(short *, int lowerIndex) ;
void freeVectorInt(int *, int lowerIndex) ;
void freeVectorIntUnsigned(int unsigned *, int lowerIndex) ;
void freeVectorLong(long *, int lowerIndex) ;
void freeVectorLongUnsigned(long unsigned *v, int lowerIndex) ;
void freeVectorFloat(float *, int lowerIndex) ;
void freeVectorDouble(double *, int lowerIndex) ;
void freeVectorComplexChar(complexChar *, int lowerIndex) ;
void freeVectorComplexShort(complexShort *v, int lowerIndex) ;
void freeVectorComplexFloat(complexFloat *, int lowerIndex) ;
void freeVectorComplexDouble(complexDouble *, int lowerIndex) ;
void freeVectorOfType(void *v, int lowerIndex, size_t type) ;

floatVector *allocFloatVectorWithIndexes(int lowerIndex, int higherIndex) ;
void freeFloatVector(floatVector *aFloatVector) ;

#endif
