//
//  tileToolLib.h
//  detPhUn
//
//  Created by Dominique Derauw on 30/08/12.
//  Copyright (c) 2012 Dominique Derauw. All rights reserved.
//

#ifndef detPhUn_tileToolLib_h
#define detPhUn_tileToolLib_h

#include "pourtous.h"
#include "rawFileLib.h"
#include "complexCalculus.h"


struct tileToolStructure {
    rawFileDescriptor *input ;                              /* Input file linked with tile tool */
    rawFileDescriptor *output ;                             /* Output file linked with tile tool */
    char mirroring ;                                        /* Flag for mirroring data on borders */
                                                            /* 0 = no mirroring (zeros on borders). 1 = borders filled by circularization of data 2 = borders filled mirroring data */
    char leftBorder, rightBorder, bottomBorder, upBorder ;  /* Flag determining which border must be mirrored */
    char dataType[16] ;                                     /* Data type: char, int, long, float, double or complexFloat */
    int unsigned typeCode ;                                 /* Data code corresponding to type */
    int unsigned typeSize ;                                 /* Data size corresponding to type */
    int unsigned xTileSize ;                                /* Tile X dimension */
    int unsigned yTileSize ;                                /* Tile Y dimension */
    int unsigned xTileBorderSize ;                          /* Tile border X dimension */
    int unsigned yTileBorderSize ;                          /* Tile border Y dimension */
    int unsigned xTileNumber ;                              /* Number of tiles within input data file along X dimension */
    int unsigned yTileNumber ;                              /* Number of tiles within input data file along Y dimension */
    int unsigned xDimIn ;                                   /* Input data X dimension (copied from input raw file descriptor!) */
    int unsigned yDimIn ;                                   /* Input data Y dimension (copied from input raw file descriptor!) */
    int unsigned xDimOut ;                                  /* Output data X dimension (copied from output raw file descriptor!) */
    int unsigned yDimOut ;                                  /* Output data Y dimension (copied from output raw file descriptor!) */
    int  x0In ;                                             /* X0 coordinate of current tile within input data */
    int  y0In ;                                             /* Y0 coordinate of current tile within input data */
    int unsigned x0Tile ;                                   /* X0 coordinate within current tile */
    int unsigned y0Tile ;                                   /* Y0 coordinate within current tile */
    int unsigned x0Out ;                                    /* X0 coordinate of current tile within output data */
    int unsigned y0Out ;                                    /* Y0 coordinate of current tile within output data */
    int unsigned x0EA ;                                     /* X0 coordinate of Efficient Area within current tool */
    int unsigned y0EA ;                                     /* Y0 coordinate of Efficient Area within current tool */
    int unsigned xDimEA ;                                   /* Tile Efficient Area X size */
    int unsigned yDimEA ;                                   /* Tile Efficient Area Y size */
    int unsigned xDimReadData ;                             /* X size of effectively read data */
    int unsigned yDimReadData ;                             /* Y size of effectively read data */
    int unsigned xTileIndex ;                               /* Current tile X index */
    int unsigned yTileIndex ;                               /* Current tile Y index */
    quadrilateral *aoi ;
    void **tile ;                                           /* Tile matrix */
    void **inputDataMatrix ;                                /* Data matrix */
    void **outputDataMatrix ;                               /* Data matrix */
} ;

typedef struct tileToolStructure tileTool ;



tileTool *allocTileToolForDataOfType(void **inputDataMatrix, void **outputDataMatrix, int xDimData, int yDimData, char *dataType) ;
tileTool *allocTileToolForFilesOfType(rawFileDescriptor *inputFile, rawFileDescriptor *outputFile, char *dataType) ;
void freeTileTool(tileTool *aTileTool) ;
void **initTileToolWithSizes(tileTool *aTileTool, int unsigned xTileSize, int unsigned yTileSize, int unsigned xTileBorderSize, int unsigned yTileBorderSize) ;
void resetTileToZero(tileTool *aTileTool) ;
void updateTileToolForIndexes(tileTool *aTileTool, int unsigned xTileIndex, int unsigned yTileIndex) ;
void **copyTileAtIndexes(tileTool *aTileTool, int unsigned xTileIndex, int unsigned yTileIndex) ;
void **readTileAtIndexes(tileTool *aTileTool, int unsigned xTileIndex, int unsigned yTileIndex) ;
void setMirroringForTileTool(tileTool *aTileTool) ;
void setCircularizationForTileTool(tileTool *aTileTool) ;
void mirrorDataLeft(tileTool *aTileTool) ;
void mirrorDataRight(tileTool *aTileTool)  ;
void mirrorDataBottom(tileTool *aTileTool) ;
void mirrorDataUp(tileTool *aTileTool) ;
void writeTile(tileTool *aTileTool) ;
void saveTile(tileTool *aTileTool) ;
void writeTileToOutputFile(tileTool *aTileTool, rawFileDescriptor *output) ;
void registerDataTileInTileTool(void **data, tileTool *aTileTool) ;
void swapInAndOutInTileTool(tileTool *aTileTool) ;
float localAverageOfFloatTile(tileTool *aTileTool) ;
void multiplyTileTools(tileTool *M1, tileTool *M2, tileTool *M3) ;


#endif
