/*
 *  ceil2.h
 *  vDephi
 *
 *  Created by Dominique Derauw on Mon Mar 01 2004.
 *  Copyright (c) 2004 Dominique Derauw. All rights reserved.
 *
 */
#include <math.h>

#if !defined(CEIL2)
#define CEIL2

void	ceil2(int *ptr_nb, int *ptr_expnb) ;
int		ceil2ofIntVal(int anInt) ;
int		ceil2ofFloatVal(float aFloat) ; 

#endif
