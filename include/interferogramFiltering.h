/* Filtre.h */
/*
 *  Created by Dominique Derauw a long time ago.
 *  Copyright (c) Dominique Derauw. All rights reserved.
 *
 */

#if !defined(FILTRE)
#define FILTRE
#include "pourtous.h"
#include "fft.h"
#include "counters.h"
#include "tileToolLib.h"
#include "complexCalculus.h"
#include "paramLib.h"

#define INTERF_FIlTERING 0x1000
#define _COMBINED_FILTERINGS_ 0x2000
#define _BLAH_BLAH_ 0x4000

#ifdef _ADAPTATIVE_FILTERING_
    #define XTILESIZE 32
    #define YTILESIZE 32
    #define XTILEBORDERSIZE 15
    #define YTILEBORDERSIZE 15
#else 
    #define XTILESIZE 512
    #define YTILESIZE 512
    #define XTILEBORDERSIZE 32
    #define YTILEBORDERSIZE 32
#endif


typedef	struct {
	int	iX ;
	int	iY ;
	int	DimX ;
	int	DimY ;
	int	SupX ;
	int	SupY ;
	int	DimXZE ;
	int	DimYZE ;
	int	NBlocX ;
	int	NBlocY ;
	int	LenX ;
	int	LenY ;
	float	*e, *s, *FFTGauss ;
    float **filteredData ;
    float **filter ;
	complexFloat	*BlocComplexe ;
    complexFloat **data ;
	StructFFT	*FFTLigne ;
	StructFFT	*FFTColonne ;
} CarBloc ;


void convertFloatPhaseTileToComplex(tileTool *phaseTileTool, tileTool *complexTileTool) ;
void convertComplexPhaseTileToFloat(tileTool *phaseTileTool, tileTool *complexTileTool) ;
void initFilterTileTool(tileTool *filterTileTool, float sigmaX, float sigmaY) ;
void tileFiltering(tileTool *aTileTool, tileTool *filterTileTool) ;

void interferogramFiltering(keyValue *parameters) ;
void adaptativeFilteringOfTile(tileTool *aTileTool, tileTool *smoothedSpectrum, float alpha) ;
char testNaNTile(tileTool *complexPhaseTileTool, char *comment) ;


#endif
