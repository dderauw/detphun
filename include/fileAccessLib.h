/*
 *  fileAccesLib.h
 *  trackDataReader
 *
 *  Created by Dominique Derauw on 3/02/05.
 *  Copyright 2005 Dominique Derauw. All rights reserved.
 *
 */

#if !defined(FILE_ACCES_LIB)
#define FILE_ACCES_LIB

#if !defined(_BUFFER_SIZE_)
#define _BUFFER_SIZE_ (int)(32 * 1024 * 1024)
#endif

#include "stringLib.h"
#include "pourtous.h"
#include "planeGeometry.h"
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/errno.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <sys/types.h>

char *getFileNameFromPath(char *aPath) ;
char *getPointerToFileNameInPath(char *path) ;
char *getFileExtensionFromPath(char *aPath) ;
char *getPointerToFileExtensionInPath(char *path) ;
char *getPointerToLastFileExtensionInPath(char *path) ;
void stripExtensionAtEndOfPath(char *aPath) ;
void stripAllExtensions(char *aPath) ;
char *addExtensionAtEndOfPath(char *aPath, char *extension) ;
char *getDirectoryPathFromPath(char *aPath) ;
char *getUpperDirectoryPath(char *aDirectoryPath) ;
char isValidDirectoryForPath(char *aPath) ;
char isSymbolicLink(char *path) ;
char fileExistsAtPath(char *aPath) ;
char isDir(char *aPath) ;
char isReadAccessGrantedAtPath(char *aPath) ;
char isWriteAccessGrantedAtPath(char *aPath) ;
char isRWAccessGrantedAtPath(char *aPath) ;
off_t getFileLengthAtPath(char *path) ;
CSVStruct *recursiveSearchOfDirInDir(char *dirName, char *dir) ;
CSVStruct *recursiveSearchOfFileInDir(char *fileName, char *dir) ;
CSVStruct *recursiveSearchOfFilesWithExtensionInDir(char *extension, char *dir) ;
char isFilePresentInSubDir(char *fileName, char *dir) ;
char **getDirectoryContentAtPath(char *aPath, int *numberOfEntries) ;
char **getDirectoryEntriesAtPath(char *aPath) ;
void freeDirectoryEntriesAtPath(char **entries, char *aPath) ;
char **getAllDirectoryEntriesAtPath(char *aPath) ;
void freeDirectoryEntries(char **entries, int numberOfEntries) ;
int getNumberOfDirectoryEntriesAtPath(char *aPath) ;
int getNumberOfAllDirectoryEntriesAtPath(char *aPath) ;
void removeDirectoryAtPath(char *aPath) ;
void copyFileAtPathToPath(char *inputPath, char *outputPath) ;
char *expendEnvironmentVariablesInPath(char *aPath) ;

#endif
