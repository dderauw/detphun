/*
 *  arithmetiqueComplexe.h
 *  decPol
 *
 *  Created by Dominique Derauw on Wed Sep 25 2002.
 *  Copyright (c) 2002 Dominique Derauw. All rights reserved.
 *
 */

#if !defined(ARITCOMP)
#define ARITCOMP

#include <math.h>
#include "complexTypes.h"

#if !defined(HALFPI)
#define HALFPI		M_PI_2 
#endif
#if !defined(PI)
#define	PI			M_PI 
#endif

complexFloat 	mRC(complexFloat a, float b) ;		/* Complex by real multiplication */
complexDouble 	mRComplexDouble(complexDouble a, double b) ;
complexFloat 	mC(complexFloat a, complexFloat b) ;	/* Complex by complex multiplication */
complexDouble 	multComplexDouble(complexDouble a, complexDouble b) ;
complexFloat 	aC(complexFloat a, complexFloat b) ;	/* Complex addition */
complexDouble sumComplexDouble(complexDouble a, complexDouble b) ;
complexFloat 	sC(complexFloat a, complexFloat b) ;	/* Complex substraction */
complexDouble	subtComplexDouble(complexDouble a, complexDouble b) ;
complexFloat 	cC(complexFloat a) ;					/* Complex conjugate */
complexDouble	cCComplexDouble(complexDouble a) ;
float			modC(complexFloat a) ;				/* modulus */
double modComplexDouble(complexDouble a) ;
float			sqmC(complexFloat a) ;				/* square modulus */
double sqmComplexDouble(complexDouble a) ;
float			phiC(complexFloat a) ;				/* Complex phase */
complexFloat divC(complexFloat a, complexFloat b)  ;	/* Complex division */
complexDouble divComplexDouble(complexDouble a, complexDouble b) ;

#endif
