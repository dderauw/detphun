/*
 *  readParamLib.h
 *  2ESARFormat
 *
 *  Created by Dominique Derauw on 16/02/06.
 *  Copyright 2006 Dominique Derauw. All rights reserved.
 *
 */

#if !defined(READPARAMLIB)
#define READPARAMLIB

#include "stringLib.h"
#include "rawFileLib.h"
#include "fileAccessLib.h"
#include "vectors.h"

#define FORMATMAXLENGTH 32
#define MAXSTRINGVALUELENGTH 128
#define NO_ERROR 0
#define VALUE_SET_FOR_KEY 1
#define KEY_VALUE_ADDDED 2
#define KEY_VALUE_NOT_FOUND 22
#define TYPE_SET_FOR_KEY 4
#define KEY_VALUE_REMOVED 5
#define COMMENT_NOT_FOUND 6

extern char unsigned plError ; /* ParamLib error code */


#define TEXT_LINE_MAX_LENGTH 512
extern char aTextLine[TEXT_LINE_MAX_LENGTH + 1] ;

struct keyValPair {
	char *key ;
	char *stringVal ;				/* The value saved as string (for rank 0) */
	char *type ;					/* Data type may be defined in a string */
	char rank ;						/* A rank can be set in order to manage vectors or matrix */
	int *dimensions ;				/* If rank is not 0, dimensions[rank] contains the dimensions of the vector/matrix */
	void *value ;					/* The value saved using the defined type and rank (if set) */
	struct keyValPair *nextOne ;
} ;

typedef struct keyValPair keyValue ;


keyValue *allocAKeyValuePair(void) ;
void freeKeyValueList(keyValue *parameterList) ;
void resetKeyValuePair(keyValue *aKeyValuePair) ;
void freeKeyValuePair(keyValue *aKeyValuePair) ;
keyValue *readParameterFileAtPath(char *aPath) ;
void writeParameterFileAtPath(keyValue *parameterList, char *aPath) ;
char *getStringValForKeyIn(keyValue *parameterList, char *aKey) ;
keyValue *setStringValForKeyIn(keyValue *parameterList, char *theValue, char *theKey) ;
size_t	findStringValLengthIn(char *aString) ;
char setTypeForKeyIn(keyValue *parameterList, char *theType, char *theKey) ;
void updateValuesIn(keyValue *parameterList) ;
void *getValueForKeyIn(keyValue *parameterList, char *aKey) ;
int getIntValForKeyIn(keyValue *parameterList, char *aKey) ;
float getFloatValForKeyIn(keyValue *parameterList, char *aKey) ;
double getDoubleValForKeyIn(keyValue *parameterList, char *aKey) ;
keyValue *setVoidPointerValForKey(keyValue *parameterList, void *aPointer, char *theKey) ;
keyValue *setUCharValForKeyIn(keyValue *parameterList, char unsigned anUChar, char *theKey) ;
keyValue *setShortValForKeyIn(keyValue *parameterList, short aShort, char *theKey) ;
keyValue *setIntValForKeyIn(keyValue *parameterList, int anInt, char *theKey) ;
keyValue *setLongValForKeyIn(keyValue *parameterList, long aLong, char *theKey) ;
keyValue *setFloatValForKeyIn(keyValue *parameterList, float aFloat, char *theKey) ;
keyValue *setDoubleValForKeyIn(keyValue *parameterList, double aDouble, char *theKey) ;
keyValue *setVectorUCharForKeyIn(keyValue *parameterList, char unsigned *anUCharVector, int vectorLength, char *theKey) ;
keyValue *setVectorShortForKeyIn(keyValue *parameterList, short *aShortVector, int vectorLength, char *theKey) ;
keyValue *setVectorIntForKeyIn(keyValue *parameterList, int *anIntVector, int vectorLength, char *theKey) ;
keyValue *setVectorLongForKeyIn(keyValue *parameterList, long *aLongVector, int vectorLength, char *theKey) ;
keyValue *setVectorFloatForKeyIn(keyValue *parameterList, float *aFloatVector, int vectorLength,  char *theKey) ;
keyValue *setVectorDoubleForKeyIn(keyValue *parameterList, double *aDoubleVector, int vectorLength,  char *theKey) ;
char removeKeyValuePairIn(keyValue *parameterList, char *theKey) ;
char removeCommentIn(keyValue *parameterList, char *aComment) ;
keyValue *isKeyValPresent(keyValue *parameterList, char *aKey) ;
keyValue *isCommentPresentIn(keyValue *parameterList, char *aComment) ;
void writeVectorAtPath(keyValue *aKeyValue, char *aVectorFilePath) ;
keyValue *setCSVValForKeyIn(keyValue *parameterList, CSVStruct *csv, char *theKey) ;
CSVStruct *getCSVValForKeyIn(keyValue *parameterList, char *aKey) ;


#endif
