/*
 *  matrix.h
 *  manMadeStructureGrowth
 *
 *  Created by Dominique Derauw on 16/12/08.
 *  Copyright 2008 Dominique Derauw All rights reserved.
 *
 */

/* Matrix definition and allocation on continuous memory space */

#if !defined(MATRIX)
#define MATRIX

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "complexTypes.h"

unsigned char	**matrixUChar(int rowLowerIndex, int rowHigherIndex, int columnLowerIndex, int columnHigherIndex) ;
void	freeMatrixUChar(unsigned char **aMatrix, int rowLowerIndex, int columnLowerIndex) ;

short	**matrixShort(int rowLowerIndex, int rowHigherIndex, int columnLowerIndex, int columnHigherIndex) ;
void	freeMatrixShort(short **aMatrix, int rowLowerIndex, int columnLowerIndex) ;

short unsigned	**matrixShortUnsigned(int rowLowerIndex, int rowHigherIndex, int columnLowerIndex, int columnHigherIndex) ;
void	freeMatrixShortUnsigned(short unsigned **aMatrix, int rowLowerIndex, int columnLowerIndex) ;

int	**matrixInt(int rowLowerIndex, int rowHigherIndex, int columnLowerIndex, int columnHigherIndex) ;
void	freeMatrixInt(int **aMatrix, int rowLowerIndex, int columnLowerIndex) ;

int unsigned **matrixIntUnsigned(int rowLowerIndex, int rowHigherIndex, int columnLowerIndex, int columnHigherIndex) ;
void freeMatrixIntUnsigned(int **aMatrix, int rowLowerIndex, int columnLowerIndex) ;

float	**matrixFloat(int rowLowerIndex, int rowHigherIndex, int columnLowerIndex, int columnHigherIndex) ;
void	freeMatrixFloat(float **aMatrix, int rowLowerIndex, int columnLowerIndex) ;

double	**matrixDouble(int rowLowerIndex, int rowHigherIndex, int columnLowerIndex, int columnHigherIndex) ;
void	freeMatrixDouble(double **aMatrix, int rowLowerIndex, int columnLowerIndex) ;

complexShort	**matrixComplexShort(int rowLowerIndex, int rowHigherIndex, int columnLowerIndex, int columnHigherIndex) ;
void	freeMatrixComplexShort(complexShort **aMatrix, int rowLowerIndex, int columnLowerIndex) ;

complexFloat	**matrixComplexFloat(int rowLowerIndex, int rowHigherIndex, int columnLowerIndex, int columnHigherIndex) ;
void	freeMatrixComplexFloat(complexFloat **aMatrix, int rowLowerIndex, int columnLowerIndex) ;

complexDouble	**matrixComplexDouble(int rowLowerIndex, int rowHigherIndex, int columnLowerIndex, int columnHigherIndex) ;
void	freeMatrixComplexDouble(complexDouble **aMatrix, int rowLowerIndex, int columnLowerIndex) ;

void	**matrixOfType(int rowLowerIndex, int rowHigherIndex, int columnLowerIndex, int columnHigherIndex, size_t type) ;
void	freeMatrixOfType(void **aMatrix, int rowLowerIndex, int columnLowerIndex, size_t type) ;

#endif
