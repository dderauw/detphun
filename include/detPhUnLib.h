//
//  detPhUnLib.h
//  detPhUn
//
//  Created by Dominique Derauw on 30/08/12.
//  Copyright (c) 2012 Dominique Derauw. All rights reserved.
//

#ifndef detPhUn_detPhUnLib_h
#define detPhUn_detPhUnLib_h

#ifndef FIRST_PHASE_COMPONENT_EXISTS
#define FIRST_PHASE_COMPONENT_EXISTS 0x4
#endif

#define _DETPHUN_WITH_FILTERING_ 0x1000

#define _DETPHUN1_ 0x4000
#define _DETPHUN2_ 0x8000

#include "pourtous.h"
#include "paramLib.h"
#include "fileAccessLib.h"
#include "vectors.h"
#include "matrix.h"
#include "complexTypes.h"
#include "complexCalculus.h"
#include "fft.h"
#include "counters.h"
#include "tileToolLib.h"
#include "interferogramFiltering.h"
#include "resmat.h"
#include <fftw3.h>

typedef struct {
    char mode, removePhasePlane ;
    rawFileDescriptor *inputPhase, *outputPhase, *residualPhase ;
    int            xDim, yDim ;
    int            xDim2, yDim2 ;
    int			iterationNumber ;
    float 		**initialPhase, **currentPhase, **dephi, **coh ;
    float       **nablaX, **nablaY ;
    complexDouble *xPhaseShift, *yPhaseShift ;
    complexDouble **Z1, **Z2 ;
    double 		*qx, *qy, *qx2, *qy2 ;
    float coherenceThreshold ;
    double **input, **output ;
    double **pq2 ;
    double **sinPhi, **cosPhi, **temp1, **temp2 ;
    fftw_plan FFTPlan, IFFTPlan ;
    fftw_plan FFTPlanZ1, IFFTPlanZ1 ;
    fftw_plan FFTPlanZ2, IFFTPlanZ2 ;
} detphun ;


void deterministicPhaseUnwrapping(keyValue* p) ;
void diffInterfDephi(detphun *gP) ;
void roundIt(detphun *gP) ;
void addZ1iToDephi(detphun *gP) ;
void diffTemp(detphun *gP) ;
void scalarMultiplyZ1(detphun *gP, float aFloat) ;
void divZ1Q2(detphun *gP) ;
void divZ1Qx(detphun *gP) ;
void addZ1Z2(detphun *gP)  ;
void halfPixelShiftZ0(detphun *gP) ;
void multZ1PIQx(detphun *gP) ;
void multZ2PIQy(detphun *gP) ;
void multZ1Qx(detphun *gP) ;
void multZ2Qy(detphun *gP) ;
void multZ1Qy(detphun *gP) ;
void multOutPQ2(detphun *gP) ;
void divOutPQ2(detphun *gP) ;
void multOutCosPhi(detphun *gP) ;
void multOutSinPhi(detphun *gP) ;
void multOutCst(detphun *gP, double factor) ;
void evaluateXGradient(detphun *gP) ;
void evaluateYGradient(detphun *gP) ;
void transferDataToZ(detphun *gP) ;
detphun *allocAndInitDetphunWithWindowSize(int xDim, int yDim, int unsigned flag) ;
void resetDetphun(detphun *gP) ;
void resetZ(detphun *gP) ;
void freeDetPhun(detphun *gP) ;
void removePhasePlane(detphun *gP) ;

#endif
