/*
 *  stringLib.h
 *  trackDataReader
 *
 *  Created by Dominique Derauw on 3/02/05.
 *  Copyright 2005 Dominique Derauw. All rights reserved.
 *
 */

#if !defined(STRING_LIB)
#define STRING_LIB

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXSTRINGLENGTH 1024
#define SUBSTRINGTOOBIG  -4 
#define SUBSTRINGNOTFOUND  -5 
#define STRINGTOOSMALL -6 

struct CSVStruct {
	int numberOfEntries ;
	char **stringVal ;
} ;
typedef struct CSVStruct CSVStruct ;


char *allocStringOfLength(size_t) ;
char *readFirstStringIn(char *texte) ;
char *getStringUpToDelimiter(char *aString, char *delimiter) ;
char *getStringAfterDelimiter(char *aString, char *delimiter) ;
char *concatenateStrings(char *firstString, char *secondString) ;
void stripWhiteCharsAtStartOfString(char *aString) ;
void stripWhiteCharsAtEndOfString(char *aString) ;
void replaceWhiteCharsInStringByChar(char *aString, char replacementChar) ;
char replaceSubStringInString(char *aString, char *subStringToFind, char *replacementString) ;
char replaceNSubStringInString(char *aString, char *subStringToFind, char *replacementString) ;
void removeStringInString(char *aString, char *stringToRemove) ;
void removeAnyOccurrenceOfSubStringInString(char *aString, char *stringToRemove) ;
char isSubStringPresentInString(char *aString, char *subStringToFind) ;
char *getStringAfterSubStringInString(char *aString, char *aSubString) ;
int locateSubStringInString(char *aString, char *subStringToFind) ;
void convertStringToLowercase(char *aString) ;
char *initStringWithString(char *aString) ;
char *initStringWith2Strings(char *string1, char *string2) ;
char *initStringWith3Strings(char *string1, char *string2, char *string3) ;
char *initStringWith4Strings(char *string1, char *string2, char *string3, char *string4) ;
void removeEmptyValsInCSV(CSVStruct *csv) ;
CSVStruct *readCSVInString(char *aString) ;
CSVStruct *readWhiteCharSeperatedValuesInString(char *aString) ;
CSVStruct *addStringValInCSVStruct(char *aStringValue, CSVStruct *csv) ;
CSVStruct *concatenateCSVStructs(CSVStruct *csv1, CSVStruct *csv2) ;
char isStringValPresentInCSV(char *aStringValue, CSVStruct *csv) ;
CSVStruct *removeStringValInCSVStruct(char *aStringValue, CSVStruct *csv) ;
CSVStruct *removeStringValAtIndexInCSVStruct(int i, CSVStruct *csv) ;
CSVStruct *allocCSVStruct(void) ;
void freeCSVStruct(CSVStruct *csv) ;

#endif
