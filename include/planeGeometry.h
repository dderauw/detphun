/*
 *  planeGeometry.h
 *  initInSAR
 *
 *  Created by Dominique Derauw on 1/11/11.
 *  Copyright 2011 Dominique Derauw All rights reserved.
 *
 */

#if !defined(PLANEGEOMETRY)
#define PLANEGEOMETRY

/* call to librairies */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

//#include "pourtous.h"
#include "matrix.h"


typedef struct {
    int	k ;
    int	l ;
} intPoint2D ;

#ifndef COORD2D
#define COORD2D
typedef struct {
    double	x ;
    double	y ;
} coord2D ;
#endif

#if !defined(RECTANGLE)
#define RECTANGLE
typedef struct {
	double xMin ;
	double yMin ;
	double xMax ;
	double yMax ;
	double dimX ;
	double dimY ;
} rectangleType ;
#endif


typedef struct {
	int N ;
	coord2D *P ;
	double **V ;
} polygon ;

typedef polygon segment ;
typedef polygon triangle ;
typedef polygon quadrilateral ;


polygon *allocPolygone(int numberOfAngles) ;
polygon *getCopyOfPolygon(polygon *aPolygon) ;
coord2D *allocPoint(void) ;
segment *allocSegment(void) ;
triangle *allocTriangle(void) ;
quadrilateral *allocQuadrilateral(void) ;
void freePolygone(polygon *aPolygone) ;
void planeAffineTransformOfPoint(double *initialPoint, double *transformedPoint, double **usedPlaneAffineTransform) ;
coord2D  *planeAffineTransformOfCoord2D(coord2D *initialPoint, coord2D *transformedPoint, double **usedAffineTransform) ;
double **inversePlaneAffineTransform(double **initialTransform, double **inverseTransform) ;
polygon *planeAffineTransformOfPolygon(polygon *initialPolygon, polygon *transformedPolygon, double **usedAffineTransform) ;
polygon *circumscribedRectangleOfPolygon(polygon *aPolygone, polygon *aRectangle) ;
double **allocUnitaryAffineTransform(void) ;
char isAffineTransformUnitary(double **T) ;
char isAffineTransformNULL(double **T) ;
char isPointIncludedInPolygon(coord2D *aPoint, polygon *aPolygon) ;
char polygonesIntersect(polygon *polygon1, polygon *polygon2) ;
coord2D *segmentIntersect(segment *seg1, segment *seg2) ;
double **combinedAffineTransform(double **T2, double **T1) ;


#endif

