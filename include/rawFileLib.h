//
//  rawFileLib.h
//  EnviSATDataReader
//
//  Created by Dominique Derauw on 30/06/16.
//
//

#ifndef __rawFileLib__
#define __rawFileLib__

#include <stdio.h>
#include "stringLib.h"
#include "fileAccessLib.h"
#include "planeGeometry.h"

typedef struct {
    FILE	*f ;
    char	*accessPath ;
    char	*directoryPath ;
    char	*fileName ;
    char	*mode ;
    char    byteOrder ;
    size_t	fileLength ;
    int unsigned	xDim ;      /* Data X dimension */
    int	unsigned	yDim ;      /* Data Y dimension */
    int xLowerIndex ;           /* Data matrix lower column index */
    int yLowerIndex ;           /* Data matrix lower row index */
    double xSampling ;          /* X sampling in data units */
    double ySampling ;          /* Y sampling in data units */
    double x0 ;                 /* Lower left X coordinate in data units */
    double y0 ;                 /* Lower left Y coordinate in data units */
    void *excludingValue ;      /* pointer to an excluding value to be defined by handler */
    size_t		type ;          /* Data type in number of bytes */
    quadrilateral *aoi ;        /* Area of interest within raw file */
    void *data ;
    void **indexedData ;
} rawFileDescriptor ;

typedef rawFileDescriptor fileDescriptor ;

typedef struct {
    int unsigned numberOfFiles ;
    char	*location ;
    char    *genericName ;
    char    **extension ;
    int		dimX ;
    int		dimY ;
    frameType *aoi ;
    FILE **f ;
    rawFileDescriptor **file ;
} rawFileSet ;

rawFileDescriptor *allocFileDescriptor(void) ;
rawFileDescriptor *allocFileDescriptorForPath(char *aPath) ;
char openRawFileForReading(rawFileDescriptor *aFileDescriptor) ;
char openRawFileForWriting(rawFileDescriptor *aFileDescriptor) ;
char openRawFileForReadingAndWriting(rawFileDescriptor *aFileDescriptor) ;
rawFileDescriptor *openRawFileForReadingAtPath(char *aPath) ;
rawFileDescriptor *openRawFileForWritingAtPath(char *aPath) ;
rawFileDescriptor *openRawFileForReadingAndWritingAtPath(char *aPath) ;
rawFileDescriptor *createRawFileForReadingAndWritingAtPath(char *aPath) ;
void setRawFilePath(rawFileDescriptor *aFileDescriptor, char *newFullPath) ;
void setRawFileDimensions(rawFileDescriptor *aFileDescriptor, int xDim, int yDim) ;
void setRawFileLowerIndexes(rawFileDescriptor *aFileDescriptor, int xLowerIndex, int yLowerIndex) ;
void setRawFileType(rawFileDescriptor *aFileDescriptor, size_t type) ;
void setRawFileOrigin(rawFileDescriptor *aFileDescriptor, int x0, int y0) ;
void setRawFileSampling(rawFileDescriptor *aFileDescriptor, int xSampling, int ySampling) ;
void setRawFileAoI(rawFileDescriptor *aFileDescriptor, quadrilateral *aoi) ;
void setRawFileAoIWithValues(rawFileDescriptor *aFileDescriptor, int xMin, int yMin, int xMax, int yMax) ;
void freeRawFileDescriptor(rawFileDescriptor *) ;
void closeRawFile(rawFileDescriptor *aFileDescriptor) ;


#endif /* defined(__EnviSATDataReader__rawFileLib__) */
