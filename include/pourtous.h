/* pourtous.h */
/* constantes, structure complexe, arret sur erreur */
/*
 *  Created by Dominique Derauw a long time ago.
 *  Copyright (c) Dominique Derauw. All rights reserved.
 *
 */


#ifndef POURTOUS
#define POURTOUS


/* call to librairies */
#include <stdio.h>
#include <stdlib.h>
#include <tgmath.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <unistd.h>

/* call to system librairies */
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/errno.h>
#include <sys/resource.h>

#include "complexTypes.h"

/* definition of structure corresponding to data pixels */
/* definition of constants */
#ifndef _VERBOSE_
#define _VERBOSE_ 0x8000000
#endif
#ifndef HALFPI
#define HALFPI		M_PI_2
#endif
#ifndef PI
#define	PI			M_PI
#endif
#define	TWOPI		(2. * PI)
#define c0		299792458
#define	Chouillat	1.0e-12
#define MAXPATHLENGTH 1024
#define MAXCOMMENTLENGTH 1024
#define _MAX_COMMAND_LENGTH_ 1024
#define MAXERRORMESSAGELENGTH 1024
#define speedOfLight c0
#define UNDEFINEDVALUE -9999
#define INCLUDESBINSAR
#define MAX_NUMBER_OF_OPENED_FILES 1024


/* Data types definition */
#define _UCHAR_ID_  0x00000001
#define _CHAR_ID_   0x00000002
#define _USHORT_ID_ 0x00000003
#define _SHORT_ID_  0x00000004
#define _UINT32_ID_ 0x00000005
#define _INT32_ID_  0x00000006
#define _UINT64_ID_ 0x00000007
#define _INT64_ID_  0x00000008
#define _REAL32_ID_ 0x00000100
#define _REAL64_ID_ 0x00000200
#define _CPLX32_ID_ 0x00010000
#define _CPLX64_ID_ 0x00020000


struct divrf {
    int 	q ;
    float	r ;
} ;

struct divr {
    int 	q ;
    double	r ;
} ;

union fl {
	float	fl ;
	char unsigned	b[4] ;
} ;

typedef struct {
    float	x ;
    float	y ;
    float	z ;
} coord3Df ;

#ifndef COORD2D
#define COORD2D
typedef struct {
    double	x ;
    double	y ;
} coord2D ;

union uCoord2D {
    coord2D p ;
    double  v[2] ;
} ;

typedef union uCoord2D point2D ;
#endif

typedef struct {
    double	x ;
    double	y ;
    double	z ;
} coord3D ;

/* Definition of a frame as a rectangle with integer coordinates to delimit a window or a frame in an image */
#ifndef _FRAME_
#define _FRAME_
typedef struct {
    int xMin ;
    int yMin ;
    int xMax ;
    int yMax ;
    int dimX ;
    int dimY ;
} frameType ;
#endif

#ifndef RECTANGLE
#define RECTANGLE
typedef struct {
    double xMin ;
    double yMin ;
    double xMax ;
    double yMax ;
    double dimX ;
    double dimY ;
} rectangleType ;
#endif



extern char	ertex[MAXERRORMESSAGELENGTH] ;
extern char	accessPath[MAXPATHLENGTH] ;
extern char	aComment[MAXCOMMENTLENGTH] ;
extern char	aCommand[_MAX_COMMAND_LENGTH_] ;
extern float floatNaN ;
extern float floatInf ;

float genFloatNaN(void) ;
float genFloatInf(void) ;
void ase(char *) ;
double unwrappedPhaseGap(float endPoint, float startPoint) ;
double unwrappPhaseFromPointToPoint(float endPoint, float startPoint) ;
double	dephi(float *, float *) ;
double	phi(complexFloat *) ;
double	phaseOfComplexDouble(complexDouble *ptr) ;
void	duration(long startTime, long endTime) ;
char	argument(char **addressOfNextArgument, int argc, const char **argv, const char *searchedArgument) ;
char	isArgumentPresent(int argc, const char **argv, const char *searchedArgument) ;
char	isFlagPresent(int argc, const char **argv, const char *searchFlag) ;
char *getNextArgument(int argc, const char **argv, const char *searchedArgument) ;
struct divr divr(double, double) ;
struct divrf divrf(float x, float y) ;
void floatByteSwap(float *) ;
double aRandomNumberBetweenValues(double minVal, double maxVal) ;
float aRandomFloatBetweenValues(float minVal, float maxVal) ;
char isArchBigEndian(void) ;
char setNumberOfAllowedOpenendFilesToMax(void) ;
char areFloatValsEquals(float val1, float val2) ;
int monthIndex(char *aMonth) ;
int ppcm(int X, int Y) ;
int pgcd(int X, int Y) ;


#endif
