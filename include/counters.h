/*
 *  counters.h
 *  ersRAWReader
 *
 *  Created by Dominique Derauw on 7/09/06.
 *  Copyright 2006 Dominique Derauw. All rights reserved.
 *
 */


/* Little functions to create and display progress counters */
#if !defined(PROGRESS_COUNTER)
#define PROGRESS_COUNTER


#include "pourtous.h"

typedef struct {
	FILE *output ;
	int currentIndex ;
	int minIndex ;
	int maxIndex ;
	int gap ;
	int formerValue ;
    int displayIndex ;
} progressCounter ;

progressCounter *allocProgressCounter(void)  ;
progressCounter *initProgressCounterWithMinMaxValue(int min, int max) ;
void resetProgressCounter(progressCounter *aProgressCounter) ;
int updateProgressCounterForIndex(progressCounter *p, int anIndex) ;
int updatePointProgressCounterForIndex(progressCounter *p, int anIndex) ;
void freeProgressCounter(progressCounter *p) ;

#endif

